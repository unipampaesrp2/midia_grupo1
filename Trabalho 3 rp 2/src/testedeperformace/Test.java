package testedeperformace;

import java.io.File;
import java.io.IOException;

import midias.Podcast;
import controler.DAOMidia;
import controler.leo.c.DAOArrayList;
import controler.leo.c.DAOHashMap;
import controler.leo.c.DAOHashSet;
import controler.leo.c.DAOLinkedHashMap;
import controler.leo.c.DAOLinkedHashSet;
import controler.leo.c.DAOLinkedList;
import controler.leo.c.DAOTreeMap;
import controler.leo.c.DAOTreeSet;
import controler.leo.c.DAOVector;

public class Test {
	private static int numeroDeCiclos=5000;
	
	public static void main(String[] args) {
		
		File files[]=new File[9];
		for (int i = 0; i < files.length; i++) {
			files[i]= new File("f"+i);
		}
		for (int i = 0; i < files.length; i++) {
			try {
				files[i].createNewFile();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		DAOMidia daos[]=new DAOMidia[]{new DAOArrayList(files[0]),new DAOHashMap(files[1]),new DAOLinkedHashMap(files[2]), new DAOLinkedList(files[3]),new DAOTreeMap(files[4]),new DAOVector(files[5]),new DAOHashSet(files[6]),new DAOLinkedHashSet(files[7]),new DAOTreeSet(files[8])};
		String nomeDasClasses[]=new String[]{"DAOArrayList","DAOHashMap","DAOLinkedHashMap","DAOLinkedList","DAOTreeMap","DAOVector","DAOHashSet","DAOLinkedHashSet","DAOTreeSet"};
		for (int i = 0; i < nomeDasClasses.length; i++) {
			System.out.println("\nTeste da classe "+nomeDasClasses[i]);
			long inicio=System.currentTimeMillis();

			testeDeInsercao(daos[i], nomeDasClasses[i]);
			testeDeBuscaIP(daos[i], nomeDasClasses[i]);
			testeDeBusca(daos[i], nomeDasClasses[i]);
			testeAlterar(daos[i], nomeDasClasses[i]);
			testeRemover(daos[i], nomeDasClasses[i]);
			
			System.out.println("\nTempo de teste da classe "+nomeDasClasses[i
			                                                              ]+" = "+(System.currentTimeMillis()-inicio));			
			
			System.out.println("\n\n\n############################################");
			if (daos[i].size()>0) {
				System.err.println("Erro");
			}
			daos[i]=null;
		}
					
	}
	
	public static void testeDeInsercao(DAOMidia midias,String classe) {
		long inicio=System.currentTimeMillis();
		for (int i = 0; i < numeroDeCiclos; i++) {
			midias.add(new Podcast("nome"+i, "titulo"+i, "descricao"+i, 1+i, "idioma"+i, "genero"+i, "autores"+i, 2000+i));
		}
		System.out.println("Tempo de inser��o na classe "+classe+" = "+(System.currentTimeMillis()-inicio));
	}
	
	public static void testeDeBuscaIP(DAOMidia midias,String classe) {
		long inicio=System.currentTimeMillis();
		for (int i = 0; i < numeroDeCiclos; i++) {
			midias.get("nome"+i);
		}
		System.out.println("Tempo de busca IP na classe "+classe+" = "+(System.currentTimeMillis()-inicio));
	}
	
	public static void testeDeBusca(DAOMidia midias,String classe) {
		long inicio=System.currentTimeMillis();
		for (int i = 0; i < numeroDeCiclos; i++) {
			midias.get("descricao"+i, 2);
		}
		System.out.println("Tempo de busca na classe "+classe+" = "+(System.currentTimeMillis()-inicio));
	}
	
	public static void testeRemover(DAOMidia midias,String classe) {
		long inicio=System.currentTimeMillis();
		for (int i = 0; i < numeroDeCiclos; i++) {
			midias.remover("nome"+i);
		}
		System.out.println("Tempo de remo��o na classe "+classe+" = "+(System.currentTimeMillis()-inicio));
	}
	
	public static void testeAlterar(DAOMidia midias,String classe) {
		long inicio=System.currentTimeMillis();
		for (int i = 0; i < numeroDeCiclos; i++) {
			midias.alterar(new Podcast("nome"+i, "titulo"+i, "descricao"+i, i+1, "idioma"+i, "genero"+i, "autores"+i, 3000+i), "nome"+i);
		}
		System.out.println("Tempo de altera��o na classe "+classe+" = "+(System.currentTimeMillis()-inicio));
	}
	
	
	
	
}
