/**
 * Classe respons�vel pelo processamento do banco de m�dias.
 */
package controler;

import java.io.Serializable;
import java.util.List;

import midias.Midia;

/**
 * 
 * @author Alex Dalosto
 * @author Cristina Benito
 * @author Leonardo Lopes
 * @author L�cio Jorge
 * @author Michel Marques
 * 
 */
public interface DAOMidia extends Serializable {

	/**
	 * Verifica se exite uma midia com determinado nome
	 * @param nome Nome do arquivo
	 * @return True se existe, false se n�o existe
	 */
	public boolean exist(String nome);

	/**
	 * M�todo respons�vel por adicionar uma m�dia, recebendo-a por par�metro.
	 * 
	 * @param midia
	 *            Midia a ser adicionada
	 * @return True se adicionou false se ja existe com esse nome
	 */
	public boolean add(Midia midia);

	/**
	 * M�todo respons�vel por remover uma m�dia, recebendo o nome por par�metro.
	 * 
	 * @param nome
	 *            Nome da midia a remover
	 * @return True se removeu false sen�o
	 */
	public boolean remover(String nome);

	/**
	 * M�todo respons�vel por retornar um m�dia, buscando-a pelo nome.
	 * 
	 * @param nome
	 *            Nome da midia desejada
	 * @return Midia se encontrar sen�o null
	 */
	public Midia get(String nome);

	/**
	 * M�todo respons�vel por alterar um m�dia, recebendo o tipo de m�dia e o
	 * nome por par�metro.
	 * 
	 * @param midia
	 *            Novos dados
	 * @param nome
	 *            Nome do arquivo a ser alterado
	 * @return True se dados alterados sen�o false
	 */
	public boolean alterar(Midia midia, String nome);

	/**
	 * M�todo respons�vel por salvar as m�dias em um arquivo, se houver
	 * altera��o nos dados.
	 * 
	 * @return True se salvou senao false.
	 */
	public boolean salvar();

	/**
	 * M�todo que verifica e retorna se o Array de m�dias est� vazio.
	 * 
	 * @return True se esta vasio senao false
	 */
	public boolean isEmpty();

	/**
	 * M�todo que verifica e retorna o tamanho do Array.
	 * 
	 * @return Numero de midias neste DAO
	 */
	public int size();

	/**
	 * M�todo respons�vel por listar dados das m�dias em paineis gr�ficos.
	 * 
	 * @return paineis AdicionadorListador contendo os dados das m�dias.
	 */
	public List<Midia> getAll();

	/**
	 * M�todo respons�vel por listar dados das m�dias especifiadas por
	 * par�metros em paineis gr�ficos.
	 * 
	 * @param str
	 *            valor do atributo buscado
	 * @param tipo
	 *            , indice do atributo a ser comparado.
	 * @return paineis AdicionadorListador contendo os dados das m�dias
	 *         especificadas.
	 */
	public List<Midia> get(String str, int tipo);

}
