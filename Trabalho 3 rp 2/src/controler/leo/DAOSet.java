package controler.leo;

import java.io.File;
import java.util.Collection;

import midias.Midia;

public class DAOSet extends DAOColections{

	public DAOSet(File file, Collection<Midia> midiasIn) {
		super(file, midiasIn);		
	}

	public boolean alterar(Midia midia, String nome) {

		for (Midia midiaI : midias) {
			if (midiaI.getNome().equals(nome)) {
				midias.remove(midiaI);
				midias.add(midia);
				temAlteracao = true;
				return true;
			}
		}
		return false;
	}
}
