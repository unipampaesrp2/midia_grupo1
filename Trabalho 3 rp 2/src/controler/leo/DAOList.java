package controler.leo;

import java.io.File;
import java.util.List;

import midias.Midia;

public class DAOList extends DAOColections {

	public DAOList(File file, List<Midia> midiasIn) {
		super(file, midiasIn);
	}

	public boolean alterar(Midia midia, String nome) {
		List<Midia> listaDeMidias=(List)midias;
		
		for (int i = 0; i < listaDeMidias.size(); i++) {
			if (listaDeMidias.get(i).getNome().equals(nome)) {
				listaDeMidias.set(i, midia);
				temAlteracao = true;
				return true;
			}
		}
		return false;
	}
}
