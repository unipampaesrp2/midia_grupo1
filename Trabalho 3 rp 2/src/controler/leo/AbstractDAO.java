package controler.leo;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import midias.Midia;
import controler.DAOMidia;

public abstract class AbstractDAO implements DAOMidia {
	protected Collection<Midia> midias;
	protected File file;
	protected boolean temAlteracao;

	
	public boolean isEmpty() {
		return midias.isEmpty();
	}

	
	public int size() {
		return midias.size();
	}

	
	public List<Midia> getAll() {
		return new ArrayList<Midia>(midias);
	}

	public List<Midia> get(String str, int tipo) {
		ArrayList<Midia> aux= new ArrayList<Midia>();		
		for (Midia midia : midias) {
			if (midia.equalsIn(str, tipo)) {
				aux.add(midia); 
			}
		}
		return aux;
		
		
	}

}
