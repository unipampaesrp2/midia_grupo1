package controler.leo.c;

import java.io.File;
import java.util.LinkedHashMap;

import midias.Midia;
import controler.leo.DAOMap;

public class DAOLinkedHashMap extends DAOMap {
	public DAOLinkedHashMap(File file) {
		super(file, new  LinkedHashMap<String, Midia>());
	}
	
}
