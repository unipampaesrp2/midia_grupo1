package controler.leo.c;

import java.io.File;
import java.util.HashSet;
import java.util.LinkedHashSet;

import midias.Midia;
import controler.leo.DAOSet;

public class DAOLinkedHashSet extends DAOSet{

	public DAOLinkedHashSet(File file) {
		super(file, new LinkedHashSet<Midia>());
	}

}
