package controler.leo.c;

import java.io.File;
import java.util.HashSet;

import midias.Midia;
import controler.leo.DAOSet;

public class DAOHashSet extends DAOSet{

	public DAOHashSet(File file) {
		super(file, new HashSet<Midia>());
	}
	
}
