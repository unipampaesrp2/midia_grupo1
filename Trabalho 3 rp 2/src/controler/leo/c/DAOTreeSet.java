package controler.leo.c;

import java.io.File;
import java.util.Comparator;
import java.util.TreeSet;

import midias.Midia;
import controler.leo.DAOSet;

public class DAOTreeSet extends DAOSet{

	public DAOTreeSet(File file) {
		super(file, new TreeSet<Midia>(new Comparator<Midia>() {

			@Override
			public int compare(Midia o1, Midia o2) {
				return o1.getNome().compareTo(o2.getNome());
			}
		}));
	}

}
