package controler.leo.c;

import java.io.File;
import java.util.ArrayList;

import midias.Midia;
import controler.leo.DAOList;


public class DAOArrayList extends DAOList {

	public DAOArrayList(File file){
		super(file, new ArrayList<Midia>());
	}

}
