package controler.leo.c;

import java.io.File;
import java.util.Comparator;
import java.util.TreeMap;

import midias.Midia;
import controler.leo.DAOMap;

public class DAOTreeMap  extends DAOMap {
	public DAOTreeMap(File file) {
		super(file, new  TreeMap<String, Midia>(new Comparator<String>() {

			@Override
			public int compare(String o1, String o2) {
				return o1.compareTo(o2);
			}
		}));
	}

}
