package controler.leo.c;

import java.io.File;
import java.util.LinkedList;

import midias.Midia;
import controler.leo.DAOList;
import controler.leo.DAOSet;

public class DAOLinkedList extends DAOSet {
	public DAOLinkedList(File file){
		super(file, new LinkedList<Midia>());
	}

}
