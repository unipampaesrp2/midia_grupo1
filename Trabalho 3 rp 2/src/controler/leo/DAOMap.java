package controler.leo;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import midias.Midia;

public class DAOMap extends AbstractDAO {
	private Map<String, Midia> map;

	public DAOMap(File file, Map<String, Midia> midiasIn) {
		if (!file.exists()) {
			throw new IllegalArgumentException("Arquivo n�o existe");
		}
		if (midiasIn == null) {
			throw new NullPointerException();
		}
		this.file = file;
		temAlteracao = false;
		FileInputStream fIs = null;
		ObjectInputStream in = null;
		try {
			
			fIs = new FileInputStream(file);
			in = new ObjectInputStream(fIs);

			map = (Map<String, Midia>) in.readObject();
			midias = map.values();
			in.close();
		} catch (Exception e) {
			map = midiasIn;
			midias = map.values();
		}
	}

	@Override
	public boolean exist(String nome) {
		return map.containsKey(nome);
	}

	@Override
	public boolean add(Midia midia) {
		if (exist(midia.getNome())) {
			return false;
		} else {
			map.put(midia.getNome(), midia);
			temAlteracao=true;
			return true;
		}
	}

	@Override
	public boolean remover(String nome) {
		if (map.remove(nome) == null) {
			return false;
		} else {
			temAlteracao=true;
			return true;
		}
	}

	@Override
	public Midia get(String nome) {
		return map.get(nome);
	}

	@Override
	public boolean alterar(Midia midia, String nome) {
		if (exist(nome)) {
			if (midia.getNome().equals(nome)) {
				map.put(nome, midia);	
			}else {
				map.remove(nome);
				map.put(midia.getNome(), midia);
			}
			temAlteracao=true;
			return true;
		} else {
			return false;
		}

	}

	@Override
	public List<Midia> get(String str, int tipo) {
		if (tipo == 0) {
			ArrayList<Midia> aux = new ArrayList<Midia>(1);
			Midia m = map.get(str);
			if (m != null) {
				aux.add(m);
			}
			return aux;
		} else {
			return super.get(str, tipo);
		}
	}

	@Override
	public boolean salvar() {
		if (temAlteracao) {
			try {
				FileOutputStream fOut = new FileOutputStream(file);
				ObjectOutputStream out = new ObjectOutputStream(fOut);

				out.writeObject(map);
				return true;
			} catch (Exception e) {
				return false;
			}

		} else {
			return false;
		}
	}

}
