package controler.leo;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Collection;
import java.util.Iterator;

import midias.Midia;

public abstract class DAOColections extends AbstractDAO {
	
	/**
	 * Contrutor da Classe DAOMidia que recebe por par�metro um arquivo e um
	 * tipo de dado.
	 * 
	 * @param file Arquivo onde os dados est�o/ser�o salvos
	 * @param fabricaDeMidias FabricaDeMidias que determinaram o tipo de DAO
	 * @throws IOException Erro de acesso a arquivo.
	 */
	public DAOColections(File file,Collection<Midia> midiasIn) {
		if (!file.exists()) {
			throw new IllegalArgumentException("Arquivo n�o existe");
		}
		if (midiasIn==null) {
			throw new NullPointerException();
		}
		this.file = file;
		temAlteracao = false;
		
		FileInputStream fIs=null;
		ObjectInputStream in = null;
		try {			
			fIs = new FileInputStream(file);
			in=new ObjectInputStream(fIs);
		} catch (Exception e) {		
		}
		try {
			midias=(Collection<Midia>)in.readObject();
		} catch (Exception e) {
			midias=midiasIn;
		}
	}
	
	public boolean add(Midia midia) {
		if (exist(midia.getNome())) {
			return false;
		} else {
			midias.add(midia);
			temAlteracao = true;
			return true;
		}
	}


	public boolean remover(String nome) {
//		Midia m = get(nome);
//		if (m == null) {
//			return false;
//		} else {
//			midias.remove(m);
//			temAlteracao = true;
//			return true;
//		}		
		for (Iterator<Midia> inter=midias.iterator();inter.hasNext();) {
			Midia midia=inter.next();
			if (midia.getNome().equals(nome)) {
				inter.remove();
				return true;
			}
		}
		return false;
	}
	
	public Midia get(String nome) {
		for (Midia midia : midias) {
			if (midia.getNome().equals(nome)) {
				return midia;
			}
		}
		return null;
	}

	public boolean exist(String nome) {
		return get(nome) != null;
	}
	
	public boolean salvar() {
		if (temAlteracao) {
			try {
				FileOutputStream fOut=new FileOutputStream(file);
				ObjectOutputStream out=new ObjectOutputStream(fOut);
				
				out.writeObject(midias);
								
				return true;
			} catch (Exception e) {
				return false;
			}
			
		} else {
			return false;
		}
		
	}


}
