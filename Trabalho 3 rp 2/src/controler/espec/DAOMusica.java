package controler.espec;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Vector;

import midias.Midia;
import controler.DAOMidia;

public class DAOMusica implements DAOMidia {

	private Collection<Midia> midias;
	private File file;
	private boolean temAlteracao;
	
	
	
	
	public DAOMusica(File file) {
		if (!file.exists()) {
			throw new IllegalArgumentException("Arquivo n�o existe");
		}
		
		this.file = file;
		temAlteracao = false;
		
		FileInputStream fIs=null;
		ObjectInputStream in = null;
		try {			
			fIs = new FileInputStream(file);
			in=new ObjectInputStream(fIs);
		
			midias=(Collection<Midia>)in.readObject();
		} catch (Exception e) {
			midias=new Vector<Midia>();
		}
	}
	
	public boolean alterar(Midia midia, String nome) {
		List<Midia> listaDeMidias=(List)midias;
		
		for (int i = 0; i < listaDeMidias.size(); i++) {
			if (listaDeMidias.get(i).getNome().equals(nome)) {
				listaDeMidias.set(i, midia);
				temAlteracao = true;
				return true;
			}
		}
		return false;
	}

	public boolean add(Midia midia) {
		if (exist(midia.getNome())) {
			return false;
		} else {
			midias.add(midia);
			temAlteracao = true;
			return true;
		}
	}


	public boolean remover(String nome) {
//		Midia m = get(nome);
//		if (m == null) {
//			return false;
//		} else {
//			midias.remove(m);
//			temAlteracao = true;
//			return true;
//		}		
		for (Iterator<Midia> inter=midias.iterator();inter.hasNext();) {
			Midia midia=inter.next();
			if (midia.getNome().equals(nome)) {
				inter.remove();
				return true;
			}
		}
		return false;
	}
	
	public Midia get(String nome) {
		for (Midia midia : midias) {
			if (midia.getNome().equals(nome)) {
				return midia;
			}
		}
		return null;
	}

	public boolean exist(String nome) {
		return get(nome) != null;
	}
	
	public boolean salvar() {
		if (temAlteracao) {
			try {
				FileOutputStream fOut=new FileOutputStream(file);
				ObjectOutputStream out=new ObjectOutputStream(fOut);
				
				out.writeObject(midias);
								
				return true;
			} catch (Exception e) {
				return false;
			}
			
		} else {
			return false;
		}
		
	}


	
	
	
	
	
	public boolean isEmpty() {
		return midias.isEmpty();
	}

	
	public int size() {
		return midias.size();
	}

	
	public List<Midia> getAll() {
		return new ArrayList<Midia>(midias);
	}

	public List<Midia> get(String str, int tipo) {
		ArrayList<Midia> aux= new ArrayList<Midia>();		
		for (Midia midia : midias) {
			if (midia.equalsIn(str, tipo)) {
				aux.add(midia); 
			}
		}
		return aux;
		
		
	}

}
