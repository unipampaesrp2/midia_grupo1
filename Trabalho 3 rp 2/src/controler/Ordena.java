/**
 * Classe respons�vel pelos m�todos de ordena��o do banco de m�dias.
 */
package controler;

import interfaces.ComparaPor;

import java.util.List;
import java.util.Random;

/**
 * 
 * @author Alex Dalosto
 * @author Cristina Benito
 * @author Leonardo Lopes
 * @author L�cio Jorge
 * @author Michel Marques
 */
public class Ordena {

	/**
	 * M�todo respons�vel pela ordena��o atrav�s do Bubble Sort. Ordena
	 * Empurrando o maior valor para o final da lista.
	 * 
	 * @param lista Lista a ser ordenada
	 * @param tipo Indice do atributo a ser considerado
	 */
	public static void bubbleSort(List<ComparaPor> lista, int tipo) {
		ComparaPor aux;
		for (int i = 0; i < lista.size(); i++)
			for (int j = 0; j < lista.size() - 1; j++)
				if (lista.get(j).compareToBy(lista.get(j + 1), tipo) > 0) {

					aux = lista.get(j);
					lista.set(j, lista.get(j + 1));
					lista.set(j + 1, aux);
				}
	}

	/**
	 * M�todo respons�vel pela ordena��o atrav�s do Selection Sort. Ordena
	 * selecionando omenor valor e o coloca na primeira posi��o, repete a
	 * operera��o para o segundo valor e assim sucessivamente.
	 * 
	 * @param lista Lista a ser ordenada
	 * @param tipo Indice do atributo a ser considerado
	 */
	public static void selectionSort(List<ComparaPor> lista, int tipo) {
		for (int i = 0; i < lista.size(); i++) {
			int menor = i;
			for (int j = i + 1; j < lista.size(); j++) {
				if (lista.get(j).compareToBy(lista.get(menor), tipo) < 0) {
					menor = j;
				}
			}
			ComparaPor aux = lista.get(i);
			lista.set(i, lista.get(menor));
			lista.set(menor, aux);

		}

	}

	/**
	 * M�todo respons�vel pela ordena��o atrav�s do Insertion Sort. Percorre o a
	 * lista da esquerda para a direita e a medida que avan�a, vai deixando sua
	 * esquerda ordenada.
	 * 
	 * @param lista Lista a ser ordenada
	 * @param t Indice do atributo a ser considerado
	 */
	public static void insertSort(List<ComparaPor> lista, int t) {
		for (int j = 1; j < lista.size(); j++) {
			int i = j - 1;
			ComparaPor aux = lista.get(j);

			while (i >= 0 && aux.compareToBy(lista.get(i), t) < 0) {
				lista.set(i + 1, lista.get(i));
				i--;
			}
			lista.set(i + 1, aux);
		}

	}


////<<<<<<< HEAD
//	static public void mergeSort(List<ComparaPor> conjunto,
//			List<ComparaPor> aux, int esquerda, int direita, int tipo) {
////=======
////	static public void mergeSort(List<ComparaPor> conjunto, int esquerda,
////			int direita, int tipo) {
////		int meio;
////		if (direita > esquerda && direita< conjunto.size() && esquerda < conjunto.size()) {
////			meio = (direita + esquerda) / 2;
////
////			mergeSort(conjunto, esquerda, meio, tipo);
////			mergeSort(conjunto, (meio + 1), direita, tipo);
////>>>>>>> branch 'master' of https://llop@bitbucket.org/unipampaesrp2/midia_grupo1.git
//
//		if (direita <= esquerda + 1)
//			return;
//		int meio = esquerda + (direita - esquerda) / 2;
//		mergeSort(conjunto, aux, esquerda, meio, tipo);
//		mergeSort(conjunto, aux, meio, direita, tipo);
//		doMerge(conjunto, aux, esquerda, meio, direita, tipo);
//
//	}
//
//
//	@SuppressWarnings("unused")
//	static private void mergeSort(List<ComparaPor> conjunto, int esquerda,
//			int direita, int tipo) {
//		int meio;
//		if (direita > esquerda && direita < conjunto.size()
//				&& esquerda < conjunto.size()) {
//			meio = (direita + esquerda) / 2;
//
//			mergeSort(conjunto, esquerda, meio, tipo);
//			mergeSort(conjunto, (meio + 1), direita, tipo);
//
////			auxMerge(conjunto, esquerda, (meio + 1), direita, tipo);
//		}
//
//	}
//
//
//	static private void doMerge(List<ComparaPor> conjunto, List<ComparaPor>aux,  int esquerda,
//			int meio, int direita, int tipo) {
//
//
//		 for (int k = esquerda; k < direita; k++) aux.set(k,conjunto.get(k) );    //   [k] = conjunto[k];
//		 int i = esquerda, j = meio;
//		 for (int k = esquerda; k < direita; k++)
//		 if (i >= meio) conjunto.set(k, aux.get(j++));                           //conjunto[k] = aux[j++];
//		 else if (j >= direita) 	conjunto.set(k, aux.get(i++)); 				//conjunto[k] = aux[i++];
//		 else if  (less(aux.get(j),aux.get(i)))  conjunto.set(k, aux.get(j++));                // (less(aux[j], aux[i])) conjunto[k] = aux[j++];
//		 else conjunto.set(k, aux.get(i++));					//conjunto[k] = aux[i++];
//
//
////		while ((esquerda <= esquerdaEndTemp) && (meio <= direita)
////				&& tempInicio < temp.size() && esquerda < conjunto.size()
////				&& meio < conjunto.size()) {
////			if (conjunto.get(esquerda).compareToBy(conjunto.get(meio), tipo) <= 0) {
////				temp.set(tempInicio++, conjunto.get(esquerda++));
////			} else {
////				temp.set(tempInicio++, conjunto.get(meio++));
////			}
////		}
//	}	
//	
//
//
//	private static boolean less(ComparaPor comparaPor, ComparaPor comparaPor2) {
//		//m�todo que vou ajustar
//		return false;
//	}

	/**
	 * M�todo respons�vel pela ordena��o atrav�s do Quick Sort. Escolhe um
	 * elemento da lista, denominado piv� e Rearranja a lista de forma que todos
	 * os elementos anteriores ao piv� sejam menores que ele, e todos os
	 * elementos posteriores ao piv� sejam maiores que ele.
	 * 
	 * @param lista Lista a ser ordenada
	 * @param tipo Indice do atributo a ser considerado
	 */
	public static void quickSort(List<ComparaPor> lista, int tipo) {
		quickSort(lista, 0, lista.size() - 1, tipo);
	}

	private static void quickSort(List<ComparaPor> lista, int inicio, int fim,
			int tipo) {
		if (inicio < fim) {
			int posicaoPivo = separar(lista, inicio, fim, tipo);
			quickSort(lista, inicio, posicaoPivo - 1, tipo);
			quickSort(lista, posicaoPivo + 1, fim, tipo);
		}
	}

	private static int separar(List<ComparaPor> lista, int inicio, int fim,
			int tipo) {
		ComparaPor pivo = lista.get(inicio);
		int i = inicio + 1, f = fim;

		while (i <= f) {
			if (lista.get(i).compareToBy(pivo, tipo) <= 0)
				i++;
			else if (pivo.compareToBy(lista.get(f), tipo) < 0) {
				f--;
			} else {

				ComparaPor troca = lista.get(i);
				lista.set(i, lista.get(f));
				lista.set(f, troca);

				i++;
				f--;
			}
		}
		lista.set(inicio, lista.get(f));
		lista.set(f, pivo);
		return f;
	}

	/**
	 * Usa metodo de ordena��o aleatorio pra ordenar conforme os criterios
	 * @param lista Lista a ser ordenada
	 * @param tipo Indice do atributo a ser considerado
	 * @return Nome do metodo de orden��o
	 */
	public static String sort(List<ComparaPor> lista, int tipo) {
		Random rad = new Random();

		switch (rad.nextInt(4)) {
		case 0:
			quickSort(lista, tipo);
			return "Quick";
		case 1:
			selectionSort(lista, tipo);
			return "Selection";
		case 2:
			insertSort(lista, tipo);
			return "Inserction";
			// case 3:
			// mergeSort(lista, tipo);
			// return "Merge";
		default:
			bubbleSort(lista, tipo);
			return "Bubble";
		}

	}

}
