package anotacoes;

import interfaces.ComparaPor;

/**
 * As contantes servem de axiliares a utiliza��o de metodos da interface {@link ComparaPor} em outros projetos que nao utilizem essa arquitetura, em que n�o � necessario utiliza-las
 * @author Leonardo
 *
 */
public @interface Explica��oDeConstantes {

}
