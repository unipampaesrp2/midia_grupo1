package anotacoes;

import midias.Audio;
import midias.Audiolivro;
import interfaces.ComparaPor;

/**
 * O metodo numero de atributos visa facilitar modifica��es no sistema uma vez que as "constantes" tem valor  variavel isto � quando oprograma � iniciado ele analiza se ouveram aletra�oes na classe pai e reajusata o valor das constantes se necessario, alem disso facilitam no momento de implementa�l�o dos metodos da interface {@link ComparaPor} uma vez que ajudam a determinar se o indice passsado por parametro � invalido se deve ser tratado pela superclasse ou pela propia classe.
 * Seque um trecho da classe {@link Audiolivro} para exemplifica��o:<br><br>
 * <pre>public int compareToBy(ComparaPor obj, int tipo) {
	if (tipo &gt;= numeroDeAtributos()) {
		throw new IllegalArgumentException("Indice de compara��o invalido");
	}<br>
	if (tipo &lt; Audio.numeroDeAtributos()) {
		return super.compareToBy(obj, tipo);
	} else {
		if (tipo == LOCAL) {
			return local.compareTo(((Audiolivro) obj).getLocal());
		} else {// EDITORA<br>
			return editora.compareTo(((Audiolivro) obj).getEditora());
		}
	}
}</pre>
 * @author Leonardo
 *
 */
public @interface Explica��oDoNumeroDeAtributos {

}
