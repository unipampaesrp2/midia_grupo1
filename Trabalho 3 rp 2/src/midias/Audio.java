/**
 * Classe abstrata que implementa atributos e m�todos comuns ao banco de m�dias. 
 */
package midias;

import interfaces.ComparaPor;

import java.util.List;

import anotacoes.Explica��oDeConstantes;
import anotacoes.Explica��oDoNumeroDeAtributos;

public abstract class Audio extends Midia {

	protected int duracao;

	protected String idioma;

	protected String genero;

	protected String autores;

	protected int ano;

	private static final int NUMERO_DE_ATRIBUTOS = 5;
	/**
	 * Indice do atrubuto duracao @see {@link Explica��oDeConstantes}
	 */
	public static final int DURACAO = Midia.numeroDeAtributos();
	/**
	 * Indice do atrubuto idioma @see {@link Explica��oDeConstantes}
	 */
	public static final int IDIOMA = Midia.numeroDeAtributos() + 1;
	/**
	 * Indice do atrubuto genero @see {@link Explica��oDeConstantes}
	 */
	public static final int GENERO = Midia.numeroDeAtributos() + 2;
	/**
	 * Indice do atrubuto autores @see {@link Explica��oDeConstantes}
	 */
	public static final int AUTORES = Midia.numeroDeAtributos() + 3;
	/**
	 * Indice do atrubuto ano @see {@link Explica��oDeConstantes}
	 */
	public static final int ANO = Midia.numeroDeAtributos() + 4;

	/**
	 * Controi midia com todos atributos
	 * 
	 * @param nome
	 *            Nome da midia
	 * @param titulo
	 *            Titulo da midia
	 * @param descricao
	 *            Descri��o da midia
	 * @param duracao
	 *            Dura��o do audio
	 * @param idioma
	 *            Idioma do audio
	 * @param genero
	 *            Genero do audio
	 * @param autores
	 *            Autores do audio
	 * @param ano
	 *            Ano do audio
	 */
	public Audio(String nome, String titulo, String descricao, int duracao,
			String idioma, String genero, String autores, int ano) {
		super(nome, titulo, descricao);
		this.duracao = duracao;
		this.idioma = idioma;
		this.genero = genero;
		this.autores = autores;
		this.ano = ano;

	}

	/**
	 * Costroi baseando-se em uma lista de atributos
	 * 
	 * @param val
	 *            Lista que contem Strings que representam os atributos,
	 *            ordenados conforme os indices, segue a mesma ordem do metodo
	 *            toSave desta forma quem utiliza a classe n�o precisa se
	 *            preocupar.
	 */
	public Audio(List<String> val) {
		super(val);
		duracao = Integer.parseInt(val.get(DURACAO));
		idioma = val.get(IDIOMA);
		genero = val.get(GENERO);
		autores = val.get(AUTORES);
		ano = Integer.parseInt(val.get(ANO));

	}

	@Override
	public String toString() {
		return super.toString() + duracao + "\n" + idioma + "\n" + genero + "\n"
				+ autores + "\n" + ano + "\n";
	}

	@Override
	public int compareToBy(ComparaPor obj, int tipo) {
		if (tipo >= numeroDeAtributos()) {
			throw new IllegalArgumentException("Indice de compara��o invalido");
		}
		if (tipo < Midia.numeroDeAtributos()) {
			return super.compareToBy(obj, tipo);
		} else {
			if (tipo == DURACAO) {
				return duracao - ((Audio) obj).getDuracao();
			}
			if (tipo == IDIOMA) {
				return idioma.compareTo(((Audio) obj).getIdioma());
			}
			if (tipo == GENERO) {
				return genero.compareTo(((Audio) obj).getGenero());
			}
			if (tipo == AUTORES) {
				return autores.compareTo(((Audio) obj).getAutores());
			} else {// ANO
				return ano - ((Audio) obj).getAno();
			}
		}
	}

	@Override
	public boolean equalsIn(String str, int tipo) {
		if (tipo >= numeroDeAtributos()) {
			throw new IllegalArgumentException("Indice de compara��o invalido");
		}
		if (tipo < Midia.numeroDeAtributos()) {
			return super.equalsIn(str, tipo);
		} else {
			if (tipo == DURACAO) {

				try {
					int m = 0;
					int s = 0;
					String strDuracao = str;
					int i = 0;

					while (i < strDuracao.length()
							&& Character.isDigit(strDuracao.charAt(i))) {
						i++;
					}
					if (i == strDuracao.length()) {
						throw new IllegalArgumentException(
								"Dura��o mal formatada use ':' para separa minutos de segundos (mm:ss)");
					}
					m = Integer.parseInt(strDuracao.substring(0, i));
					if (m < 0) {
						throw new IllegalArgumentException(
								"Minutos nao podem ser negativos");
					}
					if (strDuracao.charAt(i) != ':') {//
						throw new IllegalArgumentException(
								"Dura��o mal formatada use ':' para separa minutos de segundos (mm:ss)");
					}
					s = Integer.parseInt(strDuracao.substring(i + 1));
					if (s > 60) {
						throw new IllegalArgumentException(
								"O n�mero de segundos deve estar entre 0 e 60");
					}
					if (s < 0) {
						throw new IllegalArgumentException(
								"Segundos nao podem ser negativos");
					}

					return duracao == s + (m * 60);

				} catch (NumberFormatException e) {
					throw new IllegalArgumentException("Dura��o mal formatada");
				}

			}
			if (tipo == IDIOMA) {
				return idioma.equals(str);
			}
			if (tipo == GENERO) {
				return genero.equals(str);
			}
			if (tipo == AUTORES) {
				return autores.equals(str);
			} else {// ANO
				int aux = 0;
				try {
					aux = Integer.parseInt(str);
				} catch (NumberFormatException e) {
					throw new IllegalArgumentException(
							"Formato do ano invalido");
				}
				return ano == aux;
			}
		}
	}

	/**
	 * M�todo respons�vel por capturar a dura��o de um �udio e retorn�-la.
	 * 
	 * @return dura��o.
	 */
	public int getDuracao() {
		return duracao;
	}

	/**
	 * M�todo respons�vel por capturar o idioma de um �udio e retorn�-lo.
	 * 
	 * @return idioma.
	 */
	public String getIdioma() {
		return idioma;
	}

	/**
	 * M�todo respons�vel por capturar o g�nero de um �udio e retorn�-lo.
	 * 
	 * @return g�nero.
	 */
	public String getGenero() {
		return genero;
	}

	/**
	 * M�todo respons�vel por capturar os autores de um �udio e retorn�-los.
	 * 
	 * @return autores.
	 */
	public String getAutores() {
		return autores;
	}

	/**
	 * M�todo respons�vel por capturar o ano de um �udio e retorn�-lo.
	 * 
	 * @return ano.
	 */
	public int getAno() {
		return ano;
	}

	/**
	 * M�todo que possibilita a altera��o de dura��o de um a�dio.
	 * 
	 * @param duracao
	 *            dura��o.
	 */
	public void setDuracao(int duracao) {
		this.duracao = duracao;
	}

	/**
	 * M�todo que possibilita a altera��o de idioma de um a�dio.
	 * 
	 * @param idioma
	 *            idioma
	 */
	public void setIdioma(String idioma) {
		this.idioma = idioma;
	}

	/**
	 * M�todo que possibilita a altera��o de g�nero de um a�dio.
	 * 
	 * @param genero
	 *            G�nero.
	 */
	public void setGenero(String genero) {
		this.genero = genero;
	}

	/**
	 * M�todo que possibilita a altera��o dos autores de um a�dio.
	 * 
	 * @param autores
	 *            autores.
	 */
	public void setAutores(String autores) {
		this.autores = autores;
	}

	/**
	 * M�todo que possibilita a altera��o do ano de um a�dio.
	 * 
	 * @param ano
	 *            ano.
	 */
	public void setAno(int ano) {
		this.ano = ano;
	}

	/**
	 * Obtem numero de atributos nesta classe que sao indexaveis por metodos da
	 * interface {@link ComparaPor}, funciona conforme
	 * {@link Explica��oDoNumeroDeAtributos}
	 * 
	 * @return Numero de atributros de instancia desta classe que sao indexaveis
	 */
	protected static int numeroDeAtributos() {
		return Midia.numeroDeAtributos() + NUMERO_DE_ATRIBUTOS;
	}

	/**
	 * Retorna os nomes dos atributos da midia
	 * 
	 * @return Lista com nome dos atributos da midia, auxiliando o reuso nessa
	 *         arquitetura, e evitando o uso das constantes uma ves que os nomes
	 *         dos atributos estao na mesma ordem das constantes
	 */
	public static List<String> nomeDosAtributos() {
		List<String> atributos = Midia.nomeDosAtributos();

		atributos.add("Duracao");
		atributos.add("Idioma");
		atributos.add("Genero");
		atributos.add("Autores");
		atributos.add("Ano");

		return atributos;
	}
}
