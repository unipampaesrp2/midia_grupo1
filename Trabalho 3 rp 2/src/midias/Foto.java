/**
 * Classe respons�vel pela m�dia Foto que estende a classe Midia.
 */
package midias;

import interfaces.ComparaPor;

import java.util.Date;
import java.util.List;

import anotacoes.Explica��oDoNumeroDeAtributos;
import view.base.AdicionadorListador;

/**
 * @author Alex Dalosto
 * @author Cristina Benito
 * @author Leonardo Lopes
 * @author L�cio Jorge
 * @author Michel Marques
 */
public class Foto extends Midia {

	private String pessoas;

	private String fotografo;

	private String local;

	private Date data;

	private int hora;

	private static final int NUMERO_DE_ATRIBUTOS = 5;

	public static final int PESSOAS = Midia.numeroDeAtributos();
	public static final int FOTOGRAFO = Midia.numeroDeAtributos() + 1;
	public static final int LOCAL = Midia.numeroDeAtributos() + 2;
	public static final int DATA = Midia.numeroDeAtributos() + 3;
	public static final int HORA = Midia.numeroDeAtributos() + 4;

	/**
	 * M�todo construtor da classe Foto, que recebe por par�metro os atributos
	 * instanciados com os construtores da classe abstrata M�dia.
	 * 
	 * @param nome
	 *            Nome.
	 * @param titulo
	 *            T�tulo.
	 * @param descricao
	 *            Descri��o.
	 * @param pessoas
	 *            Pessoas envolvidas.
	 * @param fotografo
	 *            Fot�grafo.
	 * @param local
	 *            Local da foto.
	 * @param data
	 *            Data.
	 * @param hora
	 *            Hora.
	 */
	public Foto(String nome, String titulo, String descricao, String pessoas,
			String fotografo, String local, Date data, int hora) {
		super(nome, titulo, descricao);
		this.pessoas = pessoas;
		this.fotografo = fotografo;
		this.local = local;
		this.data = data;
		this.hora = hora;
	}

	/**
	 * M�todo construtor da classe Partitura, que recebe lista de String por
	 * par�metro, instancia com o construtor da classe FabricaDeM�dias e
	 * inicializa o atributo pessoas, fotogr�fo, local, data e hora, pr�prios da
	 * classe.
	 * 
	 * @param val
	 *            Lista que cont�m Strings que representam os atributos,
	 *            ordenados conforme os �ndices.
	 */
	public Foto(List<String> val) {
		super(val);
		pessoas = val.get(PESSOAS);
		fotografo = val.get(FOTOGRAFO);
		local = val.get(LOCAL);
		data = new Date(Long.parseLong(val.get(DATA)));
		hora = Integer.parseInt(val.get(HORA));
	}

	@Override
	public String toString() {
		return super.toString() + pessoas + "\n" + fotografo + "\n" + local
				+ "\n" + data.getTime() + "\n" + hora + "\n";
	}

	@Override
	public AdicionadorListador toPanel() {
		// return new PrototipoFotoGrid (this);
		return null;
	}

	@Override
	public int compareToBy(ComparaPor obj, int tipo) {
		if (tipo >= numeroDeAtributos()) {
			throw new IllegalArgumentException("Indice de compara��o invalido");
		}
		if (tipo < Midia.numeroDeAtributos()) {
			return super.compareToBy(obj, tipo);
		} else {
			if (tipo == PESSOAS) {
				return pessoas.compareTo(((Foto) obj).getPessoas());
			}
			if (tipo == FOTOGRAFO) {
				return fotografo.compareTo(((Foto) obj).getFotografo());
			}
			if (tipo == LOCAL) {
				return local.compareTo(((Foto) obj).getLocal());
			}
			if (tipo == DATA) {
				return data.compareTo(((Foto) obj).getData());
			} else {// hora
				return hora - ((Foto) obj).getHora();
			}
		}
	}

	/**
	 * M�todo respons�vel por capturar os nomes das pessoas nas fotos e
	 * retorn�-los.
	 * 
	 * @return Pessoas.
	 */
	public String getPessoas() {
		return pessoas;
	}

	/**
	 * M�todo respons�vel por capturar o nome do fot�grafo e retorn�-lo.
	 * 
	 * @return fot�grafo
	 */
	public String getFotografo() {
		return fotografo;
	}

	/**
	 * M�todo respons�vel por capturar o nome de um determinado local e
	 * retorn�-lo.
	 * 
	 * @return local.
	 */
	public String getLocal() {
		return local;
	}

	/**
	 * M�todo respons�vel por capturar uma data e retorn�-la.
	 * 
	 * @return data.
	 */
	public Date getData() {
		return data;
	}

	/**
	 * M�todo respons�vel por capturar uma hora e retorn�-la.
	 * 
	 * @return hora.
	 */
	public int getHora() {
		return hora;
	}

	/**
	 * M�todo que possibilita a altera��o dos nomes das pessoas nas fotos.
	 * 
	 * @param pessoas
	 *            Pessoas.
	 */
	public void setPessoas(String pessoas) {
		this.pessoas = pessoas;
	}

	/**
	 * M�todo que possibilita alterar o nome de um fot�grafo.
	 * 
	 * @param fotografo
	 *            Fot�grafo.
	 */
	public void setFotografo(String fotografo) {
		this.fotografo = fotografo;
	}

	/**
	 * M�todo que possibilita alterar um determinado local.
	 * 
	 * @param local
	 *            Local.
	 */
	public void setLocal(String local) {
		this.local = local;
	}

	/**
	 * M�todo que possibilita alterar uma determinada data.
	 * 
	 * @param data
	 *            Data.
	 */
	public void setData(Date data) {
		this.data = data;
	}

	/**
	 * M�todo que possibilita aterar uma determinada hora.
	 * 
	 * @param hora
	 *            Hora.
	 */
	public void setHora(int hora) {
		this.hora = hora;
	}

	@SuppressWarnings("deprecation")
	@Override
	public boolean equalsIn(String str, int tipo) {
		if (tipo >= numeroDeAtributos()) {
			throw new IllegalArgumentException("Indice de compara��o invalido");
		}
		if (tipo < Midia.numeroDeAtributos()) {
			return super.equalsIn(str, tipo);
		} else {
			if (tipo == PESSOAS) {
				return pessoas.equals(str);
			}
			if (tipo == FOTOGRAFO) {
				return fotografo.equals(str);
			}
			if (tipo == LOCAL) {
				return local.equals(str);
			}
			if (tipo == DATA) {
				Date dataAux = new Date();

				try {
					dataAux.setDate(Integer.parseInt(str.substring(0, 2)));
					dataAux.setMonth(Integer.parseInt(str.substring(3, 5)) - 1);
					dataAux.setYear(Integer.parseInt(str.substring(6)));
				} catch (Exception e) {
					throw new IllegalArgumentException("Data invalida");
				}

				return data.equals(dataAux);
			} else {// hora
				int aux = 0;
				try {
					aux = Integer.parseInt(str);
				} catch (NumberFormatException e) {
					throw new IllegalArgumentException(
							"Formato do ano invalido");
				}
				return hora == aux;
			}
		}
	}

	/**
	 * Obt�m numero de atributos nesta classe que s�o index�veis por m�todos da
	 * interface {@link ComparaPor}, funciona conforme
	 * {@link Explica��oDoNumeroDeAtributos}
	 * 
	 * @return N�mero de atributros de inst�ncia desta classe que s�o
	 *         index�veis.
	 */
	protected static int numeroDeAtributos() {
		return Midia.numeroDeAtributos() + NUMERO_DE_ATRIBUTOS;
	}

	/**
	 * Retorna os nomes dos atributos da m�dia
	 * 
	 * @return Lista com nome dos atributos da m�dia. Essa arquitetura auxilia
	 *         no re�so evitando o uso das constantes, uma vez que os nomes dos
	 *         atributos est�o na mesma ordem das constantes.
	 */
	public static List<String> nomeDosAtributos() {
		List<String> atributos = Midia.nomeDosAtributos();

		atributos.add("Pessoas");
		atributos.add("Fotografo");
		atributos.add("Local");
		atributos.add("Data");
		atributos.add("Hora");

		return atributos;
	}

	/**
	 * Retorna f�brica de m�dias
	 * 
	 * @return F�brica de m�dias respons�vel por receber uma lista de Strings e
	 *         instanciar um subtipo de m�dia
	 */
	public static FabricaDeMidias getFabricaDeMidias() {
		return new FabricaDeMidias() {
			@Override
			public Midia criaMidia(List<String> val) {
				return new Foto(val);
			}
		};
	}
}
