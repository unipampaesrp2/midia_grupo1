package midias;

import interfaces.ComparaPor;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

import anotacoes.Explica��oDeConstantes;
import anotacoes.Explica��oDoNumeroDeAtributos;
import view.base.AdicionadorListador;

/**

 * 
 * 
 * 
 * Classe respon�avel por prepresentar caracteristiccas basicas de uma midia e
 * conter metodos comuns
 * 
 * @author Alex Dalosto
 * @author Cristina Benito
 * @author Leonardo Lopes
 * @author L�cio Jorge
 * @author Michel Marques
 *
 */
public abstract class Midia implements ComparaPor, Serializable {

	protected String nome;

	protected String titulo;

	protected String descricao;

	private static final int NUMERO_DE_ATRIBUTOS = 3;
	/**
	 * Indice do atrubuto nome @see {@link Explica��oDeConstantes}
	 */
	public static final int NOME = 0;
	/**
	 * Indice do atrubuto titulo @see {@link Explica��oDeConstantes}
	 */
	public static final int TITULO = 1;
	/**
	 * Indice do atrubuto descri��o @see {@link Explica��oDeConstantes}
	 */
	public static final int DESCRICAO = 2;

	/**
	 * Controi midia com todos atributos
	 * 
	 * @param nome
	 *            Nome da midia
	 * @param titulo
	 *            Titulo da midia
	 * @param descricao
	 *            Descri��o da midia
	 */
	public Midia(String nome, String titulo, String descricao) {
		this.nome = nome;
		this.titulo = titulo;
		this.descricao = descricao;
	}

	/**
	 * Costroi baseando-se em uma lista de atributos
	 * 
	 * @param val
	 *            Lista que contem Strings que representam os atributos,
	 *            ordenados conforme os indices, segue a mesma ordem do metodo
	 *            toSave desta forma quem utiliza a classe n�o precisa se
	 *            preocupar.
	 */
	public Midia(List<String> val) {
		nome = val.get(NOME);
		titulo = val.get(TITULO);
		descricao = val.get(DESCRICAO);
	}

	/**
	 * Gera string para salvar em arquivo
	 * 
	 * @return String para salvar em arquivo com os atributos ordenados conforme
	 *         os indices, o que facilita a leitura pois sava na mesma ordem que
	 *         dela
	 */
	public String toString() {
		return nome + "\n" + titulo + "\n" + descricao + "\n";
	}

	/**
	 * Obtem painel listados de dados
	 * 
	 * @return Painel respon�avel por manipular os dados da midia e interagir
	 *         com o DAO
	 */
	public abstract AdicionadorListador toPanel();

	public int compareToBy(ComparaPor obj, int tipo) {
		if (tipo >= numeroDeAtributos()) {
			throw new IllegalArgumentException("Indice de compara��o invalido");
		}
		if (tipo < 0) {
			throw new IllegalArgumentException(
					"Indice de compara��o invalido (Negativo)");
		} else {
			if (tipo == NOME) {
				return nome.compareTo(((Midia) obj).getNome());
			}
			if (tipo == TITULO) {
				return titulo.compareTo(((Midia) obj).getTitulo());
			} else {// DESCRICAO
				return descricao.compareTo(((Midia) obj).getDescricao());
			}
		}
	}

	public boolean equalsIn(String str, int tipo) {
		if (tipo >= numeroDeAtributos()) {
			throw new IllegalArgumentException("Indice de compara��o invalido");
		}
		if (tipo < 0) {
			throw new IllegalArgumentException(
					"Indice de compara��o invalido (Negativo)");
		} else {
			if (tipo == NOME) {
				return nome.equals(str);
			}
			if (tipo == TITULO) {
				return titulo.equals(str);
			} else {// DESCRICAO
				return descricao.equals(str);
			}
		}
	}

	/**
	 * M�todo respons�vel por capturar o nome de uma m�dia e retorn�-lo.
	 * 
	 * @return nome.
	 */
	public String getNome() {
		return nome;
	}

	/**
	 * M�todo respons�vel por capturar o t�tulo de uma m�dia e retorn�-lo.
	 * 
	 * @return T�tulo.
	 */
	public String getTitulo() {
		return titulo;
	}

	/**
	 * M�todo respons�vel por capturar o descri��o de uma m�dia e retorn�-la.
	 * 
	 * @return Descri��o.
	 */
	public String getDescricao() {
		return descricao;
	}

	/**
	 * M�todo que possibilita alterar um nome.
	 * 
	 * @param nome
	 *            nome.
	 */
	public void setNome(String nome) {
		this.nome = nome;
	}

	/**
	 * M�todo que possibilita alterar um t�tulo.
	 * 
	 * @param titulo
	 *            T�tulo.
	 */
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	/**
	 * M�todo que possibilita alterar uma descri��o de m�dia.
	 * 
	 * @param descricao
	 *            Descri��o.
	 */
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	/**
	 * Obtem numero de atributos nesta classe que sao indexaveis por metodos da
	 * interface {@link ComparaPor}, funciona conforme
	 * {@link Explica��oDoNumeroDeAtributos}
	 * 
	 * @return Numero de atributros de instancia desta classe que sao indexaveis
	 */
	protected static int numeroDeAtributos() {
		return NUMERO_DE_ATRIBUTOS;
	}

	/**
	 * Retorna os nomes dos atributos da midia
	 * 
	 * @return Lista com nome dos atributos da midia, auxiliando o reuso nessa
	 *         arquitetura, e evitando o uso das constantes uma ves que os nomes
	 *         dos atributos estao na mesma ordem das constantes
	 */
	public static List<String> nomeDosAtributos() {
		LinkedList<String> atributos = new LinkedList<String>();

		atributos.add("Nome");
		atributos.add("Titulo");
		atributos.add("Descri��o");

		return atributos;
	}

	/**
	 * Retorna fabrica de midias
	 * 
	 * @return Fabrica de midias respon�avel por receber uma lista de Strings e
	 *         instanciar um subitipo de midia
	 */
	public static FabricaDeMidias getFabricaDeMidias() {
		return null;
	}

}
