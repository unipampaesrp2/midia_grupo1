/**
 * Classe respons�vel pela m�dia M�sica que estende a classe Audio.
 */

package midias;

import interfaces.ComparaPor;

import java.util.List;

import anotacoes.Explica��oDoNumeroDeAtributos;
import view.auxi.PrototipoMusicaGrid;
import view.base.AdicionadorListador;

/**
 * @author Alex Dalosto
 * @author Cristina Benito
 * @author Leonardo Lopes
 * @author L�cio Jorge
 * @author Michel Marques
 */

public class Musica extends Audio {
	private String interpretes;

	/**
	 * M�todo construtor da classe Musica, que recebe por par�metro os atributos
	 * instanciados com os construtores das classe abstratas Audio e M�dia.
	 * Lan�a uma exce��o no caso de ser colocado um nome, t�tulo, descri��o,
	 * idioma, genero autores ou interpretes nulos, ou ainda, se for colocado um
	 * ano inv�lido ou uma dura��o inferior a zero.
	 * 
	 * @param nome
	 *            nome.
	 * @param titulo
	 *            T�tulo.
	 * @param descricao
	 *            Descri��o da m�dia.
	 * @param duracao
	 *            Tempo de dura��o.
	 * @param idioma
	 *            Idioma.
	 * @param genero
	 *            G�nero.
	 * @param autores
	 *            Autores.
	 * @param ano
	 *            Ano de lan�amento.
	 * @param interpretes
	 *            Interpretes da m�sica.
	 */
	public Musica(String nome, String titulo, String descricao, int duracao,
			String idioma, String genero, String autores, int ano,
			String interpretes) {
		super(nome, titulo, descricao, duracao, idioma, genero, autores, ano);
		this.interpretes = interpretes;
		if (nome == null || titulo == null || descricao == null || duracao <= 0
				|| idioma == null || genero == null || autores == null
				|| ano < 1500 || interpretes == null) {
			throw new IllegalArgumentException("Algum parametro est� errado!");
		}
	}

	public static final int INTERPRETES = Audio.numeroDeAtributos();

	private static final int NUMERO_DE_ATRIBUTOS = 1;

	/**
	 * M�todo construtor da classe Musica, que recebe lista de String por
	 * par�metro, instancia com o construtor da classe FabricaDeM�dias e
	 * inicializa o atributo interpretes, �nico � classe.
	 * 
	 * @param val
	 *            Lista que cont�m Strings que representam os atributos,
	 *            ordenados conforme os �ndices.S
	 */
	public Musica(List<String> val) {
		super(val);
		interpretes = val.get(INTERPRETES);
	}

	@Override
	public AdicionadorListador toPanel() {
		return new PrototipoMusicaGrid(this);
	}

	@Override
	public String toString() {
		return super.toString() + interpretes + "\n";
	}

	@Override
	public int compareToBy(ComparaPor obj, int tipo) {
		if (tipo >= numeroDeAtributos()) {
			throw new IllegalArgumentException("Indice de compara��o invalido");
		}
		if (tipo < Audio.numeroDeAtributos()) {
			return super.compareToBy(obj, tipo);
		} else {
			return interpretes.compareTo(((Musica) obj).getInterpretes());
		}
	}

	/**
	 * M�todo respons�vel por capturar o nome de um interprete e retorn�-lo.
	 * 
	 * @return Interpretes.
	 */
	public String getInterpretes() {
		return interpretes;
	}

	/**
	 * M�todo respons�vel por permitir a altera��o do nome de um interprete.
	 * 
	 * @param interpretes
	 *            Interpretes.
	 */
	public void setInterpretes(String interpretes) {
		this.interpretes = interpretes;
	}

	@Override
	public boolean equalsIn(String str, int tipo) {
		if (tipo >= numeroDeAtributos()) {
			throw new IllegalArgumentException("Indice de compara��o invalido");
		}
		if (tipo < Audio.numeroDeAtributos()) {
			return super.equalsIn(str, tipo);
		} else {
			return interpretes.equals(str);
		}
	}

	/**
	 * Obt�m numero de atributos nesta classe que s�o index�veis por m�todos da
	 * interface {@link ComparaPor}, funciona conforme
	 * {@link Explica��oDoNumeroDeAtributos}
	 * 
	 * @return N�mero de atributros de inst�ncia desta classe que s�o
	 *         index�veis.
	 */
	protected static int numeroDeAtributos() {
		return Audio.numeroDeAtributos() + NUMERO_DE_ATRIBUTOS;
	}

	/**
	 * Retorna os nomes dos atributos da m�dia
	 * 
	 * @return Lista com nome dos atributos da m�dia. Essa arquitetura auxilia
	 *         no re�so evitando o uso das constantes, uma vez que os nomes dos
	 *         atributos est�o na mesma ordem das constantes.
	 */
	public static List<String> nomeDosAtributos() {
		List<String> atributos = Audio.nomeDosAtributos();

		atributos.add("Interpretes");

		return atributos;
	}

	/**
	 * Retorna f�brica de m�dias
	 * 
	 * @return F�brica de m�dias respons�vel por receber uma lista de Strings e
	 *         instanciar um subtipo de m�dia
	 */
	public static FabricaDeMidias getFabricaDeMidias() {
		return new FabricaDeMidias() {
			@Override
			public Midia criaMidia(List<String> val) {
				return new Musica(val);
			}
		};
	}

}
