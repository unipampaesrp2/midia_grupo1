/**
 * Classe respons�vel pela m�dia Ebook que estende a classe Gr�fico.
 */

package midias;

import interfaces.ComparaPor;

import java.util.List;

import anotacoes.Explica��oDoNumeroDeAtributos;
import view.auxi.PrototipoEBookGrid;
import view.base.AdicionadorListador;

/**
 * @author Alex Dalosto
 * @author Cristina Benito
 * @author Leonardo Lopes
 * @author L�cio Jorge
 * @author Michel Marques
 */
public class Ebook extends Grafico {

	private String local;
	private String editora;
	private int nPaginas;

	private static final int NUMERO_DE_ATRIBUTOS = 3;

	public static final int LOCAL = Grafico.numeroDeAtributos();
	public static final int EDITORA = Grafico.numeroDeAtributos() + 1;
	public static final int NPAGINAS = Grafico.numeroDeAtributos() + 2;

	/**
	 * M�todo construtor da classe Ebook, que recebe por par�metro os atributos
	 * instanciados com os construtores das classe abstratas Gr�fico e M�dia.
	 * 
	 * @param genero
	 *            G�nero.
	 * @param ano
	 *            Ano.
	 * @param nome
	 *            Nome.
	 * @param descricao
	 *            Descri��o.
	 * @param titulo
	 *            T�tulo.
	 * @param local
	 *            Local.
	 * @param editora
	 *            Editora.
	 * @param nPaginas
	 *            N�mero de p�ginas.
	 */
	public Ebook(String genero, int ano, String nome, String descricao,
			String titulo, String local, String editora, int nPaginas) {
		super(genero, ano, nome, descricao, titulo);
		this.local = local;
		this.editora = editora;
		this.nPaginas = nPaginas;
	}

	/**
	 * M�todo construtor da classe Ebook, que recebe lista de String por
	 * par�metro, instancia com o construtor da classe FabricaDeM�dias e
	 * inicializa os atributos local, editora e nPaginas, pr�prios da classe.
	 * 
	 * @param val
	 *            Lista que cont�m Strings que representam os atributos,
	 *            ordenados conforme os �ndices.
	 */

	public Ebook(List<String> val) {
		super(val);
		local = val.get(LOCAL);
		editora = val.get(EDITORA);
		nPaginas = Integer.parseInt(val.get(NPAGINAS));
	}

	@Override
	public String toString() {
		return super.toString() + local + "\n" + editora + "\n" + nPaginas + "\n";
	}

	@Override
	public AdicionadorListador toPanel() {
		return new PrototipoEBookGrid(this);
	}

	@Override
	public int compareToBy(ComparaPor obj, int tipo) {
		if (tipo >= numeroDeAtributos()) {
			throw new IllegalArgumentException("Indice de compara��o invalido");
		}
		if (tipo < Grafico.numeroDeAtributos()) {
			return super.compareToBy(obj, tipo);
		} else {
			if (tipo == LOCAL) {
				return local.compareTo(((Ebook) obj).getLocal());
			} else if (tipo == EDITORA) {
				return editora.compareTo(((Ebook) obj).getEditora());
			} else { // numero de p�ginas
				return nPaginas - ((Ebook) obj).getnPaginas();
			}
		}
	}

	/**
	 * M�todo respons�vel por capturar o nome de um local e retorn�-lo.
	 * 
	 * @return Local.
	 */
	public String getLocal() {
		return local;
	}

	/**
	 * M�todo respons�vel por capturar o nome de uma editora e retorn�-la.
	 * 
	 * @return Editora.
	 */
	public String getEditora() {
		return editora;
	}

	/**
	 * M�todo respons�vel por capturar o n�mero de p�ginas de um determinado
	 * Ebook e retorn�-las.
	 * 
	 * @return N�mero de p�ginas.
	 */
	public int getnPaginas() {
		return nPaginas;
	}

	/**
	 * M�todo respons�vel por capturar um determinado ano e retorn�-lo.
	 * 
	 * @return Ano.
	 */
	public int getAno() {
		return ano;
	}

	/**
	 * M�todo que possibilita altera��o de um determinado local.
	 * 
	 * @param local
	 *            Local.
	 */
	public void setLocal(String local) {
		this.local = local;
	}

	/**
	 * M�todo que possibilita altera��o de uma editora.
	 * 
	 * @param editora
	 *            Editora.
	 */
	public void setEditora(String editora) {
		this.editora = editora;
	}

	/**
	 * M�todo que possibilita altera��o de determinados n�meros de p�ginas.
	 * 
	 * @param nPaginas
	 *            N�mero de p�ginas.
	 */
	public void setnPaginas(int nPaginas) {
		this.nPaginas = nPaginas;
	}

	/**
	 * M�todo que possibilita altera��o de um determinado ano.
	 * 
	 * @param Ano.
	 */
	public void setAno(int ano) {
		this.ano = ano;
	}

	@Override
	public boolean equalsIn(String str, int tipo) {
		if (tipo >= numeroDeAtributos()) {
			throw new IllegalArgumentException("Indice de compara��o invalido");
		}
		if (tipo < Grafico.numeroDeAtributos()) {
			return super.equalsIn(str, tipo);
		} else {
			if (tipo == LOCAL) {
				return local.equals(str);
			} else if (tipo == EDITORA) {
				return editora.equals(str);
			} else {// N�mero de p�ginas
				int aux = 0;
				try {
					aux = Integer.parseInt(str);
				} catch (NumberFormatException e) {
					throw new IllegalArgumentException(
							"Formato do ano invalido");
				}
				return nPaginas == aux;
			}
		}
	}

	/**
	 * Obt�m numero de atributos nesta classe que s�o index�veis por m�todos da
	 * interface {@link ComparaPor}, funciona conforme
	 * {@link Explica��oDoNumeroDeAtributos}
	 * 
	 * @return N�mero de atributros de inst�ncia desta classe que s�o
	 *         index�veis.
	 */
	protected static int numeroDeAtributos() {
		return Midia.numeroDeAtributos() + NUMERO_DE_ATRIBUTOS;
	}

	/**
	 * Retorna os nomes dos atributos da m�dia
	 * 
	 * @return Lista com nome dos atributos da m�dia. Essa arquitetura auxilia
	 *         no re�so evitando o uso das constantes, uma vez que os nomes dos
	 *         atributos est�o na mesma ordem das constantes.
	 */
	public static List<String> nomeDosAtributos() {
		List<String> atributos = Grafico.nomeDosAtributos();

		atributos.add("Local");
		atributos.add("Editora");
		atributos.add("Numero de paginas");

		return atributos;
	}

	/**
	 * Retorna f�brica de m�dias
	 * 
	 * @return F�brica de m�dias respons�vel por receber uma lista de Strings e
	 *         instanciar um subtipo de m�dia
	 */
	public static FabricaDeMidias getFabricaDeMidias() {
		return new FabricaDeMidias() {
			@Override
			public Midia criaMidia(List<String> val) {
				return new Ebook(val);
			}
		};
	}
}
