/**
 * Classe respons�vel pela m�dia Jogos que estende a classe Gr�fico.
 */

package midias;

import interfaces.ComparaPor;

import java.util.List;

import anotacoes.Explica��oDoNumeroDeAtributos;
import view.auxi.PrototipoJogosGrid;
import view.base.AdicionadorListador;

/**
 * @author Alex Dalosto
 * @author Cristina Benito
 * @author Leonardo Lopes
 * @author L�cio Jorge
 * @author Michel Marques
 */

public class Jogos extends Grafico {
	private int nJogadores;
	private boolean sup_a_rede;
	private String autores;

	private static final int NUMERO_DE_ATRIBUTOS = 3;

	public static final int NJOGADORES = Grafico.numeroDeAtributos();
	public static final int SUP_A_REDE = Grafico.numeroDeAtributos() + 1;
	public static final int AUTORES = Grafico.numeroDeAtributos() + 2;

	/**
	 * /** M�todo construtor da classe jogos, que recebe por par�metro os
	 * atributos instanciados com os construtores das classe abstratas Gr�fico e
	 * M�dia. Lan�a uma exce��o no caso de ser colocado um ano inv�lido, um
	 * n�mero de jogadores inferior a zero, um nome, um genero, uma descri��o,
	 * um t�tulo, ou autores nulos. .
	 * 
	 * @param genero
	 *            G�nero da m�dia.
	 * @param ano
	 *            Ano.
	 * @param nome
	 *            Nome.
	 * @param descricao
	 *            Descri��o.
	 * @param titulo
	 *            T�tulo.
	 * @param nJogadores
	 *            N�meros de jogadores que comp�em o jogo.
	 * @param sup_a_rede
	 *            Op��o de suporte a rede.
	 * @param autores
	 *            Autores do jogo.
	 */
	public Jogos(String genero, int ano, String nome, String descricao,
			String titulo, int nJogadores, boolean sup_a_rede, String autores) {
		super(genero, ano, nome, descricao, titulo);
		this.nJogadores = nJogadores;
		this.sup_a_rede = sup_a_rede;
		this.autores = autores;
		if (genero == null || ano < 1500 || nome == null || descricao == null
				|| titulo == null || nJogadores <= 0 || autores == null) {
			throw new IllegalArgumentException(
					"Alguma informacao esta incorreta!");
		}
	}

	/**
	 * M�todo construtor da classe jogos, que recebe lista de String por
	 * par�metro, instancia com o construtor da classe FabricaDeM�dias e
	 * inicializa os atributos nJogadores, sup_A_Rede e autores �nicos � classe.
	 * 
	 * @param val
	 *            Lista que cont�m Strings que representam os atributos,
	 *            ordenados conforme os �ndices.
	 */
	public Jogos(List<String> val) {
		super(val);
		nJogadores = Integer.parseInt(val.get(NJOGADORES));
		sup_a_rede = "true".equals(val.get(SUP_A_REDE));
		autores = val.get(AUTORES);
	}

	@Override
	public AdicionadorListador toPanel() {
		return new PrototipoJogosGrid(this);
	}

	@Override
	public String toString() {
		return super.toString() + nJogadores + "\n" + sup_a_rede + "\n" + autores
				+ "\n";
	}

	@Override
	public int compareToBy(ComparaPor obj, int tipo) {
		if (tipo >= numeroDeAtributos()) {
			throw new IllegalArgumentException("Indice de compara��o invalido");
		}
		if (tipo < Grafico.numeroDeAtributos()) {
			return super.compareToBy(obj, tipo);
		} else {
			if (tipo == NJOGADORES) {
				int aux = 0;
				try {
					aux = (((Jogos) obj).getnJogadores());
				} catch (Exception e) {
					throw new IllegalArgumentException(
							"Numero de jogadores mal formatado");
				}
				return nJogadores - aux;
			} else if (tipo == SUP_A_REDE) {
				if (sup_a_rede) {
					return 1;
				} else {
					return -1;
				}
			} else {
				return autores.compareTo((((Jogos) obj).getAutores()));
			}

		}
	}

	/**
	 * M�todo que permite capturar o n�mero de jogadores e retorn�-lo.
	 * 
	 * @return n�mero de jogadores.
	 */
	public int getnJogadores() {
		return nJogadores;
	}

	/**
	 * M�todo respons�vel por retornar verdadeiro ou falso para a condi��o de
	 * suporte a rede.
	 * 
	 * @return se possui suporte a rede ou n�o.
	 */
	public boolean isSup_a_rede() {
		return sup_a_rede;
	}

	/**
	 * M�todo que permite altera��o em um n�mero de jogadores.
	 * 
	 * @param nJogadores
	 *            N�mero de Jogdores.
	 */
	public void setnJogadores(int nJogadores) {
		this.nJogadores = nJogadores;
	}

	/**
	 * M�todo que permite alteral�ao da condi��o de suporte a rede (sim/n�o).
	 * 
	 * @param sup_a_rede
	 *            Suporte a rede.
	 */
	public void setSup_a_rede(boolean sup_a_rede) {
		this.sup_a_rede = sup_a_rede;
	}

	/**
	 * M�todo que permite capturar o nome de um autor(es) e retorn�-lo.
	 * 
	 * @return Autores.
	 */
	public String getAutores() {
		return autores;
	}

	/**
	 * M�todo que permite altera��o dos autores do jogo.
	 * 
	 * @param autores
	 *            Autores.
	 */
	public void setAutores(String autores) {
		this.autores = autores;
	}

	@Override
	public boolean equalsIn(String str, int tipo) {
		if (tipo >= numeroDeAtributos()) {
			throw new IllegalArgumentException("Indice de compara��o invalido");
		}
		if (tipo < Grafico.numeroDeAtributos()) {
			return super.equalsIn(str, tipo);
		} else {
			if (tipo == SUP_A_REDE) {
				if (sup_a_rede) {
					return str.equalsIgnoreCase("Sim");
				} else {
					return str.equalsIgnoreCase("N�o");
				}

			} else if (tipo == AUTORES) {
				return autores.equals(str);
			} else {
				int aux = 0;
				try {
					aux = Integer.parseInt(str);
				} catch (Exception e) {
					throw new IllegalArgumentException(
							"Numero de jogadores mal formatado");
				}
				return nJogadores == aux;
			}
		}
	}

	/**
	 * Obt�m numero de atributos nesta classe que s�o index�veis por m�todos da
	 * interface {@link ComparaPor}, funciona conforme
	 * {@link Explica��oDoNumeroDeAtributos}
	 * 
	 * @return N�mero de atributros de inst�ncia desta classe que s�o
	 *         index�veis.
	 */
	protected static int numeroDeAtributos() {
		return Grafico.numeroDeAtributos() + NUMERO_DE_ATRIBUTOS;
	}

	/**
	 * Retorna os nomes dos atributos da m�dia
	 * 
	 * @return Lista com nome dos atributos da m�dia. Essa arquitetura auxilia
	 *         no re�so evitando o uso das constantes, uma vez que os nomes dos
	 *         atributos est�o na mesma ordem das constantes.
	 */
	public static List<String> nomeDosAtributos() {
		List<String> atributos = Grafico.nomeDosAtributos();

		atributos.add("Numero de Jogadores");
		atributos.add("Suporte a rede");
		atributos.add("Autores");

		return atributos;
	}

	/**
	 * Retorna f�brica de m�dias
	 * 
	 * @return F�brica de m�dias respons�vel por receber uma lista de Strings e
	 *         instanciar um subtipo de m�dia
	 */
	public static FabricaDeMidias getFabricaDeMidias() {
		return new FabricaDeMidias() {
			@Override
			public Midia criaMidia(List<String> val) {
				return new Jogos(val);
			}
		};
	}
}
