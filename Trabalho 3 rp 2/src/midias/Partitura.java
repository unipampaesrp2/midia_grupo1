/**
 * Classe respons�vel pela m�dia partitura que estende a classe Gr�fico.
 */
package midias;

import interfaces.ComparaPor;

import java.util.List;

import anotacoes.Explica��oDoNumeroDeAtributos;
import view.auxi.PrototipoPartituraGrid;
import view.base.AdicionadorListador;

/**
 * @author Alex Dalosto
 * @author Cristina Benito
 * @author Leonardo Lopes
 * @author L�cio Jorge
 * @author Michel Marques
 */
public class Partitura extends Grafico {
	private String instrumentos;
	private static final int NUMERO_DE_ATRIBUTOS = 1;
	public static final int INSTRUMENTOS = Grafico.numeroDeAtributos();

	/**
	 * M�todo construtor da classe Partitura, que recebe por par�metro os
	 * atributos instanciados com os construtores das classe abstratas Gr�fico e
	 * M�dia. Lan�a uma exce��o no caso de ser colocado um ano inv�lido, uma
	 * descri��o, ou um t�tulo, ou instrumentos nulos. .
	 * 
	 * 
	 * @param genero
	 *            Genero da M�dia.
	 * @param ano
	 *            Ano em que foi lan�ada.
	 * @param nome
	 *            Nome da m�dia.
	 * @param descricao
	 *            Descri��o
	 * @param titulo
	 *            t�tulo
	 * @param instrumentos
	 *            Intrumentos utilizados.
	 */
	public Partitura(String genero, int ano, String nome, String descricao,
			String titulo, String instrumentos) {
		super(genero, ano, nome, descricao, titulo);
		this.instrumentos = instrumentos;
		if (genero == null || ano < 1500 || descricao == null || titulo == null
				|| instrumentos == null) {
			throw new IllegalArgumentException("Algum par�metro est� errado!");
		}
	}

	/**
	 * M�todo construtor da classe Partitura, que recebe lista de String por
	 * par�metro, instancia com o construtor da classe FabricaDeM�dias e
	 * inicializa o atributo instrumentos, �nico � classe.
	 * 
	 * @param val
	 *            Lista que cont�m Strings que representam os atributos,
	 *            ordenados conforme os �ndices.
	 */
	public Partitura(List<String> val) {
		super(val);
		instrumentos = val.get(INSTRUMENTOS);
	}

	@Override
	public AdicionadorListador toPanel() {
		return new PrototipoPartituraGrid(this);
	}

	@Override
	public String toString() {
		return super.toString() + instrumentos + "\n";
	}

	@Override
	public int compareToBy(ComparaPor obj, int tipo) {
		if (tipo >= numeroDeAtributos()) {
			throw new IllegalArgumentException("Indice de compara��o invalido");
		}
		if (tipo < Grafico.numeroDeAtributos()) {
			return super.compareToBy(obj, tipo);
		} else {
			return instrumentos.compareTo(((Partitura) obj).getInstrumentos());
		}
	}

	/**
	 * M�todo que permite capturar o nome de um instrumento e retorn�-lo.
	 * 
	 * @return Instrumentos
	 */
	public String getInstrumentos() {
		return instrumentos;
	}

	/**
	 * M�todo que permite alterar o nome de um instrumento j� cadastrado.
	 * 
	 * @param instrumentos
	 *            Intrumentos.
	 */
	public void setInstrumentos(String instrumentos) {
		this.instrumentos = instrumentos;
	}

	@Override
	public boolean equalsIn(String str, int tipo) {
		if (tipo >= numeroDeAtributos()) {
			throw new IllegalArgumentException("Indice de compara��o invalido");
		}
		if (tipo < Grafico.numeroDeAtributos()) {
			return super.equalsIn(str, tipo);
		} else {
			
			return instrumentos.equals(str);
		}
	}

	/**
	 * Obt�m numero de atributos nesta classe que s�o index�veis por m�todos da
	 * interface {@link ComparaPor}, funciona conforme
	 * {@link Explica��oDoNumeroDeAtributos}
	 * 
	 * @return N�mero de atributros de inst�ncia desta classe que s�o
	 *         index�veis.
	 */
	protected static int numeroDeAtributos() {
		return Grafico.numeroDeAtributos() + NUMERO_DE_ATRIBUTOS;
	}

	/**
	 * Retorna os nomes dos atributos da m�dia
	 * 
	 * @return Lista com nome dos atributos da m�dia. Essa arquitetura auxilia
	 *         no re�so evitando o uso das constantes, uma vez que os nomes dos
	 *         atributos est�o na mesma ordem das constantes.
	 */
	public static List<String> nomeDosAtributos() {
		List<String> atributos = Grafico.nomeDosAtributos();

		atributos.add("Instrumentos");

		return atributos;
	}

	/**
	 * Retorna f�brica de m�dias
	 * 
	 * @return F�brica de m�dias respons�vel por receber uma lista de Strings e
	 *         instanciar um subtipo de m�dia
	 */
	public static FabricaDeMidias getFabricaDeMidias() {
		return new FabricaDeMidias() {
			@Override
			public Midia criaMidia(List<String> val) {
				return new Partitura(val);
			}
		};
	}
}
