/**
 * Classe abstrata que estende a classe M�dia. 
 */
package midias;

import interfaces.ComparaPor;

import java.util.List;

import anotacoes.Explica��oDoNumeroDeAtributos;

/**
 * @author Alex Dalosto
 * @author Cristina Benito
 * @author Leonardo Lopes
 * @author L�cio Jorge
 * @author Michel Marques
 */
public abstract class Grafico extends Midia {

	protected String genero;

	protected int ano;

	private static final int NUMERO_DE_ATRIBUTOS = 2;

	public static final int GENERO = Midia.numeroDeAtributos();

	public static final int ANO = Midia.numeroDeAtributos() + 1;

	public Grafico(String genero, int ano, String nome, String descricao,
			String titulo) {
		super(nome, titulo, descricao);
		this.genero = genero;
		this.ano = ano;

	}

	/**
	 * M�todo construtor da classe Gr�fico, que costroi baseando-se em uma lista
	 * de atributos.
	 * 
	 * @param val
	 *            Lista que cont�m Strings que representam os atributos,
	 *            ordenados conforme os �ndices
	 */
	public Grafico(List<String> val) {
		super(val);
		genero = val.get(GENERO);
		ano = Integer.parseInt(val.get(ANO));
	}

	@Override
	public String toString() {
		return super.toString() + genero + "\n" + ano + "\n";
	}

	@Override
	public int compareToBy(ComparaPor obj, int tipo) {
		if (tipo >= numeroDeAtributos()) {
			throw new IllegalArgumentException("Indice de compara��o invalido");
		}
		if (tipo < Midia.numeroDeAtributos()) {
			return super.compareToBy(obj, tipo);
		} else {
			if (tipo == GENERO) {
				return genero.compareTo(((Grafico) obj).getGenero());
			} else {// ANO
				return ano - ((Grafico) obj).getAno();
			}
		}
	}

	/**
	 * M�todo respons�vel por capturar um g�nero e retorn�-lo.
	 * 
	 * @return g�nero.
	 */
	public String getGenero() {
		return genero;
	}

	/**
	 * M�todo respons�vel por capturar um ano e retorn�-lo.
	 * 
	 * @return ano.
	 */
	public int getAno() {
		return ano;
	}

	/**
	 * M�todo que possibilita a altera��o de um g�nero.
	 * 
	 * @param genero
	 *            g�nero.
	 */
	public void setGenero(String genero) {
		this.genero = genero;
	}

	/**
	 * M�todo que possibilita a altera��o de um ano.
	 * 
	 * @param ano
	 *            Ano.
	 */
	public void setAno(int ano) {
		this.ano = ano;
	}

	/**
	 * Obt�m numero de atributos nesta classe que s�o index�veis por m�todos da
	 * interface {@link ComparaPor}, funciona conforme
	 * {@link Explica��oDoNumeroDeAtributos}
	 * 
	 * @return N�mero de atributros de inst�ncia desta classe que s�o
	 *         index�veis.
	 */
	protected static int numeroDeAtributos() {
		return Midia.numeroDeAtributos() + NUMERO_DE_ATRIBUTOS;
	}

	/**
	 * Retorna os nomes dos atributos da m�dia
	 * 
	 * @return Lista com nome dos atributos da m�dia. Essa arquitetura auxilia
	 *         no re�so evitando o uso das constantes, uma vez que os nomes dos
	 *         atributos est�o na mesma ordem das constantes.
	 */
	public static List<String> nomeDosAtributos() {
		List<String> atributos = Midia.nomeDosAtributos();

		atributos.add("Genero");
		atributos.add("Ano");

		return atributos;
	}

	@Override
	public boolean equalsIn(String str, int tipo) {
		if (tipo >= numeroDeAtributos()) {
			throw new IllegalArgumentException("Indice de compara��o invalido");
		}
		if (tipo < Midia.numeroDeAtributos()) {
			return super.equalsIn(str, tipo);
		} else {

			if (tipo == GENERO) {
				return genero.equals(str);
			} else {// ANO
				int aux = 0;
				try {
					aux = Integer.parseInt(str);
				} catch (NumberFormatException e) {
					throw new IllegalArgumentException(
							"Formato do ano invalido");
				}
				return ano == aux;
			}
		}
	}
}
