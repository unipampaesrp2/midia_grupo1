/**
 * Classe respons�vel por implementar o tipo de m�dia Audiolivro e estende a classe Audio.
 */
package midias;

import interfaces.ComparaPor;

import java.util.List;

import anotacoes.Explica��oDeConstantes;
import anotacoes.Explica��oDoNumeroDeAtributos;
import view.auxi.PrototipoAudioLivroGrid;
import view.base.AdicionadorListador;

/**
 * @author Alex Dalosto
 * @author Cristina Benito
 * @author Leonardo Lopes
 * @author L�cio Jorge
 * @author Michel Marques
 */
public class Audiolivro extends Audio {

	private String local;

	private String editora;

	private static final int NUMERO_DE_ATRIBUTOS = 2;

	// public static final int LOCAL=8;
	// public static final int EDITORA=9;
	/**
	 * Indice do atrubuto local @see {@link Explica��oDeConstantes}
	 */
	public static final int LOCAL = Audio.numeroDeAtributos();
	/**
	 * Indice do atrubuto editora @see {@link Explica��oDeConstantes}
	 */
	public static final int EDITORA = Audio.numeroDeAtributos() + 1;

	/**
	 * Construtor da classe Audiolivro que recebe por par�metro todos os
	 * atributos de Audiolivro.
	 * 
	 * Controi midia com todos atributos
	 * 
	 * @param nome
	 *            Nome da midia
	 * @param titulo
	 *            Titulo da midia
	 * @param descricao
	 *            Descri��o da midia
	 * @param duracao
	 *            Dura��o do audio
	 * @param idioma
	 *            Idioma do audio
	 * @param genero
	 *            Genero do audio
	 * @param autores
	 *            Autores do audio
	 * @param ano
	 *            Ano do audio
	 * @param local
	 *            Local do audio livro
	 * @param editora
	 *            Editora do audio livro
	 */
	public Audiolivro(String nome, String titulo, String descricao,
			int duracao, String idioma, String genero, String autores, int ano,
			String local, String editora) {
		super(nome, titulo, descricao, duracao, idioma, genero, autores, ano);
		this.local = local;
		this.editora = editora;
	}

	/**
	 * Costroi baseando-se em uma lista de atributos
	 * 
	 * @param val
	 *            Lista que cont�m Strings que representam os atributos,
	 *            ordenados conforme os �ndices. Seguem a mesma ordem do m�todo
	 *            toSave desta forma quem utiliza a classe n�o precisa se
	 *            preocupar.
	 */
	public Audiolivro(List<String> val) {
		super(val);
		local = val.get(LOCAL);
		editora = val.get(EDITORA);
	}

	@Override
	public String toString() {
		return super.toString() + local + "\n" + editora + "\n";
	}

	@Override
	public AdicionadorListador toPanel() {
		return new PrototipoAudioLivroGrid(this);
	}

	@Override
	public int compareToBy(ComparaPor obj, int tipo) {
		if (tipo >= numeroDeAtributos()) {
			throw new IllegalArgumentException("Indice de compara��o invalido");
		}
		if (tipo < Audio.numeroDeAtributos()) {
			return super.compareToBy(obj, tipo);
		} else {
			if (tipo == LOCAL) {
				return local.compareTo(((Audiolivro) obj).getLocal());
			} else {// EDITORA
				return editora.compareTo(((Audiolivro) obj).getEditora());
			}
		}
	}

	/**
	 * Captura e retorna um local.
	 * 
	 * @return Local
	 */
	public String getLocal() {
		return local;
	}

	/**
	 * Captura e retorna a editora.
	 * 
	 * @return Editora
	 */
	public String getEditora() {
		return editora;
	}

	/**
	 * Possibilita altera��o de um local.
	 * 
	 * @param local
	 *            Local
	 */
	public void setLocal(String local) {
		this.local = local;
	}

	/**
	 * Possibilita altera��o da editora.
	 * 
	 * @param editora
	 *            Editora
	 */
	public void setEditora(String editora) {
		this.editora = editora;
	}

	@Override
	public boolean equalsIn(String str, int tipo) {
		if (tipo >= numeroDeAtributos()) {
			throw new IllegalArgumentException("Indice de compara��o invalido");
		}
		if (tipo < Audio.numeroDeAtributos()) {
			return super.equalsIn(str, tipo);
		} else {
			if (tipo == LOCAL) {
				return local.equals(str);
			} else {// EDITORA
				return editora.equals(str);
			}
		}
	}

	/**
	 * Obt�m numero de atributos nesta classe que s�o index�veis por m�todos da
	 * interface {@link ComparaPor}, funciona conforme
	 * {@link Explica��oDoNumeroDeAtributos}
	 * 
	 * @return N�mero de atributros de inst�ncia desta classe que s�o
	 *         index�veis.
	 */
	protected static int numeroDeAtributos() {
		return Audio.numeroDeAtributos() + NUMERO_DE_ATRIBUTOS;
	}

	/**
	 * Retorna os nomes dos atributos da m�dia
	 * 
	 * @return Lista com nome dos atributos da m�dia. Essa arquitetura auxilia
	 *         no re�so evitando o uso das constantes, uma vez que os nomes dos
	 *         atributos est�o na mesma ordem das constantes.
	 */
	public static List<String> nomeDosAtributos() {
		List<String> atributos = Audio.nomeDosAtributos();

		atributos.add("Local");
		atributos.add("Editora");

		return atributos;
	}

	/**
	 * Retorna f�brica de m�dias
	 * 
	 * @return F�brica de m�dias respons�vel por receber uma lista de Strings e
	 *         instanciar um subtipo de m�dia
	 */
	public static FabricaDeMidias getFabricaDeMidias() {
		return new FabricaDeMidias() {
			@Override
			public Midia criaMidia(List<String> val) {
				return new Audiolivro(val);
			}
		};
	}
}
