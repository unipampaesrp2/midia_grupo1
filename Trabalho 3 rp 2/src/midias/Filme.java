/**
 * Classe respons�vel pela m�dia Filme que estende a classe Gr�fico.
 */

package midias;

import interfaces.ComparaPor;

import java.util.List;

import anotacoes.Explica��oDoNumeroDeAtributos;
import view.auxi.PrototipoFilmeGrid;
import view.base.AdicionadorListador;

/**
 * @author Alex Dalosto
 * @author Cristina Benito
 * @author Leonardo Lopes
 * @author L�cio Jorge
 * @author Michel Marques
 */
public class Filme extends Grafico {

	private String diretor;
	private String atoresPrincipais;

	private static final int NUMERO_DE_ATRIBUTOS = 2;

	public static final int DIRETOR = Grafico.numeroDeAtributos();

	public static final int ATORESPRINCIPAIS = Grafico.numeroDeAtributos() + 1;

	/**
	 * M�todo construtor da classe Filme, que recebe por par�metro os atributos
	 * instanciados com os construtores das classe abstratas Gr�fico e M�dia.
	 * 
	 * @param genero
	 *            G�nero.
	 * @param ano
	 *            ano.
	 * @param nome
	 *            Nome.
	 * @param descricao
	 *            Descri��o.
	 * @param titulo
	 *            T�tulo.
	 * @param diretor
	 *            Diretor do filme.
	 * @param atoresPrincipais
	 *            Atores Principais.
	 */
	public Filme(String genero, int ano, String nome, String descricao,
			String titulo, String diretor, String atoresPrincipais) {
		super(genero, ano, nome, descricao, titulo);
		this.diretor = diretor;
		this.atoresPrincipais = atoresPrincipais;
	}

	/**
	 * M�todo construtor da classe Filme, que recebe lista de String por
	 * par�metro, instancia com o construtor da classe FabricaDeM�dias e
	 * inicializa os atributos diretor e atoresPrincipais, �nicos � classe.
	 * 
	 * @param val
	 *            Lista que cont�m Strings que representam os atributos,
	 *            ordenados conforme os �ndices.
	 */
	public Filme(List<String> val) {
		super(val);
		diretor = val.get(DIRETOR);
		atoresPrincipais = val.get(ATORESPRINCIPAIS);
	}

	@Override
	public String toString() {
		return super.toString() + diretor + "\n" + atoresPrincipais + "\n";
	}

	@Override
	public AdicionadorListador toPanel() {
		return new PrototipoFilmeGrid(this);
	}

	@Override
	public int compareToBy(ComparaPor obj, int tipo) {
		if (tipo >= numeroDeAtributos()) {
			throw new IllegalArgumentException("Indice de compara��o invalido");
		}
		if (tipo < Grafico.numeroDeAtributos()) {
			return super.compareToBy(obj, tipo);
		} else {
			if (tipo == DIRETOR) {
				return diretor.compareTo(((Filme) obj).getDiretor());
			} else {// ATORES PRINCIPAIS
				return atoresPrincipais.compareTo(((Filme) obj)
						.getAtoresPrincipais());
			}
		}
	}

	/**
	 * M�todo respons�vel por capturar o nome de um diretor e retorn�-lo.
	 * 
	 * @return Diretor.
	 */
	public String getDiretor() {
		return diretor;
	}

	/**
	 * M�todo repons�vel por capturar os nomes dos atores principais e
	 * retorn�-los.
	 * 
	 * @return Atores principais.
	 */
	public String getAtoresPrincipais() {
		return atoresPrincipais;
	}

	/**
	 * M�todo que possibilita altera��o do nome de um diretor.
	 * 
	 * @param diretor
	 *            Diretor.
	 */
	public void setDiretor(String diretor) {
		this.diretor = diretor;
	}

	/**
	 * M�todo que possibilita altera��o dos nomes dos atores principais.
	 * 
	 * @param atoresPrincipais
	 *            Atores Principais.
	 */
	public void setAtoresPrincipais(String atoresPrincipais) {
		this.atoresPrincipais = atoresPrincipais;
	}

	@Override
	public boolean equalsIn(String str, int tipo) {
		if (tipo >= numeroDeAtributos()) {
			throw new IllegalArgumentException("Indice de compara��o invalido");
		}
		if (tipo < Grafico.numeroDeAtributos()) {
			return super.equalsIn(str, tipo);
		} else {
			if (tipo == DIRETOR) {
				return diretor.equals(str);
			} else {// ATORES PRINCIPAIS
				return atoresPrincipais.equals(str);
			}
		}
	}

	/**
	 * Obt�m numero de atributos nesta classe que s�o index�veis por m�todos da
	 * interface {@link ComparaPor}, funciona conforme
	 * {@link Explica��oDoNumeroDeAtributos}
	 * 
	 * @return N�mero de atributros de inst�ncia desta classe que s�o
	 *         index�veis.
	 */
	protected static int numeroDeAtributos() {
		return Grafico.numeroDeAtributos() + NUMERO_DE_ATRIBUTOS;
	}

	/**
	 * Retorna os nomes dos atributos da m�dia
	 * 
	 * @return Lista com nome dos atributos da m�dia. Essa arquitetura auxilia
	 *         no re�so evitando o uso das constantes, uma vez que os nomes dos
	 *         atributos est�o na mesma ordem das constantes.
	 */
	public static List<String> nomeDosAtributos() {
		List<String> atributos = Grafico.nomeDosAtributos();

		atributos.add("Diretor");
		atributos.add("Atores Principais");

		return atributos;
	}

	/**
	 * Retorna f�brica de m�dias
	 * 
	 * @return F�brica de m�dias respons�vel por receber uma lista de Strings e
	 *         instanciar um subtipo de m�dia
	 */
	public static FabricaDeMidias getFabricaDeMidias() {
		return new FabricaDeMidias() {
			@Override
			public Midia criaMidia(List<String> val) {
				return new Filme(val);
			}
		};
	}
}
