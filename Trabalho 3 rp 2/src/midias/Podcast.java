/**
 * Classe respons�vel pela m�dia Podcast que estende a classe Audio.
 */
package midias;

import interfaces.ComparaPor;

import java.util.List;

import view.auxi.PrototipoPodcastGrid;
import view.base.AdicionadorListador;

/**
 * @author Alex Dalosto
 * @author Cristina Benito
 * @author Leonardo Lopes
 * @author L�cio Jorge
 * @author Michel Marques
 */
public class Podcast extends Audio {

	/**
	 * M�todo construtor da classe Podcast, que recebe por par�metro os
	 * atributos instanciados com os construtores das classe abstratas �udio e
	 * M�dia. Lan�a uma exce��o no caso de ser colocado um ano inv�lido, uma
	 * dura��o inferior a zero, um nome, um t�tulo, uma descri��o, um idioma, um
	 * g�nero ou autores nulos.
	 * 
	 * @param nome
	 *            nome.
	 * @param titulo
	 *            t�tulo.
	 * @param descricao
	 *            Descri;��o da m�dia.
	 * @param duracao
	 *            Dura��o.
	 * @param idioma
	 *            Idioma.
	 * @param genero
	 *            G�nero.
	 * @param autores
	 *            Autores da m�dia.
	 * @param ano
	 *            Ano de grava��o.
	 */
	public Podcast(String nome, String titulo, String descricao, int duracao,
			String idioma, String genero, String autores, int ano) {
		super(nome, titulo, descricao, duracao, idioma, genero, autores, ano);
		if (nome == null || titulo == null || descricao == null || duracao <= 0
				|| idioma == null || genero == null || autores == null
				|| ano < 1500) {
			throw new IllegalArgumentException("Algum parametro est� errado!");
		}
	}

	/**
	 * M�todo construtor da classe Podcast, que recebe lista de String por
	 * par�metro, instancia com o construtor da classe FabricaDeM�dias
	 * 
	 * @param val
	 *            Lista que cont�m Strings que representam os atributos,
	 *            ordenados conforme os �ndices.
	 */
	public Podcast(List<String> val) {
		super(val);
	}

	@Override
	public AdicionadorListador toPanel() {
		return new PrototipoPodcastGrid(this);
	}

	@Override
	public String toString() {
		return super.toString();
	}

	@Override
	public int compareToBy(ComparaPor obj, int tipo) {
		return super.compareToBy(obj, tipo);
	}

	@Override
	public boolean equalsIn(String str, int tipo) {
		return super.equalsIn(str, tipo);
	}

	/**
	 * Retorna os nomes dos atributos da m�dia
	 * 
	 * @return Lista com nome dos atributos da m�dia. Essa arquitetura auxilia
	 *         no re�so evitando o uso das constantes, uma vez que os nomes dos
	 *         atributos est�o na mesma ordem das constantes.
	 */
	public static List<String> nomeDosAtributos() {
		List<String> atributos = Audio.nomeDosAtributos();

		return atributos;
	}

	/**
	 * Retorna f�brica de m�dias
	 * 
	 * @return F�brica de m�dias respons�vel por receber uma lista de Strings e
	 *         instanciar um subtipo de m�dia
	 */
	public static FabricaDeMidias getFabricaDeMidias() {
		return new FabricaDeMidias() {
			@Override
			public Midia criaMidia(List<String> val) {
				return new Podcast(val);
			}
		};
	}

}
