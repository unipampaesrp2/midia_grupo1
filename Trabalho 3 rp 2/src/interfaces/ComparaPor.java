package interfaces;
/**
 * Interface responsavel por comparar valores do atributo do objeto
 * @author Leonardo
 *
 */
public interface ComparaPor {
	/**
	 * Compara um determinado atributo de dois objetos do mesmo tipo
	 * @param obj Objeto a comparar atributos
	 * @param tipo Indice atributo a ser comparado
	 * @return Numero positivo caso o propio objeto tenha o atributo especificado com o maior valor que o passado por parametro, 0 se forem iguis e negativo se o do objeto passado por parametro for maior. Diferen�a de valor do propio objeto com o passado por parametro.
	 */
	public abstract int compareToBy(ComparaPor obj, int tipo);
	/**
	 * Testa se determinado atributo � igual ao parametro
	 * @param str Valor a ser comparado
	 * @param tipo Indice do atributo a ser comparado
	 * @return True se o atributo do indice passado por parametro for igual ao valor contido na string tambem passada por parametro
	 */
	public abstract boolean equalsIn(String str, int tipo);
	

}
