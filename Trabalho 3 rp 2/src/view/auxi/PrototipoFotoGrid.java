package view.auxi;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.text.ParseException;
import java.util.Date;

import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.text.MaskFormatter;

import midias.Foto;
import midias.Midia;
import view.base.AdicionadorListador;
import controler.DAOMidia;

/**
 * Painel adicionador listador de {@link Foto}
 */
@SuppressWarnings("serial")
public class PrototipoFotoGrid extends AdicionadorListador {

	private JTextField txtTitulo;
	private JTextField txtNome;
	private JLabel label_2;
	private JTextField txtDescricao;
	private JLabel lblGenero;
	private JTextField txtLocal;
	private JLabel lblData;
	private JLabel lblHora;
	private JTextField txtHora;
	private JLabel lblPessoas;
	private JTextField txtPessoas;
	private JLabel lblFotgrafo;
	private JTextField txtFotografo;
	private JFormattedTextField formattedTextField;

	
	
	/**
	 * Cria painel com fu��o de adicionar {@link Foto}s
	 * @param dao DAO onde serao adicionados os dados
	 */
	public PrototipoFotoGrid(DAOMidia dao) {
		this();
		super.dao=dao;
	}
	
	/**
	 * Cria painel com a fun��o de listar os dados de um {@link Foto} e manipola-los
	 * @param foto {@link Foto} do qual os dados ser�o listados
	 */
	@SuppressWarnings("deprecation")
	public PrototipoFotoGrid(Foto foto) {
		this();
		nomeDoArquivoDeMidia=foto.getNome();
		midia=(Midia)foto;
		
		txtNome.setText(foto.getNome());
		txtTitulo.setText(foto.getTitulo());
		txtDescricao.setText(foto.getDescricao());
		txtLocal.setText(foto.getLocal());

		formattedTextField.setText(foto.getData().getDate()+"/"+(foto.getData().getMonth()+1)+"/"+foto.getData().getYear());
			
		txtHora.setText(Integer.toString(foto.getHora()));
		txtPessoas.setText(foto.getPessoas());
		txtFotografo.setText(foto.getFotografo());
		

		txtNome.setEditable(false);
		txtTitulo.setEditable(false);
		txtDescricao.setEditable(false);
		txtLocal.setEditable(false);
		formattedTextField.setEditable(false);
		txtHora.setEditable(false);
		txtPessoas.setEditable(false);
		txtFotografo.setEditable(false);
		
		
	}
	
	
	/**
	 * Create the panel.
	 */
	public PrototipoFotoGrid() {
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0 };
		gridBagLayout.rowHeights = new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
				0, 0, 0, 0 };
		gridBagLayout.columnWeights = new double[] { 0.0, 1.0, 0.0, 1.0, 0.0,
				1.0, 0.0, 0.0, Double.MIN_VALUE };
		gridBagLayout.rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
				0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE };
		setLayout(gridBagLayout);

		JLabel lblTtulo = new JLabel("T\u00EDtulo:");
		GridBagConstraints gbc_lblTtulo = new GridBagConstraints();
		gbc_lblTtulo.weighty = 10.0;
		gbc_lblTtulo.weightx = 10.0;
		gbc_lblTtulo.anchor = GridBagConstraints.SOUTHWEST;
		gbc_lblTtulo.insets = new Insets(0, 0, 5, 5);
		gbc_lblTtulo.gridx = 1;
		gbc_lblTtulo.gridy = 1;
		add(lblTtulo, gbc_lblTtulo);

		txtTitulo = new JTextField();
		txtTitulo.setColumns(10);
		GridBagConstraints gbc_txtTitulo = new GridBagConstraints();
		gbc_txtTitulo.weighty = 10.0;
		gbc_txtTitulo.weightx = 10.0;
		gbc_txtTitulo.gridwidth = 6;
		gbc_txtTitulo.insets = new Insets(0, 0, 5, 5);
		gbc_txtTitulo.fill = GridBagConstraints.BOTH;
		gbc_txtTitulo.gridx = 1;
		gbc_txtTitulo.gridy = 2;
		add(txtTitulo, gbc_txtTitulo);

		JLabel label_1 = new JLabel("Nome:");
		GridBagConstraints gbc_label_1 = new GridBagConstraints();
		gbc_label_1.weighty = 10.0;
		gbc_label_1.weightx = 10.0;
		gbc_label_1.insets = new Insets(0, 0, 5, 5);
		gbc_label_1.anchor = GridBagConstraints.SOUTHWEST;
		gbc_label_1.gridx = 1;
		gbc_label_1.gridy = 3;
		add(label_1, gbc_label_1);

		txtNome = new JTextField();
		txtNome.setColumns(10);
		GridBagConstraints gbc_txtNome = new GridBagConstraints();
		gbc_txtNome.weightx = 10.0;
		gbc_txtNome.weighty = 10.0;
		gbc_txtNome.gridwidth = 6;
		gbc_txtNome.insets = new Insets(0, 0, 5, 5);
		gbc_txtNome.fill = GridBagConstraints.BOTH;
		gbc_txtNome.gridx = 1;
		gbc_txtNome.gridy = 4;
		add(txtNome, gbc_txtNome);

		label_2 = new JLabel("Descri\u00E7\u00E3o:");
		GridBagConstraints gbc_label_2 = new GridBagConstraints();
		gbc_label_2.weighty = 10.0;
		gbc_label_2.weightx = 10.0;
		gbc_label_2.insets = new Insets(0, 0, 5, 5);
		gbc_label_2.anchor = GridBagConstraints.SOUTHWEST;
		gbc_label_2.gridx = 1;
		gbc_label_2.gridy = 5;
		add(label_2, gbc_label_2);

		txtDescricao = new JTextField();
		txtDescricao.setColumns(10);
		GridBagConstraints gbc_txtDescricao = new GridBagConstraints();
		gbc_txtDescricao.weightx = 10.0;
		gbc_txtDescricao.weighty = 10.0;
		gbc_txtDescricao.gridwidth = 6;
		gbc_txtDescricao.insets = new Insets(0, 0, 5, 5);
		gbc_txtDescricao.fill = GridBagConstraints.BOTH;
		gbc_txtDescricao.gridx = 1;
		gbc_txtDescricao.gridy = 6;
		add(txtDescricao, gbc_txtDescricao);

		lblGenero = new JLabel("Local:");
		GridBagConstraints gbc_lblGenero = new GridBagConstraints();
		gbc_lblGenero.weighty = 10.0;
		gbc_lblGenero.weightx = 10.0;
		gbc_lblGenero.insets = new Insets(0, 0, 5, 5);
		gbc_lblGenero.anchor = GridBagConstraints.SOUTHWEST;
		gbc_lblGenero.gridx = 1;
		gbc_lblGenero.gridy = 7;
		add(lblGenero, gbc_lblGenero);

		lblData = new JLabel("Data:");
		GridBagConstraints gbc_lblData = new GridBagConstraints();
		gbc_lblData.weighty = 10.0;
		gbc_lblData.weightx = 10.0;
		gbc_lblData.anchor = GridBagConstraints.SOUTHWEST;
		gbc_lblData.insets = new Insets(0, 0, 5, 5);
		gbc_lblData.gridx = 3;
		gbc_lblData.gridy = 7;
		add(lblData, gbc_lblData);

		lblHora = new JLabel("Hora:");
		GridBagConstraints gbc_lblHora = new GridBagConstraints();
		gbc_lblHora.weightx = 10.0;
		gbc_lblHora.weighty = 10.0;
		gbc_lblHora.anchor = GridBagConstraints.SOUTHWEST;
		gbc_lblHora.insets = new Insets(0, 0, 5, 5);
		gbc_lblHora.gridx = 5;
		gbc_lblHora.gridy = 7;
		add(lblHora, gbc_lblHora);

		txtLocal = new JTextField();
		txtLocal.setColumns(10);
		GridBagConstraints gbc_txtLocal = new GridBagConstraints();
		gbc_txtLocal.weightx = 10.0;
		gbc_txtLocal.weighty = 10.0;
		gbc_txtLocal.insets = new Insets(0, 0, 5, 5);
		gbc_txtLocal.fill = GridBagConstraints.BOTH;
		gbc_txtLocal.gridx = 1;
		gbc_txtLocal.gridy = 8;
		add(txtLocal, gbc_txtLocal);
		
		
		MaskFormatter formater=null;
		try {
			formater = new MaskFormatter("##/##/####");
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		formattedTextField = new JFormattedTextField(formater);
		GridBagConstraints gbc_formattedTextField = new GridBagConstraints();
		gbc_formattedTextField.insets = new Insets(0, 0, 5, 5);
		gbc_formattedTextField.fill = GridBagConstraints.BOTH;
		gbc_formattedTextField.gridx = 3;
		gbc_formattedTextField.gridy = 8;
		add(formattedTextField, gbc_formattedTextField);

		txtHora = new JTextField();
		txtHora.setColumns(10);
		GridBagConstraints gbc_txtHora = new GridBagConstraints();
		gbc_txtHora.weighty = 10.0;
		gbc_txtHora.weightx = 10.0;
		gbc_txtHora.gridwidth = 2;
		gbc_txtHora.insets = new Insets(0, 0, 5, 5);
		gbc_txtHora.fill = GridBagConstraints.BOTH;
		gbc_txtHora.gridx = 5;
		gbc_txtHora.gridy = 8;
		add(txtHora, gbc_txtHora);

		lblPessoas = new JLabel("Pessoas:");
		GridBagConstraints gbc_lblPessoas = new GridBagConstraints();
		gbc_lblPessoas.weighty = 10.0;
		gbc_lblPessoas.weightx = 10.0;
		gbc_lblPessoas.anchor = GridBagConstraints.SOUTHWEST;
		gbc_lblPessoas.insets = new Insets(0, 0, 5, 5);
		gbc_lblPessoas.gridx = 1;
		gbc_lblPessoas.gridy = 9;
		add(lblPessoas, gbc_lblPessoas);

		txtPessoas = new JTextField();
		txtPessoas.setColumns(10);
		GridBagConstraints gbc_txtPessoas = new GridBagConstraints();
		gbc_txtPessoas.weightx = 10.0;
		gbc_txtPessoas.weighty = 10.0;
		gbc_txtPessoas.gridwidth = 6;
		gbc_txtPessoas.insets = new Insets(0, 0, 5, 5);
		gbc_txtPessoas.fill = GridBagConstraints.BOTH;
		gbc_txtPessoas.gridx = 1;
		gbc_txtPessoas.gridy = 10;
		add(txtPessoas, gbc_txtPessoas);

		lblFotgrafo = new JLabel("Fot\u00F3grafo:");
		GridBagConstraints gbc_lblFotgrafo = new GridBagConstraints();
		gbc_lblFotgrafo.weighty = 10.0;
		gbc_lblFotgrafo.weightx = 10.0;
		gbc_lblFotgrafo.anchor = GridBagConstraints.SOUTHWEST;
		gbc_lblFotgrafo.insets = new Insets(0, 0, 5, 5);
		gbc_lblFotgrafo.gridx = 1;
		gbc_lblFotgrafo.gridy = 11;
		add(lblFotgrafo, gbc_lblFotgrafo);

		txtFotografo = new JTextField();
		txtFotografo.setColumns(10);
		GridBagConstraints gbc_txtFotografo = new GridBagConstraints();
		gbc_txtFotografo.weighty = 10.0;
		gbc_txtFotografo.weightx = 10.0;
		gbc_txtFotografo.gridwidth = 6;
		gbc_txtFotografo.insets = new Insets(0, 0, 5, 5);
		gbc_txtFotografo.fill = GridBagConstraints.BOTH;
		gbc_txtFotografo.gridx = 1;
		gbc_txtFotografo.gridy = 12;
		add(txtFotografo, gbc_txtFotografo);
		
	}
	

	

	@Override
	public String salva() {
		
		try {
			Midia au=leMidia();
			
			dao.alterar(au, nomeDoArquivoDeMidia);
			nomeDoArquivoDeMidia = txtNome.getText();
			midia=au;
			
			
			
			txtNome.setEditable(false);
			txtTitulo.setEditable(false);
			txtDescricao.setEditable(false);
			txtLocal.setEditable(false);
			formattedTextField.setEditable(false);
			txtHora.setEditable(false);
			txtPessoas.setEditable(false);
			txtFotografo.setEditable(false);
			
			return null;
		} catch (Exception e) {
			return e.getMessage();
		}
	
	}

	@Override
	public void liberaEdicao() {
		txtNome.setEditable(true);
		txtTitulo.setEditable(true);
		txtDescricao.setEditable(true);
		txtLocal.setEditable(true);
		formattedTextField.setEditable(true);
		txtHora.setEditable(true);
		txtPessoas.setEditable(true);
		txtFotografo.setEditable(true);

	}

	
	@Override
	public String criar() {

		try {			
			if (!dao.add(leMidia())) {
				return "Nome ja existe";
			}
			
			
			return "";
		} catch (Exception e) {
//			e.printStackTrace();
			return e.getMessage();
		}
	}

	@Override
	public void limpar() {
		txtNome.setText("");
		txtTitulo.setText("");
		txtDescricao.setText("");
		txtLocal.setText("");
		formattedTextField.setText("  /  /    ");		
		txtHora.setText("");
		txtPessoas.setText("");
		txtFotografo.setText("");

	}
	
	
	@SuppressWarnings("deprecation")
	private Midia leMidia() {
		int hora=0;
		Date data=new Date();
		
		try {
			int h=0;
			int m=0;
			String strHora=txtHora.getText();
			int i=0;
			
			while (i<strHora.length()&&Character.isDigit(strHora.charAt(i))) {
				i++;
			}
			if (i==strHora.length()) {
				throw new IllegalArgumentException("Dura��o mal formatada use ':' para separa minutos de segundos (mm:ss)");
			}
			h=Integer.parseInt(strHora.substring(0, i));
			if (h<0) {
				throw new IllegalArgumentException("Minutos nao podem ser negativos");
			}
			if (strHora.charAt(i)!=':') {//
				throw new IllegalArgumentException("Dura��o mal formatada use ':' para separa minutos de segundos (mm:ss)");
			}
			m=Integer.parseInt(strHora.substring(i+1));
			if (m>60) {
				throw new IllegalArgumentException("O n�mero de segundos deve estar entre 0 e 60");
			}
			if (m<0) {
				throw new IllegalArgumentException("Segundos nao podem ser negativos");
			}
			
			hora=m+(h*60);
			
		} catch (NumberFormatException e) {
			throw new IllegalArgumentException("Dura��o mal formatada");
		}
		
		String textoData=formattedTextField.getText();
		
		
		try {
			data.setDate(Integer.parseInt(textoData.substring(0, 2)));
			data.setMonth(Integer.parseInt(textoData.substring(3,5))-1);
			data.setYear(Integer.parseInt(textoData.substring(6)));			
		} catch (Exception e) {
			throw new IllegalArgumentException("Data invalida");
		}
		
		if(data.getYear()<1700){
			throw new IllegalArgumentException("Data invalida muito antiga");
		}
		
				
		return new Foto(txtNome.getText(), txtTitulo.getText(), txtDescricao.getText(), txtPessoas.getText(), txtFotografo.getText(), txtLocal.getText(), data, hora);
	}

}
