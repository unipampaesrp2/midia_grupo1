package view.auxi;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.JLabel;
import javax.swing.JTextField;

import midias.Ebook;
import midias.Midia;
import view.base.AdicionadorListador;
import controler.DAOMidia;

/**
 * Painel adicionador listador de {@link Ebook}
 */
@SuppressWarnings("serial")
public class PrototipoEBookGrid extends AdicionadorListador {
	private JTextField txtTitulo;
	private JTextField txtNome;
	private JLabel label_2;
	private JTextField txtDescricao;
	private JLabel lblGenero;
	private JTextField txtGenero;
	private JLabel lblAno;
	private JTextField txtAno;
	private JLabel lblLocal;
	private JTextField txtLocal;
	private JLabel lblNmeroDePginas;
	private JTextField txtNPaginas;
	private JLabel label_9;
	private JTextField txtEditora;

	/**
	 * Cria painel com fu��o de adicionar {@link Ebook}s
	 * @param dao DAO onde serao adicionados os dados
	 */
	public PrototipoEBookGrid(DAOMidia dao) {
		this();
		super.dao=dao;
	}
	/**
	 * Cria painel com a fun��o de listar os dados de um {@link Ebook} e manipola-los
	 * @param ebook {@link Ebook} do qual os dados ser�o listados
	 */
	public PrototipoEBookGrid(Ebook ebook) {
		this();
		nomeDoArquivoDeMidia=ebook.getNome();
		midia=(Midia)ebook;
		
		txtNome.setText(ebook.getNome());
		txtTitulo.setText(ebook.getTitulo());
		txtDescricao.setText(ebook.getDescricao());
		
		txtGenero.setText(ebook.getGenero());
				txtAno.setText(Integer.toString(ebook.getAno()));
		txtLocal.setText(ebook.getLocal());
		txtEditora.setText(ebook.getEditora());
		txtNPaginas.setText(Integer.toString(ebook.getnPaginas()));
		

		txtNome.setEditable(false);
		txtTitulo.setEditable(false);
		txtGenero.setEditable(false);
		txtAno.setEditable(false);
		txtDescricao.setEditable(false);
		txtLocal.setEditable(false);
		txtEditora.setEditable(false);
		txtNPaginas.setEditable(false);
		
	}
	
	
	/**
	 * Create the panel.
	 */
	public PrototipoEBookGrid() {
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
		gridBagLayout.rowHeights = new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
		gridBagLayout.columnWeights = new double[]{0.0, 1.0, 0.0, 1.0, 0.0, 1.0, 0.0, 1.0, 0.0, Double.MIN_VALUE};
		gridBagLayout.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		setLayout(gridBagLayout);
		
		JLabel label = new JLabel("Titulo:");
		GridBagConstraints gbc_label = new GridBagConstraints();
		gbc_label.weightx = 10.0;
		gbc_label.weighty = 10.0;
		gbc_label.anchor = GridBagConstraints.SOUTHWEST;
		gbc_label.insets = new Insets(0, 0, 5, 5);
		gbc_label.gridx = 1;
		gbc_label.gridy = 1;
		add(label, gbc_label);
		
		txtTitulo = new JTextField();
		txtTitulo.setColumns(10);
		GridBagConstraints gbc_txtTitulo = new GridBagConstraints();
		gbc_txtTitulo.weighty = 10.0;
		gbc_txtTitulo.weightx = 10.0;
		gbc_txtTitulo.gridwidth = 7;
		gbc_txtTitulo.insets = new Insets(0, 0, 5, 5);
		gbc_txtTitulo.fill = GridBagConstraints.BOTH;
		gbc_txtTitulo.gridx = 1;
		gbc_txtTitulo.gridy = 2;
		add(txtTitulo, gbc_txtTitulo);
		
		JLabel label_1 = new JLabel("Nome:");
		GridBagConstraints gbc_label_1 = new GridBagConstraints();
		gbc_label_1.weightx = 10.0;
		gbc_label_1.weighty = 10.0;
		gbc_label_1.insets = new Insets(0, 0, 5, 5);
		gbc_label_1.anchor = GridBagConstraints.SOUTHWEST;
		gbc_label_1.gridx = 1;
		gbc_label_1.gridy = 3;
		add(label_1, gbc_label_1);
		
		txtNome = new JTextField();
		txtNome.setColumns(10);
		GridBagConstraints gbc_txtNome = new GridBagConstraints();
		gbc_txtNome.weighty = 10.0;
		gbc_txtNome.weightx = 10.0;
		gbc_txtNome.gridwidth = 7;
		gbc_txtNome.insets = new Insets(0, 0, 5, 5);
		gbc_txtNome.fill = GridBagConstraints.BOTH;
		gbc_txtNome.gridx = 1;
		gbc_txtNome.gridy = 4;
		add(txtNome, gbc_txtNome);
		
		label_2 = new JLabel("Descri\u00E7\u00E3o:");
		GridBagConstraints gbc_label_2 = new GridBagConstraints();
		gbc_label_2.weightx = 10.0;
		gbc_label_2.weighty = 10.0;
		gbc_label_2.insets = new Insets(0, 0, 5, 5);
		gbc_label_2.anchor = GridBagConstraints.SOUTHWEST;
		gbc_label_2.gridx = 1;
		gbc_label_2.gridy = 5;
		add(label_2, gbc_label_2);
		
		txtDescricao = new JTextField();
		txtDescricao.setColumns(10);
		GridBagConstraints gbc_txtDescricao = new GridBagConstraints();
		gbc_txtDescricao.weighty = 10.0;
		gbc_txtDescricao.weightx = 10.0;
		gbc_txtDescricao.gridwidth = 7;
		gbc_txtDescricao.insets = new Insets(0, 0, 5, 5);
		gbc_txtDescricao.fill = GridBagConstraints.BOTH;
		gbc_txtDescricao.gridx = 1;
		gbc_txtDescricao.gridy = 6;
		add(txtDescricao, gbc_txtDescricao);
		
		lblGenero = new JLabel("Genero");
		GridBagConstraints gbc_lblGenero = new GridBagConstraints();
		gbc_lblGenero.weighty = 10.0;
		gbc_lblGenero.weightx = 10.0;
		gbc_lblGenero.insets = new Insets(0, 0, 5, 5);
		gbc_lblGenero.anchor = GridBagConstraints.SOUTHWEST;
		gbc_lblGenero.gridx = 1;
		gbc_lblGenero.gridy = 7;
		add(lblGenero, gbc_lblGenero);
		
		lblAno = new JLabel("Ano:");
		GridBagConstraints gbc_lblAno = new GridBagConstraints();
		gbc_lblAno.weighty = 10.0;
		gbc_lblAno.weightx = 10.0;
		gbc_lblAno.anchor = GridBagConstraints.SOUTHWEST;
		gbc_lblAno.insets = new Insets(0, 0, 5, 5);
		gbc_lblAno.gridx = 5;
		gbc_lblAno.gridy = 7;
		add(lblAno, gbc_lblAno);
		
		txtGenero = new JTextField();
		txtGenero.setColumns(10);
		GridBagConstraints gbc_txtGenero = new GridBagConstraints();
		gbc_txtGenero.weighty = 10.0;
		gbc_txtGenero.weightx = 10.0;
		gbc_txtGenero.gridwidth = 2;
		gbc_txtGenero.insets = new Insets(0, 0, 5, 5);
		gbc_txtGenero.fill = GridBagConstraints.BOTH;
		gbc_txtGenero.gridx = 1;
		gbc_txtGenero.gridy = 8;
		add(txtGenero, gbc_txtGenero);
		
		txtAno = new JTextField();
		txtAno.setColumns(10);
		GridBagConstraints gbc_txtAno = new GridBagConstraints();
		gbc_txtAno.weightx = 10.0;
		gbc_txtAno.weighty = 10.0;
		gbc_txtAno.gridwidth = 3;
		gbc_txtAno.insets = new Insets(0, 0, 5, 5);
		gbc_txtAno.fill = GridBagConstraints.BOTH;
		gbc_txtAno.gridx = 5;
		gbc_txtAno.gridy = 8;
		add(txtAno, gbc_txtAno);
		
		lblLocal = new JLabel("Local:");
		GridBagConstraints gbc_lblLocal = new GridBagConstraints();
		gbc_lblLocal.weighty = 10.0;
		gbc_lblLocal.weightx = 10.0;
		gbc_lblLocal.anchor = GridBagConstraints.SOUTHWEST;
		gbc_lblLocal.insets = new Insets(0, 0, 5, 5);
		gbc_lblLocal.gridx = 1;
		gbc_lblLocal.gridy = 9;
		add(lblLocal, gbc_lblLocal);
		
		txtLocal = new JTextField();
		txtLocal.setColumns(10);
		GridBagConstraints gbc_txtLocal = new GridBagConstraints();
		gbc_txtLocal.weighty = 10.0;
		gbc_txtLocal.weightx = 10.0;
		gbc_txtLocal.gridwidth = 7;
		gbc_txtLocal.insets = new Insets(0, 0, 5, 5);
		gbc_txtLocal.fill = GridBagConstraints.BOTH;
		gbc_txtLocal.gridx = 1;
		gbc_txtLocal.gridy = 10;
		add(txtLocal, gbc_txtLocal);
		
		lblNmeroDePginas = new JLabel("N\u00FAmero de p\u00E1ginas:");
		GridBagConstraints gbc_lblNmeroDePginas = new GridBagConstraints();
		gbc_lblNmeroDePginas.weighty = 10.0;
		gbc_lblNmeroDePginas.weightx = 10.0;
		gbc_lblNmeroDePginas.anchor = GridBagConstraints.SOUTHWEST;
		gbc_lblNmeroDePginas.insets = new Insets(0, 0, 5, 5);
		gbc_lblNmeroDePginas.gridx = 1;
		gbc_lblNmeroDePginas.gridy = 11;
		add(lblNmeroDePginas, gbc_lblNmeroDePginas);
		
		label_9 = new JLabel("Editora:");
		GridBagConstraints gbc_label_9 = new GridBagConstraints();
		gbc_label_9.weighty = 10.0;
		gbc_label_9.weightx = 10.0;
		gbc_label_9.anchor = GridBagConstraints.SOUTHWEST;
		gbc_label_9.insets = new Insets(0, 0, 5, 5);
		gbc_label_9.gridx = 5;
		gbc_label_9.gridy = 11;
		add(label_9, gbc_label_9);
		
		txtNPaginas = new JTextField();
		txtNPaginas.setColumns(10);
		GridBagConstraints gbc_txtNPaginas = new GridBagConstraints();
		gbc_txtNPaginas.weighty = 10.0;
		gbc_txtNPaginas.weightx = 10.0;
		gbc_txtNPaginas.gridwidth = 2;
		gbc_txtNPaginas.insets = new Insets(0, 0, 5, 5);
		gbc_txtNPaginas.fill = GridBagConstraints.BOTH;
		gbc_txtNPaginas.gridx = 1;
		gbc_txtNPaginas.gridy = 12;
		add(txtNPaginas, gbc_txtNPaginas);
		
		txtEditora = new JTextField();
		txtEditora.setColumns(10);
		GridBagConstraints gbc_txtEditora = new GridBagConstraints();
		gbc_txtEditora.weightx = 10.0;
		gbc_txtEditora.weighty = 10.0;
		gbc_txtEditora.gridwidth = 3;
		gbc_txtEditora.insets = new Insets(0, 0, 5, 5);
		gbc_txtEditora.fill = GridBagConstraints.BOTH;
		gbc_txtEditora.gridx = 5;
		gbc_txtEditora.gridy = 12;
		add(txtEditora, gbc_txtEditora);

	}

	
	public String salva() {
		
		try {
			Midia au=leMidia();
			
			dao.alterar(au, nomeDoArquivoDeMidia);
			nomeDoArquivoDeMidia = txtNome.getText();
			midia=au;
			
			
			
			txtNome.setEditable(false);
			txtTitulo.setEditable(false);
			txtGenero.setEditable(false);
			txtAno.setEditable(false);
			txtDescricao.setEditable(false);
			txtLocal.setEditable(false);
			txtEditora.setEditable(false);
			txtNPaginas.setEditable(false);
			
			return null;
		} catch (Exception e) {
			return e.getMessage();
		}
	
	}

	@Override
	public void liberaEdicao() {
		txtNome.setEditable(true);
		txtTitulo.setEditable(true);
		txtGenero.setEditable(true);
		txtAno.setEditable(true);
		txtDescricao.setEditable(true);
		txtLocal.setEditable(true);
		txtEditora.setEditable(true);
		txtNPaginas.setEditable(true);

	}

	
	@Override
	public String criar() {

		try {			
			if (!dao.add(leMidia())) {
				return "Nome ja existe";
			}
			
			
			return "";
		} catch (Exception e) {
//			e.printStackTrace();
			return e.getMessage();
		}
	}

	@Override
	public void limpar() {
		txtNome.setText("");
		txtTitulo.setText("");
		txtGenero.setText("");
		txtAno.setText("");
		txtDescricao.setText("");
		txtLocal.setText("");
		txtEditora.setText("");
		txtNPaginas.setText("");

	}
	
	
	private Midia leMidia() {
		int nPaginas=0;
		int ano=0;
		
		try {
			ano=Integer.parseInt(txtAno.getText());										
		} catch (NumberFormatException e) {
			throw new IllegalArgumentException("Formato do ano invalido");
		}
		try {
			nPaginas=Integer.parseInt(txtNPaginas.getText());										
		} catch (NumberFormatException e) {
			throw new IllegalArgumentException("Formato do ano invalido");
		}
		
		
		return new Ebook(txtGenero.getText(), ano, txtNome.getText(), txtDescricao.getText() , txtTitulo.getText(), txtLocal.getText(), txtEditora.getText(), nPaginas);
	}

	
	

}
