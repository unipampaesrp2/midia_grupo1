package view.auxi;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JTextField;

import midias.Jogos;
import midias.Midia;
import view.base.AdicionadorListador;
import controler.DAOMidia;

/**
 * Painel adicionador listador de {@link Jogos}
 */
@SuppressWarnings("serial")
public class PrototipoJogosGrid extends AdicionadorListador {


	private JTextField txtTitulo;
	private JTextField txtNome;
	private JLabel label_2;
	private JTextField txtDescricao;
	private JLabel lblGenero;
	private JTextField txtGenero;
	private JLabel lblAno;
	private JTextField txtAno;
	private JLabel lblNmeroDeJogadoresl;
	private JLabel lblSuporteARede_1;
	private JCheckBox checkBox;
	private JLabel lblNewLabel;
	private JTextField txtNJogadores;
	private JTextField txtAutores;

	
	/**
	 * Cria painel com fu��o de adicionar {@link Jogos}s
	 * @param dao DAO onde serao adicionados os dados
	 */
	public PrototipoJogosGrid(DAOMidia dao) {
		this();
		super.dao=dao;
	}
	/**
	 * Cria painel com a fun��o de listar os dados de um {@link Jogos} e manipola-los
	 * @param jogos {@link Jogos} do qual os dados ser�o listados
	 */
	public PrototipoJogosGrid(Jogos jogos) {
		this();
		nomeDoArquivoDeMidia=jogos.getNome();
		midia=(Midia)jogos;
		
		txtNome.setText(jogos.getNome());
		txtTitulo.setText(jogos.getTitulo());
		txtDescricao.setText(jogos.getDescricao());
		txtGenero.setText(jogos.getGenero());
		txtAutores.setText(jogos.getAutores());
		txtNJogadores.setText(Integer.toString(jogos.getnJogadores()));
		checkBox.setSelected(jogos.isSup_a_rede());
		txtAno.setText(Integer.toString(jogos.getAno()));
		
		

		txtNome.setEditable(false);
		txtTitulo.setEditable(false);
		txtDescricao.setEditable(false);
		txtGenero.setEditable(false);
		txtAutores.setEditable(false);
		txtNJogadores.setEditable(false);
		txtAno.setEditable(false);
		txtNJogadores.setEditable(false);
		checkBox.setEnabled(false);
		

	}

	/**
	 * Create the panel.
	 */
	public PrototipoJogosGrid() {
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
		gridBagLayout.rowHeights = new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
		gridBagLayout.columnWeights = new double[]{0.0, 1.0, 0.0, 1.0, 0.0, 1.0, 0.0, 1.0, 0.0, Double.MIN_VALUE};
		gridBagLayout.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		setLayout(gridBagLayout);
		
		JLabel label = new JLabel("Titulo:");
		GridBagConstraints gbc_label = new GridBagConstraints();
		gbc_label.weightx = 10.0;
		gbc_label.weighty = 10.0;
		gbc_label.anchor = GridBagConstraints.SOUTHWEST;
		gbc_label.insets = new Insets(0, 0, 5, 5);
		gbc_label.gridx = 1;
		gbc_label.gridy = 1;
		add(label, gbc_label);
		
		txtTitulo = new JTextField();
		txtTitulo.setColumns(10);
		GridBagConstraints gbc_txtTitulo = new GridBagConstraints();
		gbc_txtTitulo.weightx = 10.0;
		gbc_txtTitulo.weighty = 10.0;
		gbc_txtTitulo.gridwidth = 7;
		gbc_txtTitulo.insets = new Insets(0, 0, 5, 5);
		gbc_txtTitulo.fill = GridBagConstraints.BOTH;
		gbc_txtTitulo.gridx = 1;
		gbc_txtTitulo.gridy = 2;
		add(txtTitulo, gbc_txtTitulo);
		
		JLabel label_1 = new JLabel("Nome:");
		GridBagConstraints gbc_label_1 = new GridBagConstraints();
		gbc_label_1.weighty = 10.0;
		gbc_label_1.weightx = 10.0;
		gbc_label_1.insets = new Insets(0, 0, 5, 5);
		gbc_label_1.anchor = GridBagConstraints.SOUTHWEST;
		gbc_label_1.gridx = 1;
		gbc_label_1.gridy = 3;
		add(label_1, gbc_label_1);
		
		txtNome = new JTextField();
		txtNome.setColumns(10);
		GridBagConstraints gbc_txtNome = new GridBagConstraints();
		gbc_txtNome.weightx = 10.0;
		gbc_txtNome.weighty = 10.0;
		gbc_txtNome.gridwidth = 7;
		gbc_txtNome.insets = new Insets(0, 0, 5, 5);
		gbc_txtNome.fill = GridBagConstraints.BOTH;
		gbc_txtNome.gridx = 1;
		gbc_txtNome.gridy = 4;
		add(txtNome, gbc_txtNome);
		
		label_2 = new JLabel("Descricao:");
		GridBagConstraints gbc_label_2 = new GridBagConstraints();
		gbc_label_2.weighty = 10.0;
		gbc_label_2.weightx = 10.0;
		gbc_label_2.insets = new Insets(0, 0, 5, 5);
		gbc_label_2.anchor = GridBagConstraints.SOUTHWEST;
		gbc_label_2.gridx = 1;
		gbc_label_2.gridy = 5;
		add(label_2, gbc_label_2);
		
		txtDescricao = new JTextField();
		txtDescricao.setColumns(10);
		GridBagConstraints gbc_txtDescricao = new GridBagConstraints();
		gbc_txtDescricao.weightx = 10.0;
		gbc_txtDescricao.weighty = 10.0;
		gbc_txtDescricao.gridwidth = 7;
		gbc_txtDescricao.insets = new Insets(0, 0, 5, 5);
		gbc_txtDescricao.fill = GridBagConstraints.BOTH;
		gbc_txtDescricao.gridx = 1;
		gbc_txtDescricao.gridy = 6;
		add(txtDescricao, gbc_txtDescricao);
		
		lblGenero = new JLabel("Genero:");
		GridBagConstraints gbc_lblGenero = new GridBagConstraints();
		gbc_lblGenero.weighty = 10.0;
		gbc_lblGenero.weightx = 10.0;
		gbc_lblGenero.insets = new Insets(0, 0, 5, 5);
		gbc_lblGenero.anchor = GridBagConstraints.SOUTHWEST;
		gbc_lblGenero.gridx = 1;
		gbc_lblGenero.gridy = 7;
		add(lblGenero, gbc_lblGenero);
		
		lblAno = new JLabel("Ano:");
		GridBagConstraints gbc_lblAno = new GridBagConstraints();
		gbc_lblAno.weightx = 10.0;
		gbc_lblAno.weighty = 10.0;
		gbc_lblAno.anchor = GridBagConstraints.SOUTHWEST;
		gbc_lblAno.insets = new Insets(0, 0, 5, 5);
		gbc_lblAno.gridx = 5;
		gbc_lblAno.gridy = 7;
		add(lblAno, gbc_lblAno);
		
		txtGenero = new JTextField();
		txtGenero.setColumns(10);
		GridBagConstraints gbc_txtGenero = new GridBagConstraints();
		gbc_txtGenero.weightx = 10.0;
		gbc_txtGenero.weighty = 10.0;
		gbc_txtGenero.gridwidth = 3;
		gbc_txtGenero.insets = new Insets(0, 0, 5, 5);
		gbc_txtGenero.fill = GridBagConstraints.BOTH;
		gbc_txtGenero.gridx = 1;
		gbc_txtGenero.gridy = 8;
		add(txtGenero, gbc_txtGenero);
		
		txtAno = new JTextField();
		txtAno.setColumns(10);
		GridBagConstraints gbc_txtAno = new GridBagConstraints();
		gbc_txtAno.weighty = 10.0;
		gbc_txtAno.weightx = 10.0;
		gbc_txtAno.gridwidth = 3;
		gbc_txtAno.insets = new Insets(0, 0, 5, 5);
		gbc_txtAno.fill = GridBagConstraints.BOTH;
		gbc_txtAno.gridx = 5;
		gbc_txtAno.gridy = 8;
		add(txtAno, gbc_txtAno);
		
		lblNewLabel = new JLabel("Autores:");
		GridBagConstraints gbc_lblNewLabel = new GridBagConstraints();
		gbc_lblNewLabel.weightx = 10.0;
		gbc_lblNewLabel.weighty = 10.0;
		gbc_lblNewLabel.anchor = GridBagConstraints.SOUTHWEST;
		gbc_lblNewLabel.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel.gridx = 1;
		gbc_lblNewLabel.gridy = 9;
		add(lblNewLabel, gbc_lblNewLabel);
		
		txtAutores = new JTextField();
		GridBagConstraints gbc_txtAutores = new GridBagConstraints();
		gbc_txtAutores.gridwidth = 7;
		gbc_txtAutores.weighty = 10.0;
		gbc_txtAutores.weightx = 10.0;
		gbc_txtAutores.insets = new Insets(0, 0, 5, 5);
		gbc_txtAutores.fill = GridBagConstraints.BOTH;
		gbc_txtAutores.gridx = 1;
		gbc_txtAutores.gridy = 10;
		add(txtAutores, gbc_txtAutores);
		txtAutores.setColumns(10);
		
		lblSuporteARede_1 = new JLabel("Suporte a Rede:");
		GridBagConstraints gbc_lblSuporteARede_1 = new GridBagConstraints();
		gbc_lblSuporteARede_1.weightx = 10.0;
		gbc_lblSuporteARede_1.weighty = 10.0;
		gbc_lblSuporteARede_1.anchor = GridBagConstraints.SOUTH;
		gbc_lblSuporteARede_1.insets = new Insets(0, 0, 5, 5);
		gbc_lblSuporteARede_1.gridx = 5;
		gbc_lblSuporteARede_1.gridy = 11;
		add(lblSuporteARede_1, gbc_lblSuporteARede_1);
		
		lblNmeroDeJogadoresl = new JLabel("N\u00FAmero de Jogadores:");
		GridBagConstraints gbc_lblNmeroDeJogadoresl = new GridBagConstraints();
		gbc_lblNmeroDeJogadoresl.weightx = 10.0;
		gbc_lblNmeroDeJogadoresl.weighty = 10.0;
		gbc_lblNmeroDeJogadoresl.anchor = GridBagConstraints.SOUTHWEST;
		gbc_lblNmeroDeJogadoresl.insets = new Insets(0, 0, 5, 5);
		gbc_lblNmeroDeJogadoresl.gridx = 1;
		gbc_lblNmeroDeJogadoresl.gridy = 11;
		add(lblNmeroDeJogadoresl, gbc_lblNmeroDeJogadoresl);
		
		txtNJogadores = new JTextField();
		GridBagConstraints gbc_txtNJogadores = new GridBagConstraints();
		gbc_txtNJogadores.weighty = 10.0;
		gbc_txtNJogadores.weightx = 10.0;
		gbc_txtNJogadores.insets = new Insets(0, 0, 5, 5);
		gbc_txtNJogadores.fill = GridBagConstraints.BOTH;
		gbc_txtNJogadores.gridx = 1;
		gbc_txtNJogadores.gridy = 12;
		add(txtNJogadores, gbc_txtNJogadores);
		txtNJogadores.setColumns(10);
		
		checkBox = new JCheckBox("Sim");
		GridBagConstraints gbc_checkBox = new GridBagConstraints();
		gbc_checkBox.weighty = 10.0;
		gbc_checkBox.weightx = 10.0;
		gbc_checkBox.insets = new Insets(0, 0, 5, 5);
		gbc_checkBox.gridx = 5;
		gbc_checkBox.gridy = 12;
		add(checkBox, gbc_checkBox);

	}

	




	@Override
	public String salva() {
		
		try {
			Midia au=leMidia();
			
			dao.alterar(au, nomeDoArquivoDeMidia);
			nomeDoArquivoDeMidia = txtNome.getText();
			midia=au;
			
			
			
			txtNome.setEditable(false);
			txtTitulo.setEditable(false);
			txtGenero.setEditable(false);
			txtAno.setEditable(false);
			txtAutores.setEditable(false);
			txtDescricao.setEditable(false);
			txtNJogadores.setEditable(false);
			checkBox.setEnabled(false);
			
			return null;
		} catch (Exception e) {
			return e.getMessage();
		}
	
	}

	@Override
	public void liberaEdicao() {
		txtNome.setEditable(true);
		txtTitulo.setEditable(true);
		txtGenero.setEditable(true);
		txtAno.setEditable(true);
		txtAutores.setEditable(true);
		txtDescricao.setEditable(true);
		txtNJogadores.setEditable(true);
		checkBox.setEnabled(true);

	}

	
	@Override
	public String criar() {

		try {			
			if (!dao.add(leMidia())) {
				return "Nome ja existe";
			}
			
			
			return "";
		} catch (Exception e) {
//			e.printStackTrace();
			return e.getMessage();
		}
	}

	@Override
	public void limpar() {
		txtNome.setText("");
		txtTitulo.setText("");
		txtGenero.setText("");
		txtAno.setText("");
		txtAutores.setText("");
		txtDescricao.setText("");
		txtNJogadores.setText("");
		checkBox.setSelected(false);

	}
	
	
	private Midia leMidia() {
		int numeroDeJogadores=0;
		int ano=0;
		
		try {
			ano=Integer.parseInt(txtAno.getText());										
		} catch (NumberFormatException e) {
			throw new IllegalArgumentException("Formato do ano invalido");
		}
		
		if (ano < 1900) {
			throw new IllegalArgumentException("Ano sem tecnologia para isso");
		}
		
		try {
			numeroDeJogadores=Integer.parseInt(txtNJogadores.getText());
		} catch (Exception e) {
			throw new IllegalArgumentException("Formato de numero de jogadores invalido");
		}
		
		if (numeroDeJogadores<1) {
			throw new IllegalArgumentException("Para ser um jogo � necessario que pelo menos exista um jogador");
		}
		
		
		
		
		return new Jogos(txtGenero.getText(), ano, txtNome.getText(), txtDescricao.getText(), txtTitulo.getText(), numeroDeJogadores, checkBox.isSelected(), txtAutores.getText());
	}
}
