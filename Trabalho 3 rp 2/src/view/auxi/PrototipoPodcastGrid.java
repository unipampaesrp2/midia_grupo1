package view.auxi;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.JLabel;
import javax.swing.JTextField;

import midias.Midia;
import midias.Podcast;
import view.base.AdicionadorListador;
import controler.DAOMidia;
/**
 * Painel adicionador listador de {@link Podcast}
 */
@SuppressWarnings("serial")
public class PrototipoPodcastGrid extends AdicionadorListador {

	private JTextField txtTitulo;
	private JTextField txtNome;
	private JLabel label_2;
	private JLabel lblGenero;
	private JTextField txtGenero;
	private JLabel label_4;
	private JTextField txtIdioma;
	private JLabel label_5;
	private JTextField txtDuracao;
	private JLabel label_6;
	private JTextField txtAno;
	private JLabel label_7;
	private JTextField txtAutores;
	private JTextField txtDescricao;

	/**
	 * Cria painel com fu��o de adicionar {@link Podcast}s
	 * @param dao DAO onde serao adicionados os dados
	 */
	public PrototipoPodcastGrid(DAOMidia dao) {
		this();
		super.dao=dao;
	}
	
	/**
	 * Cria painel com a fun��o de listar os dados de um {@link Podcast} e manipola-los
	 * @param podcast {@link Podcast} do qual os dados ser�o listados
	 */
	public PrototipoPodcastGrid(Podcast	podcast) {
		this();
		nomeDoArquivoDeMidia=podcast.getNome();
		midia=(Midia)podcast;
		
		txtNome.setText(podcast.getNome());
		txtTitulo.setText(podcast.getTitulo());
		txtDescricao.setText(podcast.getDescricao());
		txtDuracao.setText(Integer.toString((podcast.getDuracao() / 60))
				+ ":" + Integer.toString(podcast.getDuracao() % 60));
		txtIdioma.setText(podcast.getIdioma());
		txtGenero.setText(podcast.getGenero());
		txtAutores.setText(podcast.getAutores());
		txtAno.setText(Integer.toString(podcast.getAno()));
		
		

		txtNome.setEditable(false);
		txtTitulo.setEditable(false);
		txtGenero.setEditable(false);
		txtIdioma.setEditable(false);
		txtDuracao.setEditable(false);
		txtAno.setEditable(false);
		txtAutores.setEditable(false);
		txtDescricao.setEditable(false);

	}
	
	
	
	
	
	/**
	 * Create the panel.
	 */
	public PrototipoPodcastGrid() {
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
		gridBagLayout.rowHeights = new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
		gridBagLayout.columnWeights = new double[]{0.0, 1.0, 0.0, 1.0, 0.0, 1.0, 0.0, 1.0, 0.0, Double.MIN_VALUE};
		gridBagLayout.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		setLayout(gridBagLayout);
		
		JLabel label = new JLabel("Titulo:");
		GridBagConstraints gbc_label = new GridBagConstraints();
		gbc_label.weighty = 10.0;
		gbc_label.weightx = 10.0;
		gbc_label.anchor = GridBagConstraints.SOUTHWEST;
		gbc_label.insets = new Insets(0, 0, 5, 5);
		gbc_label.gridx = 1;
		gbc_label.gridy = 1;
		add(label, gbc_label);
		
		txtTitulo = new JTextField();
		txtTitulo.setColumns(10);
		GridBagConstraints gbc_txtTitulo = new GridBagConstraints();
		gbc_txtTitulo.weightx = 10.0;
		gbc_txtTitulo.weighty = 10.0;
		gbc_txtTitulo.gridwidth = 7;
		gbc_txtTitulo.insets = new Insets(0, 0, 5, 5);
		gbc_txtTitulo.fill = GridBagConstraints.BOTH;
		gbc_txtTitulo.gridx = 1;
		gbc_txtTitulo.gridy = 2;
		add(txtTitulo, gbc_txtTitulo);
		
		JLabel label_1 = new JLabel("Nome:");
		GridBagConstraints gbc_label_1 = new GridBagConstraints();
		gbc_label_1.weighty = 10.0;
		gbc_label_1.weightx = 10.0;
		gbc_label_1.insets = new Insets(0, 0, 5, 5);
		gbc_label_1.anchor = GridBagConstraints.SOUTHWEST;
		gbc_label_1.gridx = 1;
		gbc_label_1.gridy = 3;
		add(label_1, gbc_label_1);
		
		txtNome = new JTextField();
		txtNome.setColumns(10);
		GridBagConstraints gbc_txtNome = new GridBagConstraints();
		gbc_txtNome.weighty = 10.0;
		gbc_txtNome.weightx = 10.0;
		gbc_txtNome.gridwidth = 7;
		gbc_txtNome.insets = new Insets(0, 0, 5, 5);
		gbc_txtNome.fill = GridBagConstraints.BOTH;
		gbc_txtNome.gridx = 1;
		gbc_txtNome.gridy = 4;
		add(txtNome, gbc_txtNome);
		
		label_2 = new JLabel("Descri\u00E7\u00E3o:");
		GridBagConstraints gbc_label_2 = new GridBagConstraints();
		gbc_label_2.weighty = 10.0;
		gbc_label_2.weightx = 10.0;
		gbc_label_2.insets = new Insets(0, 0, 5, 5);
		gbc_label_2.anchor = GridBagConstraints.SOUTHWEST;
		gbc_label_2.gridx = 1;
		gbc_label_2.gridy = 5;
		add(label_2, gbc_label_2);
		
		txtDescricao = new JTextField();
		txtDescricao.setColumns(10);
		GridBagConstraints gbc_txtDescricao = new GridBagConstraints();
		gbc_txtDescricao.weightx = 10.0;
		gbc_txtDescricao.weighty = 10.0;
		gbc_txtDescricao.gridwidth = 7;
		gbc_txtDescricao.insets = new Insets(0, 0, 5, 5);
		gbc_txtDescricao.fill = GridBagConstraints.BOTH;
		gbc_txtDescricao.gridx = 1;
		gbc_txtDescricao.gridy = 6;
		add(txtDescricao, gbc_txtDescricao);
		
		lblGenero = new JLabel("Genero");
		GridBagConstraints gbc_lblGenero = new GridBagConstraints();
		gbc_lblGenero.weighty = 10.0;
		gbc_lblGenero.weightx = 10.0;
		gbc_lblGenero.insets = new Insets(0, 0, 5, 5);
		gbc_lblGenero.anchor = GridBagConstraints.SOUTHWEST;
		gbc_lblGenero.gridx = 1;
		gbc_lblGenero.gridy = 7;
		add(lblGenero, gbc_lblGenero);
		
		label_4 = new JLabel("Idioma:");
		GridBagConstraints gbc_label_4 = new GridBagConstraints();
		gbc_label_4.weighty = 10.0;
		gbc_label_4.weightx = 10.0;
		gbc_label_4.anchor = GridBagConstraints.SOUTHWEST;
		gbc_label_4.insets = new Insets(0, 0, 5, 5);
		gbc_label_4.gridx = 3;
		gbc_label_4.gridy = 7;
		add(label_4, gbc_label_4);
		
		label_5 = new JLabel("Dura\u00E7\u00E3o:");
		GridBagConstraints gbc_label_5 = new GridBagConstraints();
		gbc_label_5.anchor = GridBagConstraints.SOUTHWEST;
		gbc_label_5.insets = new Insets(0, 0, 5, 5);
		gbc_label_5.gridx = 5;
		gbc_label_5.gridy = 7;
		add(label_5, gbc_label_5);
		
		label_6 = new JLabel("Ano:");
		GridBagConstraints gbc_label_6 = new GridBagConstraints();
		gbc_label_6.weighty = 10.0;
		gbc_label_6.weightx = 10.0;
		gbc_label_6.anchor = GridBagConstraints.SOUTHWEST;
		gbc_label_6.insets = new Insets(0, 0, 5, 5);
		gbc_label_6.gridx = 7;
		gbc_label_6.gridy = 7;
		add(label_6, gbc_label_6);
		
		txtGenero = new JTextField();
		txtGenero.setColumns(10);
		GridBagConstraints gbc_txtGenero = new GridBagConstraints();
		gbc_txtGenero.weighty = 10.0;
		gbc_txtGenero.weightx = 10.0;
		gbc_txtGenero.insets = new Insets(0, 0, 5, 5);
		gbc_txtGenero.fill = GridBagConstraints.BOTH;
		gbc_txtGenero.gridx = 1;
		gbc_txtGenero.gridy = 8;
		add(txtGenero, gbc_txtGenero);
		
		txtIdioma = new JTextField();
		txtIdioma.setColumns(10);
		GridBagConstraints gbc_txtIdioma = new GridBagConstraints();
		gbc_txtIdioma.weightx = 10.0;
		gbc_txtIdioma.weighty = 10.0;
		gbc_txtIdioma.insets = new Insets(0, 0, 5, 5);
		gbc_txtIdioma.fill = GridBagConstraints.BOTH;
		gbc_txtIdioma.gridx = 3;
		gbc_txtIdioma.gridy = 8;
		add(txtIdioma, gbc_txtIdioma);
		
		txtDuracao = new JTextField();
		txtDuracao.setColumns(10);
		GridBagConstraints gbc_txtDuracao = new GridBagConstraints();
		gbc_txtDuracao.weighty = 10.0;
		gbc_txtDuracao.weightx = 10.0;
		gbc_txtDuracao.insets = new Insets(0, 0, 5, 5);
		gbc_txtDuracao.fill = GridBagConstraints.BOTH;
		gbc_txtDuracao.gridx = 5;
		gbc_txtDuracao.gridy = 8;
		add(txtDuracao, gbc_txtDuracao);
		
		txtAno = new JTextField();
		txtAno.setColumns(10);
		GridBagConstraints gbc_txtAno = new GridBagConstraints();
		gbc_txtAno.weightx = 10.0;
		gbc_txtAno.weighty = 10.0;
		gbc_txtAno.insets = new Insets(0, 0, 5, 5);
		gbc_txtAno.fill = GridBagConstraints.BOTH;
		gbc_txtAno.gridx = 7;
		gbc_txtAno.gridy = 8;
		add(txtAno, gbc_txtAno);
		
		label_7 = new JLabel("Autores:");
		GridBagConstraints gbc_label_7 = new GridBagConstraints();
		gbc_label_7.weightx = 10.0;
		gbc_label_7.weighty = 10.0;
		gbc_label_7.anchor = GridBagConstraints.SOUTHWEST;
		gbc_label_7.insets = new Insets(0, 0, 5, 5);
		gbc_label_7.gridx = 1;
		gbc_label_7.gridy = 9;
		add(label_7, gbc_label_7);
		
		txtAutores = new JTextField();
		txtAutores.setColumns(10);
		GridBagConstraints gbc_txtAutores = new GridBagConstraints();
		gbc_txtAutores.weighty = 10.0;
		gbc_txtAutores.weightx = 10.0;
		gbc_txtAutores.gridwidth = 7;
		gbc_txtAutores.insets = new Insets(0, 0, 5, 5);
		gbc_txtAutores.fill = GridBagConstraints.BOTH;
		gbc_txtAutores.gridx = 1;
		gbc_txtAutores.gridy = 10;
		add(txtAutores, gbc_txtAutores);

	}
	
	
	
	@Override
	public String salva() {
		
		try {
			Midia au=leMidia();
			
			dao.alterar(au, nomeDoArquivoDeMidia);
			nomeDoArquivoDeMidia = txtNome.getText();
			midia=au;
			
			
			
			txtNome.setEditable(false);
			txtTitulo.setEditable(false);
			txtGenero.setEditable(false);
			txtIdioma.setEditable(false);
			txtDuracao.setEditable(false);
			txtAno.setEditable(false);
			txtAutores.setEditable(false);
			txtDescricao.setEditable(false);
			
			return null;
		} catch (Exception e) {
			return e.getMessage();
		}
	
	}

	@Override
	public void liberaEdicao() {
		txtNome.setEditable(true);
		txtTitulo.setEditable(true);
		txtGenero.setEditable(true);
		txtIdioma.setEditable(true);
		txtDuracao.setEditable(true);
		txtAno.setEditable(true);
		txtAutores.setEditable(true);
		txtDescricao.setEditable(true);

	}

	
	@Override
	public String criar() {

		try {			
			if (!dao.add(leMidia())) {
				return "Nome ja existe";
			}
			
			
			return "";
		} catch (Exception e) {
//			e.printStackTrace();
			return e.getMessage();
		}
	}

	@Override
	public void limpar() {
		txtNome.setText("");
		txtTitulo.setText("");
		txtGenero.setText("");
		txtIdioma.setText("");
		txtDuracao.setText("");
		txtAno.setText("");
		txtAutores.setText("");
		txtDescricao.setText("");

	}
	
	
	private Midia leMidia() {
		int duracao=0;
		int ano=0;
		
		try {
			ano=Integer.parseInt(txtAno.getText());										
		} catch (NumberFormatException e) {
			throw new IllegalArgumentException("Formato do ano invalido");
		}
		
		if (ano < 1800) {
			throw new IllegalArgumentException("Ano sem tecnologia para isso");
		}
		
		try {
			int m=0;
			int s=0;
			String strDuracao=txtDuracao.getText();
			int i=0;
			
			while (i<strDuracao.length()&&Character.isDigit(strDuracao.charAt(i))) {
				i++;
			}
			if (i==strDuracao.length()) {
				throw new IllegalArgumentException("Dura��o mal formatada use ':' para separa minutos de segundos (mm:ss)");
			}
			m=Integer.parseInt(strDuracao.substring(0, i));
			if (m<0) {
				throw new IllegalArgumentException("Minutos nao podem ser negativos");
			}
			if (strDuracao.charAt(i)!=':') {//
				throw new IllegalArgumentException("Dura��o mal formatada use ':' para separa minutos de segundos (mm:ss)");
			}
			s=Integer.parseInt(strDuracao.substring(i+1));
			if (s>60) {
				throw new IllegalArgumentException("O n�mero de segundos deve estar entre 0 e 60");
			}
			if (s<0) {
				throw new IllegalArgumentException("Segundos nao podem ser negativos");
			}
			
			duracao=s+(m*60);
			
		} catch (NumberFormatException e) {
			throw new IllegalArgumentException("Dura��o mal formatada");
		}
		
		return new Podcast(txtNome.getText(), txtTitulo.getText(), txtDescricao.getText(), duracao, txtIdioma.getText(), txtGenero.getText(), txtAutores.getText(), ano);
	}

}
