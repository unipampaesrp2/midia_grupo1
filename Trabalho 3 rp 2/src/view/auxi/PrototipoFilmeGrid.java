package view.auxi;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.JLabel;
import javax.swing.JTextField;

import midias.Filme;
import midias.Midia;
import view.base.AdicionadorListador;
import controler.DAOMidia;

/**
 * Painel adicionador listador de {@link Filme}
 */
@SuppressWarnings("serial")
public class PrototipoFilmeGrid extends AdicionadorListador {
	private JTextField txtTitulo;
	private JTextField txtNome;
	private JTextField txtDescricao;
	private JTextField txtGenero;
	private JTextField txtAno;
	private JLabel lblDiretores;
	private JTextField txtDiretores;
	private JLabel lblAtoresPrincipais;
	private JTextField txtAtoresPrincipais;
	
	/**
	 * Cria painel com fu��o de adicionar {@link Filme}s
	 * @param dao DAO onde serao adicionados os dados
	 */
	public PrototipoFilmeGrid(DAOMidia dao) {
		this();
		super.dao=dao;
	}
	/**
	 * Cria painel com a fun��o de listar os dados de um {@link Filme} e manipola-los
	 * @param filme {@link Filme} do qual os dados ser�o listados
	 */
	public PrototipoFilmeGrid(Filme filme){
		this();
		nomeDoArquivoDeMidia=filme.getNome();
		midia=(Midia)filme;
		
		txtNome.setText(filme.getNome());
		txtTitulo.setText(filme.getTitulo());
		txtDescricao.setText(filme.getDescricao());
		txtGenero.setText(filme.getGenero());
		txtAno.setText(Integer.toString(filme.getAno()));
		txtAtoresPrincipais.setText(filme.getAtoresPrincipais());
		txtDiretores.setText(filme.getDiretor());

		txtNome.setEditable(false);
		txtTitulo.setEditable(false);
		txtGenero.setEditable(false);
		txtAno.setEditable(false);
		txtDescricao.setEditable(false);
		txtDiretores.setEditable(false);
		txtAtoresPrincipais.setEditable(false);
	
	}
	
	/**
	 * Create the panel.
	 */
	public PrototipoFilmeGrid() {
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[] { 0, 0, 0, 0, 0, 0 };
		gridBagLayout.rowHeights = new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
				0, 0, 0, 0 };
		gridBagLayout.columnWeights = new double[] { 0.0, 1.0, 0.0, 1.0, 0.0,
				Double.MIN_VALUE };
		gridBagLayout.rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
				0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE };
		setLayout(gridBagLayout);

		JLabel label = new JLabel("Titulo:");
		GridBagConstraints gbc_label = new GridBagConstraints();
		gbc_label.weighty = 10.0;
		gbc_label.weightx = 10.0;
		gbc_label.anchor = GridBagConstraints.SOUTHWEST;
		gbc_label.insets = new Insets(0, 0, 5, 5);
		gbc_label.gridx = 1;
		gbc_label.gridy = 1;
		add(label, gbc_label);

		txtTitulo = new JTextField();
		txtTitulo.setColumns(10);
		GridBagConstraints gbc_txtTitulo = new GridBagConstraints();
		gbc_txtTitulo.weighty = 10.0;
		gbc_txtTitulo.weightx = 10.0;
		gbc_txtTitulo.gridwidth = 3;
		gbc_txtTitulo.insets = new Insets(0, 0, 5, 5);
		gbc_txtTitulo.fill = GridBagConstraints.BOTH;
		gbc_txtTitulo.gridx = 1;
		gbc_txtTitulo.gridy = 2;
		add(txtTitulo, gbc_txtTitulo);

		JLabel label_1 = new JLabel("Nome:");
		GridBagConstraints gbc_label_1 = new GridBagConstraints();
		gbc_label_1.weightx = 10.0;
		gbc_label_1.weighty = 10.0;
		gbc_label_1.insets = new Insets(0, 0, 5, 5);
		gbc_label_1.anchor = GridBagConstraints.SOUTHWEST;
		gbc_label_1.gridx = 1;
		gbc_label_1.gridy = 3;
		add(label_1, gbc_label_1);

		txtNome = new JTextField();
		txtNome.setColumns(10);
		GridBagConstraints gbc_txtNome = new GridBagConstraints();
		gbc_txtNome.weighty = 10.0;
		gbc_txtNome.weightx = 10.0;
		gbc_txtNome.gridwidth = 3;
		gbc_txtNome.insets = new Insets(0, 0, 5, 5);
		gbc_txtNome.fill = GridBagConstraints.BOTH;
		gbc_txtNome.gridx = 1;
		gbc_txtNome.gridy = 4;
		add(txtNome, gbc_txtNome);

		JLabel label_2 = new JLabel("Descri\u00E7\u00E3o:");
		GridBagConstraints gbc_label_2 = new GridBagConstraints();
		gbc_label_2.weighty = 10.0;
		gbc_label_2.weightx = 10.0;
		gbc_label_2.insets = new Insets(0, 0, 5, 5);
		gbc_label_2.anchor = GridBagConstraints.SOUTHWEST;
		gbc_label_2.gridx = 1;
		gbc_label_2.gridy = 5;
		add(label_2, gbc_label_2);

		txtDescricao = new JTextField();
		txtDescricao.setColumns(10);
		GridBagConstraints gbc_txtDescricao = new GridBagConstraints();
		gbc_txtDescricao.weighty = 10.0;
		gbc_txtDescricao.weightx = 10.0;
		gbc_txtDescricao.gridwidth = 3;
		gbc_txtDescricao.insets = new Insets(0, 0, 5, 5);
		gbc_txtDescricao.fill = GridBagConstraints.BOTH;
		gbc_txtDescricao.gridx = 1;
		gbc_txtDescricao.gridy = 6;
		add(txtDescricao, gbc_txtDescricao);

		JLabel lblGenero = new JLabel("Genero:");
		GridBagConstraints gbc_lblGenero = new GridBagConstraints();
		gbc_lblGenero.weightx = 10.0;
		gbc_lblGenero.weighty = 10.0;
		gbc_lblGenero.insets = new Insets(0, 0, 5, 5);
		gbc_lblGenero.anchor = GridBagConstraints.SOUTHWEST;
		gbc_lblGenero.gridx = 1;
		gbc_lblGenero.gridy = 7;
		add(lblGenero, gbc_lblGenero);

		JLabel lblAno = new JLabel("Ano:");
		GridBagConstraints gbc_lblAno = new GridBagConstraints();
		gbc_lblAno.weighty = 10.0;
		gbc_lblAno.weightx = 10.0;
		gbc_lblAno.anchor = GridBagConstraints.SOUTHWEST;
		gbc_lblAno.insets = new Insets(0, 0, 5, 5);
		gbc_lblAno.gridx = 3;
		gbc_lblAno.gridy = 7;
		add(lblAno, gbc_lblAno);

		txtGenero = new JTextField();
		txtGenero.setColumns(10);
		GridBagConstraints gbc_txtGenero = new GridBagConstraints();
		gbc_txtGenero.weightx = 10.0;
		gbc_txtGenero.weighty = 10.0;
		gbc_txtGenero.insets = new Insets(0, 0, 5, 5);
		gbc_txtGenero.fill = GridBagConstraints.BOTH;
		gbc_txtGenero.gridx = 1;
		gbc_txtGenero.gridy = 8;
		add(txtGenero, gbc_txtGenero);

		txtAno = new JTextField();
		GridBagConstraints gbc_txtAno = new GridBagConstraints();
		gbc_txtAno.weighty = 10.0;
		gbc_txtAno.weightx = 10.0;
		gbc_txtAno.insets = new Insets(0, 0, 5, 5);
		gbc_txtAno.fill = GridBagConstraints.BOTH;
		gbc_txtAno.gridx = 3;
		gbc_txtAno.gridy = 8;
		add(txtAno, gbc_txtAno);
		txtAno.setColumns(10);

		lblDiretores = new JLabel("Diretores:");
		GridBagConstraints gbc_lblDiretores = new GridBagConstraints();
		gbc_lblDiretores.weighty = 10.0;
		gbc_lblDiretores.weightx = 10.0;
		gbc_lblDiretores.anchor = GridBagConstraints.SOUTHWEST;
		gbc_lblDiretores.insets = new Insets(0, 0, 5, 5);
		gbc_lblDiretores.gridx = 1;
		gbc_lblDiretores.gridy = 9;
		add(lblDiretores, gbc_lblDiretores);

		txtDiretores = new JTextField();
		txtDiretores.setColumns(10);
		GridBagConstraints gbc_txtDiretores = new GridBagConstraints();
		gbc_txtDiretores.weightx = 10.0;
		gbc_txtDiretores.weighty = 10.0;
		gbc_txtDiretores.gridwidth = 3;
		gbc_txtDiretores.insets = new Insets(0, 0, 5, 5);
		gbc_txtDiretores.fill = GridBagConstraints.BOTH;
		gbc_txtDiretores.gridx = 1;
		gbc_txtDiretores.gridy = 10;
		add(txtDiretores, gbc_txtDiretores);

		lblAtoresPrincipais = new JLabel("Atores Principais:");
		GridBagConstraints gbc_lblAtoresPrincipais = new GridBagConstraints();
		gbc_lblAtoresPrincipais.weightx = 10.0;
		gbc_lblAtoresPrincipais.weighty = 10.0;
		gbc_lblAtoresPrincipais.anchor = GridBagConstraints.SOUTHWEST;
		gbc_lblAtoresPrincipais.insets = new Insets(0, 0, 5, 5);
		gbc_lblAtoresPrincipais.gridx = 1;
		gbc_lblAtoresPrincipais.gridy = 11;
		add(lblAtoresPrincipais, gbc_lblAtoresPrincipais);

		txtAtoresPrincipais = new JTextField();
		txtAtoresPrincipais.setColumns(10);
		GridBagConstraints gbc_txtAtoresPrincipais = new GridBagConstraints();
		gbc_txtAtoresPrincipais.weighty = 10.0;
		gbc_txtAtoresPrincipais.weightx = 10.0;
		gbc_txtAtoresPrincipais.gridwidth = 3;
		gbc_txtAtoresPrincipais.insets = new Insets(0, 0, 5, 5);
		gbc_txtAtoresPrincipais.fill = GridBagConstraints.BOTH;
		gbc_txtAtoresPrincipais.gridx = 1;
		gbc_txtAtoresPrincipais.gridy = 12;
		add(txtAtoresPrincipais, gbc_txtAtoresPrincipais);

	}


	@Override
	public String criar() {
		try {
			if (!dao.add(leMidia())) {
				return "Nome ja existe";
			}

			return "";
		} catch (Exception e) {
			// e.printStackTrace();
			return e.getMessage();
		}
	}

	private Midia leMidia() {
		int ano = 0;
		try {
			ano = Integer.parseInt(txtAno.getText());
		} catch (NumberFormatException e) {
			throw new IllegalArgumentException("Formato do ano invalido");
		}
		if (ano < 1800) {
			throw new IllegalArgumentException("Ano sem tecnologia para isso");
		}

		return new Filme(txtGenero.getText(), ano, txtNome.getText(),
				txtDescricao.getText(), txtTitulo.getText(),
				txtDiretores.getText(), txtAtoresPrincipais.getText());
	}

	@Override
	public void limpar() {
		txtNome.setText("");
		txtTitulo.setText("");
		txtGenero.setText("");
		txtAno.setText("");
		txtDescricao.setText("");
		txtDiretores.setText("");
		txtAtoresPrincipais.setText("");

	}

	@Override
	public String salva() {

		try {
			Midia filme = leMidia();

			dao.alterar(filme, nomeDoArquivoDeMidia);
			nomeDoArquivoDeMidia = txtNome.getText();
			midia = (Midia) filme;

			txtNome.setEditable(false);
			txtTitulo.setEditable(false);
			txtGenero.setEditable(false);
			txtAno.setEditable(false);
			txtDescricao.setEditable(false);
			txtAtoresPrincipais.setEditable(false);
			txtDiretores.setEditable(false);

			return null;
		} catch (Exception e) {
			return e.getMessage();
		}

	}

	@Override
	public void liberaEdicao() {
		txtNome.setEditable(true);
		txtTitulo.setEditable(true);
		txtGenero.setEditable(true);
		txtAno.setEditable(true);
		txtDescricao.setEditable(true);
		txtAtoresPrincipais.setEditable(true);
		txtDiretores.setEditable(true);

	}

}
