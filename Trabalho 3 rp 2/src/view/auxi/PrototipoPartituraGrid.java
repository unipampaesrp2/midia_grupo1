package view.auxi;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.JLabel;
import javax.swing.JTextField;

import midias.Midia;
import midias.Partitura;
import view.base.AdicionadorListador;
import controler.DAOMidia;

/**
 * Painel adicionador listador de {@link Partitura}
 */
@SuppressWarnings("serial")
public class PrototipoPartituraGrid extends AdicionadorListador {
	private JTextField txtTitulo;
	private JTextField txtNome;
	private JLabel lblNewLabel_1;
	private JTextField txtDescricao;
	private JLabel lblGnero;
	private JTextField txtGenero;
	private JLabel lblNewLabel_2;
	private JTextField txtAno;
	private JLabel lblInstrumentos;
	private JTextField txtInstrumentos;
	
	/**
	 * Cria painel com fu��o de adicionar {@link Partitura}s
	 * @param dao DAO onde serao adicionados os dados
	 */
	public PrototipoPartituraGrid(DAOMidia dao) {
		this();
		super.dao=dao;
	}
	
	/**
	 * Cria painel com a fun��o de listar os dados de um {@link Partitura} e manipola-los
	 * @param partitura {@link Partitura} do qual os dados ser�o listados
	 */
	public PrototipoPartituraGrid(Partitura partitura) {
		this();
		nomeDoArquivoDeMidia=partitura.getNome();
		midia=(Midia)partitura;
		
		txtNome.setText(partitura.getNome());
		txtTitulo.setText(partitura.getTitulo());
		txtDescricao.setText(partitura.getDescricao());
		txtGenero.setText(partitura.getGenero());
		txtAno.setText(Integer.toString(partitura.getAno()));
		txtInstrumentos.setText(partitura.getInstrumentos());
		
		

		txtNome.setEditable(false);
		txtTitulo.setEditable(false);
		txtGenero.setEditable(false);
		txtAno.setEditable(false);
		txtDescricao.setEditable(false);
		txtInstrumentos.setEditable(false);
	}
	
	
	
	

	/**
	 * Create the panel.
	 */
	public PrototipoPartituraGrid() {
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[]{0, 0, 0, 0, 0, 0};
		gridBagLayout.rowHeights = new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
		gridBagLayout.columnWeights = new double[]{0.0, 1.0, 0.0, 1.0, 0.0, Double.MIN_VALUE};
		gridBagLayout.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		setLayout(gridBagLayout);
		
		JLabel lblNome = new JLabel("T\u00EDtulo:");
		GridBagConstraints gbc_lblNome = new GridBagConstraints();
		gbc_lblNome.weighty = 10.0;
		gbc_lblNome.weightx = 10.0;
		gbc_lblNome.anchor = GridBagConstraints.SOUTHWEST;
		gbc_lblNome.insets = new Insets(0, 0, 5, 5);
		gbc_lblNome.gridx = 1;
		gbc_lblNome.gridy = 1;
		add(lblNome, gbc_lblNome);
		
		txtTitulo = new JTextField();
		GridBagConstraints gbc_txtTitulo = new GridBagConstraints();
		gbc_txtTitulo.weightx = 10.0;
		gbc_txtTitulo.weighty = 10.0;
		gbc_txtTitulo.gridwidth = 3;
		gbc_txtTitulo.insets = new Insets(0, 0, 5, 5);
		gbc_txtTitulo.fill = GridBagConstraints.BOTH;
		gbc_txtTitulo.gridx = 1;
		gbc_txtTitulo.gridy = 2;
		add(txtTitulo, gbc_txtTitulo);
		txtTitulo.setColumns(10);
		
		JLabel lblNewLabel = new JLabel("Nome:");
		GridBagConstraints gbc_lblNewLabel = new GridBagConstraints();
		gbc_lblNewLabel.weightx = 10.0;
		gbc_lblNewLabel.weighty = 10.0;
		gbc_lblNewLabel.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel.anchor = GridBagConstraints.SOUTHWEST;
		gbc_lblNewLabel.gridx = 1;
		gbc_lblNewLabel.gridy = 3;
		add(lblNewLabel, gbc_lblNewLabel);
		
		txtNome = new JTextField();
		GridBagConstraints gbc_txtNome = new GridBagConstraints();
		gbc_txtNome.weighty = 10.0;
		gbc_txtNome.weightx = 10.0;
		gbc_txtNome.gridwidth = 3;
		gbc_txtNome.insets = new Insets(0, 0, 5, 5);
		gbc_txtNome.fill = GridBagConstraints.BOTH;
		gbc_txtNome.gridx = 1;
		gbc_txtNome.gridy = 4;
		add(txtNome, gbc_txtNome);
		txtNome.setColumns(10);
		
		lblNewLabel_1 = new JLabel("Descri\u00E7\u00E3o:");
		GridBagConstraints gbc_lblNewLabel_1 = new GridBagConstraints();
		gbc_lblNewLabel_1.weighty = 10.0;
		gbc_lblNewLabel_1.weightx = 10.0;
		gbc_lblNewLabel_1.anchor = GridBagConstraints.SOUTHWEST;
		gbc_lblNewLabel_1.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel_1.gridx = 1;
		gbc_lblNewLabel_1.gridy = 5;
		add(lblNewLabel_1, gbc_lblNewLabel_1);
		
		txtDescricao = new JTextField();
		GridBagConstraints gbc_txtDescricao = new GridBagConstraints();
		gbc_txtDescricao.weightx = 10.0;
		gbc_txtDescricao.weighty = 10.0;
		gbc_txtDescricao.gridwidth = 3;
		gbc_txtDescricao.insets = new Insets(0, 0, 5, 5);
		gbc_txtDescricao.fill = GridBagConstraints.BOTH;
		gbc_txtDescricao.gridx = 1;
		gbc_txtDescricao.gridy = 6;
		add(txtDescricao, gbc_txtDescricao);
		txtDescricao.setColumns(10);
		
		lblGnero = new JLabel("G\u00EAnero:");
		GridBagConstraints gbc_lblGnero = new GridBagConstraints();
		gbc_lblGnero.weighty = 10.0;
		gbc_lblGnero.weightx = 10.0;
		gbc_lblGnero.anchor = GridBagConstraints.SOUTHWEST;
		gbc_lblGnero.insets = new Insets(0, 0, 5, 5);
		gbc_lblGnero.gridx = 1;
		gbc_lblGnero.gridy = 7;
		add(lblGnero, gbc_lblGnero);
		
		lblNewLabel_2 = new JLabel("Ano:");
		GridBagConstraints gbc_lblNewLabel_2 = new GridBagConstraints();
		gbc_lblNewLabel_2.weighty = 10.0;
		gbc_lblNewLabel_2.weightx = 10.0;
		gbc_lblNewLabel_2.anchor = GridBagConstraints.SOUTHWEST;
		gbc_lblNewLabel_2.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel_2.gridx = 3;
		gbc_lblNewLabel_2.gridy = 7;
		add(lblNewLabel_2, gbc_lblNewLabel_2);
		
		txtGenero = new JTextField();
		GridBagConstraints gbc_txtGenero = new GridBagConstraints();
		gbc_txtGenero.weighty = 10.0;
		gbc_txtGenero.weightx = 10.0;
		gbc_txtGenero.insets = new Insets(0, 0, 5, 5);
		gbc_txtGenero.fill = GridBagConstraints.BOTH;
		gbc_txtGenero.gridx = 1;
		gbc_txtGenero.gridy = 8;
		add(txtGenero, gbc_txtGenero);
		txtGenero.setColumns(10);
		
		txtAno = new JTextField();
		GridBagConstraints gbc_txtAno = new GridBagConstraints();
		gbc_txtAno.weighty = 10.0;
		gbc_txtAno.weightx = 10.0;
		gbc_txtAno.insets = new Insets(0, 0, 5, 5);
		gbc_txtAno.fill = GridBagConstraints.BOTH;
		gbc_txtAno.gridx = 3;
		gbc_txtAno.gridy = 8;
		add(txtAno, gbc_txtAno);
		txtAno.setColumns(10);
		
		lblInstrumentos = new JLabel("Instrumentos:");
		GridBagConstraints gbc_lblInstrumentos = new GridBagConstraints();
		gbc_lblInstrumentos.weighty = 10.0;
		gbc_lblInstrumentos.weightx = 10.0;
		gbc_lblInstrumentos.anchor = GridBagConstraints.SOUTHWEST;
		gbc_lblInstrumentos.insets = new Insets(0, 0, 5, 5);
		gbc_lblInstrumentos.gridx = 1;
		gbc_lblInstrumentos.gridy = 9;
		add(lblInstrumentos, gbc_lblInstrumentos);
		
		txtInstrumentos = new JTextField();
		GridBagConstraints gbc_txtInstrumentos = new GridBagConstraints();
		gbc_txtInstrumentos.weightx = 10.0;
		gbc_txtInstrumentos.weighty = 10.0;
		gbc_txtInstrumentos.gridwidth = 3;
		gbc_txtInstrumentos.insets = new Insets(0, 0, 5, 5);
		gbc_txtInstrumentos.fill = GridBagConstraints.BOTH;
		gbc_txtInstrumentos.gridx = 1;
		gbc_txtInstrumentos.gridy = 10;
		add(txtInstrumentos, gbc_txtInstrumentos);
		txtInstrumentos.setColumns(10);

	}


	@Override
	public String criar() {
		try {
			if (!dao.add(leMidia())) {
				return "Nome ja existe";
			}

			return "";
		} catch (Exception e) {
			// e.printStackTrace();
			return e.getMessage();
		}
	}

	@Override
	public void limpar() {
		txtNome.setText("");
		txtTitulo.setText("");
		txtGenero.setText("");
		txtAno.setText("");
		txtDescricao.setText("");
		txtInstrumentos.setText("");
	}

	@Override
	public String salva() {
		try {
			Midia partitura = leMidia();

			dao.alterar(partitura, nomeDoArquivoDeMidia);
			nomeDoArquivoDeMidia = txtNome.getText();
			midia = (Midia) partitura;

			txtNome.setEditable(false);
			txtTitulo.setEditable(false);
			txtGenero.setEditable(false);
			txtAno.setEditable(false);
			txtDescricao.setEditable(false);
			txtInstrumentos.setEditable(false);

			return null;
		} catch (Exception e) {
			return e.getMessage();
		}
	}

	@Override
	public void liberaEdicao() {
		txtNome.setEditable(true);
		txtTitulo.setEditable(true);
		txtGenero.setEditable(true);
		txtAno.setEditable(true);
		txtDescricao.setEditable(true);
		txtInstrumentos.setEditable(false);

		
	}

	private Midia leMidia() {
		int ano = 0;
		try {
			ano = Integer.parseInt(txtAno.getText());
		} catch (NumberFormatException e) {
			throw new IllegalArgumentException("Formato do ano invalido");
		}
		if (ano < -1000) {
			throw new IllegalArgumentException("Ano sem tecnologia para isso");
		}

		return new Partitura(txtGenero.getText(), ano, txtNome.getText(),
				txtDescricao.getText(), txtTitulo.getText(),txtInstrumentos.getText());
	}
}
