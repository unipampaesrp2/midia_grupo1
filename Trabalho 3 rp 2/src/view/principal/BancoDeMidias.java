package view.principal;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import midias.Audiolivro;
import midias.Ebook;
import midias.Filme;
import midias.Foto;
import midias.Jogos;
import midias.Musica;
import midias.Partitura;
import midias.Podcast;
import view.auxi.PrototipoAudioLivroGrid;
import view.auxi.PrototipoEBookGrid;
import view.auxi.PrototipoFilmeGrid;
import view.auxi.PrototipoFotoGrid;
import view.auxi.PrototipoJogosGrid;
import view.auxi.PrototipoMusicaGrid;
import view.auxi.PrototipoPartituraGrid;
import view.auxi.PrototipoPodcastGrid;
import view.base.AdicionadorListador;
import view.base.MeuBotao;
import view.base.PainelCRUD;
import controler.DAOMidia;
import controler.espec.DAOJogos;
import controler.espec.DAOLinkedHashSet;
import controler.espec.DAOMusica;
import controler.leo.c.DAOArrayList;
import controler.leo.c.DAOLinkedHashMap;

/**
 * Resposavel por gerenciar dados de midias de diversos tipos
 * @author Leonardo
 *
 */
@SuppressWarnings("serial")
public class BancoDeMidias extends JFrame {

	private JPanel contentPane;

	private PainelCRUD audioLivroPainel;
	private PainelCRUD eBookPainel;
	private PainelCRUD filmesPainel;
	private PainelCRUD fotosPainel;
	private PainelCRUD jogosPainel;
	private PainelCRUD musicasPainel;
	private PainelCRUD partiturasPainel;
	private PainelCRUD podcastsPainel;

	private DAOMidia daoAudioLivro;
	private DAOMidia daoEBook;
	private DAOMidia daoFilmes;
	private DAOMidia daoFotos;
	private DAOMidia daoJogos;
	private DAOMidia daoMusicas;
	private DAOMidia daoPartituras;
	private DAOMidia daoPodcasts;

	

	/**
	 * Classe responsavel por gerenciar o banco de midias
	 * 
	 * @param pastaDosArquivos Pasta que o banco de midias salvara os dados
	 * @throws IOException Erro de leirura ou escrita
	 */
	public BancoDeMidias(File pastaDosArquivos) throws IOException {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(0, 0, 1000, 730);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		GridBagLayout gbl_contentPane = new GridBagLayout();
		gbl_contentPane.columnWidths = new int[]{0, 0, 0, 0, 0, 0, 0, 0};
		gbl_contentPane.rowHeights = new int[]{0, 0, 0, 0, 0};
		gbl_contentPane.columnWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		gbl_contentPane.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		contentPane.setLayout(gbl_contentPane);
		
		setMinimumSize(new Dimension(1000,730));
		
		
		File audioLivro=new File(pastaDosArquivos,"audioLivro.txt" );
		File eBook=new File(pastaDosArquivos, "eBook.txt");
		File filmes=new File(pastaDosArquivos, "filmes.txt");
		File fotos=new File(pastaDosArquivos,"fotos.txt" );
		File jogos=new File(pastaDosArquivos,"jogos.txt" );
		File musicas=new File(pastaDosArquivos,"musicas.txt" );
		File partituras=new File(pastaDosArquivos,"partituras.txt" );
		File podcasts=new File(pastaDosArquivos,"podcasts.txt" );
		
		audioLivro.createNewFile();
		eBook.createNewFile();
		filmes.createNewFile();
		fotos.createNewFile();
		jogos.createNewFile();
		musicas.createNewFile();
		partituras.createNewFile();
		podcasts.createNewFile();
		
		
		 
		
		daoAudioLivro=new DAOArrayList(audioLivro);
		daoEBook=new DAOLinkedHashMap(eBook);
		daoFilmes=new DAOArrayList(filmes);
		daoFotos=new DAOArrayList(fotos);
		daoJogos=new DAOJogos(jogos);
		daoMusicas=new DAOMusica(musicas);
		daoPartituras=new DAOLinkedHashSet(partituras);
		daoPodcasts=new DAOArrayList(podcasts);
		
		
		
		
//		
//		Audiolivro al=new Audiolivro("nome", "titulo", "descricao", 60, "idioma", "genero", "autores", 1900, "local", "editora");
//		daoAudioLivro.add(al);
//		al=new Audiolivro("nome1", "titulo1", "descricao1", 61, "idioma1", "genero1", "autores1", 1901, "local1", "editora1");
//		daoAudioLivro.add(al);
//		al=new Audiolivro("nome2", "titulo2", "descricao2", 62, "idioma2", "genero2", "autores2", 1902, "local2", "editora2");
//		daoAudioLivro.add(al);
//		al=new Audiolivro("nome3", "titulo3", "descricao3", 63, "idioma3", "genero3", "autores3", 1903, "local3", "editora3");
//		daoAudioLivro.add(al);
		
//		System.out.println(daoAudioLivro.size());
		
//		String cararteres[]={"q","w","e","r","t","y","u","i","o","p","a","s","d","f","g","h","j","k","l","z","x","c","v","b","n","m"};
//		Random rad=new Random();
////		String cararteres[]={"a","b"};
//		for (int i = 0; i < 20; i++) {
//			String atributos[]=new String[9];
//			for (int j = 0; j < atributos.length; j++) {
//				atributos[j]=cararteres[rad.nextInt(cararteres.length)];
//				for (int j2 = 0; j2 < 5; j2++) {
//					atributos[j]+=cararteres[rad.nextInt(cararteres.length)];	
//				}
//			}
//			
//			
//			daoAudioLivro.add(new Audiolivro(atributos[0], atributos[1], atributos[2], 300+rad.nextInt(600), atributos[3], atributos[4], atributos[5], 1995+rad.nextInt(18), atributos[6], atributos[7]));
//		}
		
		
		
		
		
		
		
		//audio livro
		AdicionadorListador adicionadorAudioLivro=new PrototipoAudioLivroGrid(daoAudioLivro);
								
		audioLivroPainel=new PainelCRUD(this,adicionadorAudioLivro , daoAudioLivro,Audiolivro.nomeDosAtributos());
			
		
		//filme 
		AdicionadorListador adicionadorFilme=new PrototipoFilmeGrid(daoFilmes);
				
		filmesPainel=new PainelCRUD(this, adicionadorFilme, daoFilmes , Filme.nomeDosAtributos());
		
		//musica
		
		AdicionadorListador adicionadorMusica=new PrototipoMusicaGrid(daoMusicas);
				
		musicasPainel=new PainelCRUD(this, adicionadorMusica, daoMusicas,Musica.nomeDosAtributos());
		
		
		//ebook
		
		AdicionadorListador adicionadorEbook=new PrototipoEBookGrid(daoEBook);
				
		eBookPainel=new PainelCRUD(this, adicionadorEbook, daoEBook,Ebook.nomeDosAtributos());
		
		//jogos
		
		AdicionadorListador adicionadorJogos=new PrototipoJogosGrid(daoJogos);
				
		jogosPainel=new PainelCRUD(this, adicionadorJogos, daoJogos,Jogos.nomeDosAtributos());
		
		//podcast
		
		AdicionadorListador adicionadorPodcast=new PrototipoPodcastGrid(daoPodcasts);
				
		podcastsPainel=new PainelCRUD(this, adicionadorPodcast, daoPodcasts,Podcast.nomeDosAtributos());
		
		//partitura
		
		AdicionadorListador adicionadorPartitura=new PrototipoPartituraGrid(daoPartituras);
						
		partiturasPainel=new PainelCRUD(this, adicionadorPartitura, daoPartituras,Partitura.nomeDosAtributos());
		
		//foto
		
		AdicionadorListador adicionadorFoto=new PrototipoFotoGrid(daoFilmes);
						
		fotosPainel=new PainelCRUD(this, adicionadorFoto, daoFotos,Foto.nomeDosAtributos());
		
		 
		
		
		
		
		
		
		
		
		
		///######## Botoes ######################
		JButton btnNewButton = new MeuBotao(new ImageIcon("Imagens"+File.separator+"Audiolivros.jpg"));
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				setContentPane(audioLivroPainel);
				revalidate();
				repaint();
			}
		});
		
		GridBagConstraints gbc_btnNewButton = new GridBagConstraints();
		gbc_btnNewButton.fill = GridBagConstraints.BOTH;
		gbc_btnNewButton.weighty = 100.0;
		gbc_btnNewButton.weightx = 100.0;
		gbc_btnNewButton.insets = new Insets(20, 20, 20, 20);
		gbc_btnNewButton.gridx = 1;
		gbc_btnNewButton.gridy = 1;
		contentPane.add(btnNewButton, gbc_btnNewButton);
		
		JButton btnNewButton_2 =  new MeuBotao(new ImageIcon("Imagens"+File.separator+"Ebook.jpg"));
		btnNewButton_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setContentPane(eBookPainel);
				revalidate();
				repaint();
			}
		});
		GridBagConstraints gbc_btnNewButton_2 = new GridBagConstraints();
		gbc_btnNewButton_2.fill = GridBagConstraints.BOTH;
		gbc_btnNewButton_2.weighty = 100.0;
		gbc_btnNewButton_2.weightx = 100.0;
		gbc_btnNewButton_2.insets = new Insets(20, 20, 20, 20);
		gbc_btnNewButton_2.gridx = 2;
		gbc_btnNewButton_2.gridy = 1;
		contentPane.add(btnNewButton_2, gbc_btnNewButton_2);
		
		JButton btnNewButton_4 =  new MeuBotao(new ImageIcon("Imagens"+File.separator+"Filmes.jpg"));
		btnNewButton_4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setContentPane(filmesPainel);
				revalidate();
				repaint();
			}
		});
		GridBagConstraints gbc_btnNewButton_4 = new GridBagConstraints();
		gbc_btnNewButton_4.insets = new Insets(20, 20, 20, 20);
		gbc_btnNewButton_4.weightx = 100.0;
		gbc_btnNewButton_4.weighty = 100.0;
		gbc_btnNewButton_4.fill = GridBagConstraints.BOTH;
		gbc_btnNewButton_4.gridx = 3;
		gbc_btnNewButton_4.gridy = 1;
		contentPane.add(btnNewButton_4, gbc_btnNewButton_4);
		
		JButton btnNewButton_1 =  new MeuBotao(new ImageIcon("Imagens"+File.separator+"fotos.jpg"));
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setContentPane(fotosPainel);
				revalidate();
				repaint();
			}
		});
		GridBagConstraints gbc_btnNewButton_1 = new GridBagConstraints();
		gbc_btnNewButton_1.fill = GridBagConstraints.BOTH;
		gbc_btnNewButton_1.weighty = 100.0;
		gbc_btnNewButton_1.weightx = 100.0;
		gbc_btnNewButton_1.insets = new Insets(20, 20, 20, 20);
		gbc_btnNewButton_1.gridx = 1;
		gbc_btnNewButton_1.gridy = 2;
		contentPane.add(btnNewButton_1, gbc_btnNewButton_1);
		
		JButton btnNewButton_3 =  new MeuBotao(new ImageIcon("Imagens"+File.separator+"Jogos.jpg"));
		btnNewButton_3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setContentPane(jogosPainel);
				revalidate();
				repaint();
			}
		});
		GridBagConstraints gbc_btnNewButton_3 = new GridBagConstraints();
		gbc_btnNewButton_3.fill = GridBagConstraints.BOTH;
		gbc_btnNewButton_3.weighty = 100.0;
		gbc_btnNewButton_3.weightx = 100.0;
		gbc_btnNewButton_3.insets = new Insets(20, 20, 20, 20);
		gbc_btnNewButton_3.gridx = 2;
		gbc_btnNewButton_3.gridy = 2;
		contentPane.add(btnNewButton_3, gbc_btnNewButton_3);
		
		JButton btnNewButton_5 =  new MeuBotao(new ImageIcon("Imagens"+File.separator+"M�sicas.jpg"));
		btnNewButton_5.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setContentPane(musicasPainel);
				revalidate();
				repaint();
			}
		});
		GridBagConstraints gbc_btnNewButton_5 = new GridBagConstraints();
		gbc_btnNewButton_5.fill = GridBagConstraints.BOTH;
		gbc_btnNewButton_5.insets = new Insets(20, 20, 20, 20);
		gbc_btnNewButton_5.gridx = 3;
		gbc_btnNewButton_5.gridy = 2;
		contentPane.add(btnNewButton_5, gbc_btnNewButton_5);
		
		JButton btnNewButton_6 =  new MeuBotao(new ImageIcon("Imagens"+File.separator+"Partituras.jpg"));
		btnNewButton_6.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setContentPane(partiturasPainel);
				revalidate();
				repaint();
			}
		});
		GridBagConstraints gbc_btnNewButton_6 = new GridBagConstraints();
		gbc_btnNewButton_6.weighty = 100.0;
		gbc_btnNewButton_6.weightx = 100.0;
		gbc_btnNewButton_6.fill = GridBagConstraints.BOTH;
		gbc_btnNewButton_6.insets = new Insets(20, 20, 20, 20);
		gbc_btnNewButton_6.gridx = 1;
		gbc_btnNewButton_6.gridy = 3;
		contentPane.add(btnNewButton_6, gbc_btnNewButton_6);
		
		JButton btnNewButton_7 =  new MeuBotao(new ImageIcon("Imagens"+File.separator+"Podcasts.jpg"));
		btnNewButton_7.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setContentPane(podcastsPainel);
				revalidate();
				repaint();
			}
		});
		GridBagConstraints gbc_btnNewButton_7 = new GridBagConstraints();
		gbc_btnNewButton_7.fill = GridBagConstraints.BOTH;
		gbc_btnNewButton_7.weightx = 100.0;
		gbc_btnNewButton_7.weighty = 100.0;
		gbc_btnNewButton_7.insets = new Insets(20, 20, 20, 20);
		gbc_btnNewButton_7.gridx = 2;
		gbc_btnNewButton_7.gridy = 3;
		contentPane.add(btnNewButton_7, gbc_btnNewButton_7);
		
		MeuBotao meuBotao = new MeuBotao(new ImageIcon("Imagens"+File.separator+"Salvar.jpg"));
		meuBotao.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				daoAudioLivro.salvar();
				daoEBook.salvar();
				daoFilmes.salvar();
				daoFotos.salvar();
				daoJogos.salvar();
				daoMusicas.salvar();
				daoPartituras.salvar();
				daoPodcasts.salvar();
			}
		});
		GridBagConstraints gbc_meuBotao = new GridBagConstraints();
		gbc_meuBotao.weighty = 100.0;
		gbc_meuBotao.weightx = 100.0;
		gbc_meuBotao.fill = GridBagConstraints.BOTH;
		gbc_meuBotao.insets = new Insets(20, 20, 20, 20);
		gbc_meuBotao.gridx = 3;
		gbc_meuBotao.gridy = 3;
		contentPane.add(meuBotao, gbc_meuBotao);
	}
	/**
	 * Seta o painel principal com o painel do menu principal
	 */
	public void retornaMenu() {
		setContentPane(contentPane);
		revalidate();
		repaint();
	}

}
