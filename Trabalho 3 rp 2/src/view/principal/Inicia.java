package view.principal;

import java.awt.EventQueue;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
/**
 * Launch the application.
 * @author Leonardo
 *
 */
public class Inicia {

	/**
	 * Launch the application.
	 * @param args Argumentos de linha de comandop nao necessarios
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					JOptionPane.showMessageDialog(null, "Selecione uma pasta para salvar os dados do banco de midias");
					JFileChooser fch=new JFileChooser();
					fch.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
					int resposta=fch.showOpenDialog(null);
					if (resposta==JFileChooser.APPROVE_OPTION) {
						
//						File f=new File("Arquivo");
//						f.mkdir();
						BancoDeMidias frame = new BancoDeMidias(fch.getSelectedFile());
						frame.setVisible(true);
					}else{
						JOptionPane.showMessageDialog(null, "Tens que selecionar a pasta que queres salvar o banco de midias");
					}
				} catch (Exception e) {
					JOptionPane.showMessageDialog(null, "Erro:"+e.getMessage());
				}
			}
		});
	}

}
