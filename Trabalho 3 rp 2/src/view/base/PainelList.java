package view.base;

import interfaces.ComparaPor;

import java.awt.BorderLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;

import midias.Midia;
import controler.DAOMidia;
import controler.Ordena;
/**
 * Permite que o usuario liste os dados do DAO e e permita sua manipul��o
 * @author Leonardo
 *
 */
@SuppressWarnings("serial")
public class PainelList extends JPanel {

	private JPanel painelListador;
//	private JPanel listadorDeTipoDeOrdenacao;
//	private DAOMidia dao;
	private boolean travado;
	private JScrollPane scrollPane;
	private JTextField txtBusca;
	private JLabel lblMensagemDeErro;

	private List<PainelDadosExterno> dadosDaMidia;

	/**
	 * Cria painel
	 * @param dao {@link DAOMidia} que sera manipulado
	 * @param nomesDosAtributosAOrdenar Nome dos atributos do tipo de midia do DAO ordenado comforme ordem na {@link Midia} e seus subtipos pode ser obtidos pelo metodo nomeDosAtributos() presente na subtipo de cada midia
	 */
	public PainelList(DAOMidia dao, List<String> nomesDosAtributosAOrdenar) {
//		this.dao = dao;

		setLayout(new BorderLayout(0, 0));

		scrollPane = new JScrollPane();
		add(scrollPane, BorderLayout.CENTER);

		painelListador = new JPanel();
		scrollPane.setViewportView(painelListador);

		JPanel painelDeControle = new JPanel();
		add(painelDeControle, BorderLayout.NORTH);
		painelDeControle.setLayout(new BorderLayout(0, 0));

		JPanel panel = new JPanel();
		painelDeControle.add(panel);
		GridBagLayout gbl_panel = new GridBagLayout();
		gbl_panel.columnWidths = new int[] { 75, 75, 75, 75, 75, 75, 0 };
		gbl_panel.rowHeights = new int[] { 20, 0 };
		gbl_panel.columnWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
				Double.MIN_VALUE };
		gbl_panel.rowWeights = new double[] { 0.0, Double.MIN_VALUE };
		panel.setLayout(gbl_panel);

		JLabel lblNewLabel = new JLabel("Ordenar por:");
		GridBagConstraints gbc_lblNewLabel = new GridBagConstraints();
		gbc_lblNewLabel.fill = GridBagConstraints.BOTH;
		gbc_lblNewLabel.insets = new Insets(0, 0, 0, 5);
		gbc_lblNewLabel.gridx = 0;
		gbc_lblNewLabel.gridy = 0;
		panel.add(lblNewLabel, gbc_lblNewLabel);

		JComboBox<String> boxOrdenacao = new JComboBox<String>();

		GridBagConstraints gbc_boxOrdenacao = new GridBagConstraints();
		gbc_boxOrdenacao.weighty = 10.0;
		gbc_boxOrdenacao.weightx = 10.0;
		gbc_boxOrdenacao.fill = GridBagConstraints.BOTH;
		gbc_boxOrdenacao.insets = new Insets(0, 0, 0, 5);
		gbc_boxOrdenacao.gridx = 1;
		gbc_boxOrdenacao.gridy = 0;
		panel.add(boxOrdenacao, gbc_boxOrdenacao);

		JLabel lblNewLabel_1 = new JLabel("Buscar");
		GridBagConstraints gbc_lblNewLabel_1 = new GridBagConstraints();
		gbc_lblNewLabel_1.anchor = GridBagConstraints.EAST;
		gbc_lblNewLabel_1.fill = GridBagConstraints.VERTICAL;
		gbc_lblNewLabel_1.insets = new Insets(0, 0, 0, 5);
		gbc_lblNewLabel_1.gridx = 2;
		gbc_lblNewLabel_1.gridy = 0;
		panel.add(lblNewLabel_1, gbc_lblNewLabel_1);

		txtBusca = new JTextField();
		GridBagConstraints gbc_txtBusca = new GridBagConstraints();
		gbc_txtBusca.weighty = 10.0;
		gbc_txtBusca.weightx = 10.0;
		gbc_txtBusca.fill = GridBagConstraints.BOTH;
		gbc_txtBusca.insets = new Insets(0, 0, 0, 5);
		gbc_txtBusca.gridx = 3;
		gbc_txtBusca.gridy = 0;
		panel.add(txtBusca, gbc_txtBusca);
		txtBusca.setColumns(10);

		JLabel lblPor = new JLabel("por:");
		GridBagConstraints gbc_lblPor = new GridBagConstraints();
		gbc_lblPor.fill = GridBagConstraints.VERTICAL;
		gbc_lblPor.insets = new Insets(0, 0, 0, 5);
		gbc_lblPor.gridx = 4;
		gbc_lblPor.gridy = 0;
		panel.add(lblPor, gbc_lblPor);

		JComboBox<String> boxBusca = new JComboBox<String>();
		for (int i = 0; i < nomesDosAtributosAOrdenar.size(); i++) {
			boxOrdenacao.addItem(nomesDosAtributosAOrdenar.get(i));
			boxBusca.addItem(nomesDosAtributosAOrdenar.get(i));
		}

		boxBusca.addItem("Todos");
		boxBusca.setSelectedIndex(nomesDosAtributosAOrdenar.size());

		boxBusca.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (!travado) {
					if (boxBusca.getSelectedIndex() == nomesDosAtributosAOrdenar
							.size()) {						
						List<AdicionadorListador> aux =new ArrayList<AdicionadorListador>();
						List<Midia>m=dao.getAll();
						for (int i = 0; i < m.size(); i++) {
							aux.add(FabricaDePaineisMidias.getPainel(m.get(i)));
							aux.get(i).setDao(dao);
						}
						
						
						
						lblMensagemDeErro.setText("");
						dadosDaMidia = new LinkedList<PainelDadosExterno>();
						for (int i = 0; i < aux.size(); i++) {
							dadosDaMidia.add(new PainelDadosExterno(aux.get(i),
									PainelList.this));
						}
					} else {
						List<AdicionadorListador> aux =new ArrayList<AdicionadorListador>();
						List<Midia>m;
						
						try {
							m = dao.get(txtBusca.getText(),
									boxBusca.getSelectedIndex());
							lblMensagemDeErro.setText("");
						} catch (Exception e2) {
							lblMensagemDeErro.setText(e2.getMessage());
							return;
						}
						
						for (int i = 0; i < m.size(); i++) {
							aux.add(FabricaDePaineisMidias.getPainel(m.get(i)));
							aux.get(i).setDao(dao);
						}
						
						dadosDaMidia = new LinkedList<PainelDadosExterno>();
						for (int i = 0; i < aux.size(); i++) {
							dadosDaMidia.add(new PainelDadosExterno(aux.get(i),
									PainelList.this));
						}
					}
					// ordena(boxOrdenacao.getSelectedIndex());
					refaz();
				}
			}
		});
		boxOrdenacao.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (!travado) {
					ordena(boxOrdenacao.getSelectedIndex());
				}
			}
		});

		GridBagConstraints gbc_boxBusca = new GridBagConstraints();
		gbc_boxBusca.weighty = 10.0;
		gbc_boxBusca.weightx = 10.0;
		gbc_boxBusca.fill = GridBagConstraints.BOTH;
		gbc_boxBusca.gridx = 5;
		gbc_boxBusca.gridy = 0;
		panel.add(boxBusca, gbc_boxBusca);

		lblMensagemDeErro = new JLabel("");
		painelDeControle.add(lblMensagemDeErro, BorderLayout.NORTH);

		List<AdicionadorListador> aux =new ArrayList<AdicionadorListador>();
		List<Midia>m=dao.getAll();
		for (int i = 0; i < m.size(); i++) {
			aux.add(FabricaDePaineisMidias.getPainel(m.get(i)));
		}
		

		dadosDaMidia = new LinkedList<PainelDadosExterno>();
		for (int i = 0; i < aux.size(); i++) {
			dadosDaMidia
					.add(new PainelDadosExterno(aux.get(i), PainelList.this));
			aux.get(i).setDao(dao);
		}

		refaz();
	}

	private List<ComparaPor> getList() {

		List<ComparaPor> aux = new LinkedList<ComparaPor>();
		for (int i = 0; i < dadosDaMidia.size(); i++) {
			aux.add(dadosDaMidia.get(i));
		}

		return aux;
	}

	private void setList(List<ComparaPor> aux) {
		dadosDaMidia = new LinkedList<PainelDadosExterno>();
		for (int i = 0; i < aux.size(); i++) {
			dadosDaMidia.add((PainelDadosExterno) aux.get(i));
		}
	} 

	/**
	 * Ordena os dados conforme o inesimo atributo. Pode ser obtido atraves de contasntes e esta na mesma ordem das {@link String}s do metodo nomeDosAtributos()
	 * @param t Indice do atributo a ser comparado na ordena��o
	 */
	public void ordena(int t) {

		List<ComparaPor> aux = getList();
		lblMensagemDeErro.setText(Ordena.sort(aux, t));
//		Ordena.mergeSort(aux, t);
//		Ordena.quickSort(aux, t);
		setList(aux);

		refaz();
	}
	
	/**
	 * Trava painel para que o usuario nao se esque�a de salvar as altera�oes
	 * @return Se ja esta travado retorna false se nao retorna true e trava
	 */
	public boolean trava() {
		if (travado) {
			return false;
		} else {
			travado = true;
			return true;
		}
	}
	/**
	 * Destrava o painel
	 */
	public void destrava() {
		travado = false;
	}
	/**
	 * Remove o painel da lista
	 * @param painel Painel a ser removido
	 * @return True se removeu e false se nao removeu por estar travado
	 */
	public boolean remove(PainelDadosExterno painel) {
		if (!travado) {
		for (int i = 0; i < dadosDaMidia.size(); i++) {
			if (dadosDaMidia.get(i) == painel) {
				dadosDaMidia.remove(i);
			}
		}
		refaz();
		return true;
		}
		return false;
	}

	private void refaz() {
		painelListador.removeAll();
		painelListador.setLayout(new GridLayout(dadosDaMidia.size(), 1));

		for (int i = 0; i < dadosDaMidia.size(); i++) {
			painelListador.add(dadosDaMidia.get(i));
		}
		scrollPane.setViewportView(painelListador);
		revalidate();
		repaint();
	}

}
