package view.base;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;

import javax.swing.ImageIcon;
import javax.swing.JButton;

/**
 * Classe responsavel por fazer com que a imagem do botao se redimensione
 * 
 * @author Leonardo
 *
 */
@SuppressWarnings("serial")
public class MeuBotao extends JButton {
	ImageIcon imagem;
	
	/**
	 * Cria botao com imagem redimencionavel
	 * @param img Imagem que o bot�o tera
	 */
	public MeuBotao(ImageIcon img) {
		super();
		imagem=img;
	}
	@Override
	public void paint(Graphics g) {
		super.paint(g);
		BufferedImage bi=new BufferedImage(getWidth(), getHeight(), BufferedImage.TYPE_4BYTE_ABGR);	
		Graphics2D gAux=bi.createGraphics();
		gAux.drawImage(imagem.getImage(), 0, 0, getWidth(), getHeight(), null);
		
		setIcon(new ImageIcon(bi));
		
		
	}
	
}
