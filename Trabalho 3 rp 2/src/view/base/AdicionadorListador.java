package view.base;

import interfaces.ComparaPor;

import javax.swing.JPanel;

import midias.Midia;
import controler.DAOMidia;

/**
 * Classe respons�vel por listar, adicionar, editar e remover midia
 * @author Leonardo
 *
 */
@SuppressWarnings("serial")
public abstract class AdicionadorListador extends JPanel implements ComparaPor{
	
	protected DAOMidia dao;
	protected String nomeDoArquivoDeMidia;
	protected Midia midia;
	
	/**
	 * Retorna midia do painel
	 * @return Midia do painel
	 */
	private Midia getMidia(){
		return midia;
	}
	
	
	/**
	 * Cria o objeto com os dados presentes no painel e adiciona ao DAO
	 * @return Mensagem de erro caso n�o ocorra retorna String vazia
	 */
	public abstract String criar();
	/**
	 * Limpa campos de dados
	 */
	public abstract void limpar();
	
	//||||||||||||||||||||||||||||||||
	/**
	 * Salva altera��es na midia representada
	 * @return Mensagem de erro se ocorrer sen�o null 
	 */
	public abstract String salva();
	
	/**
	 * Deixa o usuario alterar os dados do painel
	 */
	public abstract void liberaEdicao();
	
	
	
	
	/**
	 * Set o dao que o painel vai manipular
	 * @param dao DAO a ser manipulado
	 */
	public void setDao(DAOMidia dao) {
		this.dao = dao;
	}
	/**
	 * Remove midia do DAO
	 */
	public void removeMidia() {
		dao.remover(nomeDoArquivoDeMidia);
	}
	/**
	 * Compara os dados da midia
	 */
	public int compareToBy(ComparaPor obj, int tipo) {
		return midia.compareToBy(((AdicionadorListador)obj).getMidia(), tipo);
	}
	
	/**
	 * Se a midia contem determinado atributo com determinado valor
	 */
	public boolean equalsIn(String str, int tipo) {
		return midia.equalsIn(str, tipo);
	}
	
}
