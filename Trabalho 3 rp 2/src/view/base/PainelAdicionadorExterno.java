package view.base;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

import midias.Midia;

/**
 * Respovel por com ter um painel {@link AdicionadorListador} e gerenciar as fun�oes de criar {@link Midia} e limpar dados
 * @author Leonardo
 *
 */
@SuppressWarnings("serial")
public class PainelAdicionadorExterno extends JPanel {

	AdicionadorListador painelAdicionador;
	
	/**
	 * Cria painel
	 * @param painelAdicionador painel 
	 */
	public PainelAdicionadorExterno(AdicionadorListador painelAdicionador) {
		this.painelAdicionador=painelAdicionador;
		setLayout(new BorderLayout(0, 0));
		
		JPanel panel = new JPanel();
		add(panel, BorderLayout.NORTH);
		
		JLabel lblMensagemdeerro = new JLabel("");
		panel.add(lblMensagemdeerro);
		
		JPanel painelDeBotoes = new JPanel();
		add(painelDeBotoes, BorderLayout.SOUTH);
		painelDeBotoes.setLayout(new GridLayout(1, 0, 0, 0));
		
		JButton btnCriar = new JButton("Criar");
		btnCriar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblMensagemdeerro.setText(painelAdicionador.criar());
			}
		});
		painelDeBotoes.add(btnCriar);
		
		JButton btnLimpar = new JButton("Limpar");
		btnLimpar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				painelAdicionador.limpar();
			}
		});
		painelDeBotoes.add(btnLimpar);
		
		add(painelAdicionador, BorderLayout.CENTER);

	}

}
