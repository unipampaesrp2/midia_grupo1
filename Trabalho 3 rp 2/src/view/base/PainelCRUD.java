package view.base;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JPanel;

import view.principal.BancoDeMidias;
import controler.DAOMidia;


/**
 * Contem paineis que gerenciam o DAO, gerenciam sua alternação e o retorno ao menu principal
 * @author Leonardo
 *
 */
@SuppressWarnings("serial")
public class PainelCRUD extends JPanel {
	private BancoDeMidias janela;
	private PainelList painelExibidor;
	private PainelAdicionadorExterno adicionadorExterno;
	private JPanel painelCental;
	/**
	 * Cria painel
	 * @param janela {@link BancoDeMidias} em que esta contido
	 * @param painelAdicionador Painel adicionador de dados
	 * @param dao {@link DAOMidia} a ser manipulado
	 * @param nomesDosAtributosAOrdenar Nome dos atributos da midia gerenciada
	 */
	public PainelCRUD(BancoDeMidias janela, AdicionadorListador painelAdicionador, DAOMidia dao, List<String> nomesDosAtributosAOrdenar) {
		this.janela=janela;		
//		System.out.println(dao.size());
		adicionadorExterno=new PainelAdicionadorExterno(painelAdicionador);
		painelExibidor=new PainelList(dao, nomesDosAtributosAOrdenar);
		
		
		
		
		setLayout(new BorderLayout(0, 0));
		
		JPanel painelDeControle = new JPanel();
		add(painelDeControle, BorderLayout.WEST);
		painelDeControle.setLayout(new GridLayout(3,1));
		
		JButton btnMenuPrincipal = new JButton(new ImageIcon("ImagensSecundarias"+File.separator+"Voltar.jpg"));
		btnMenuPrincipal.setBackground(Color.white);
		btnMenuPrincipal.setBorder(BorderFactory.createLineBorder(Color.blue));
		btnMenuPrincipal.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				PainelCRUD.this.janela.retornaMenu();
			}
		});
		painelDeControle.add(btnMenuPrincipal);
		
		
		painelCental=new JPanel();
		painelCental.setLayout(new BorderLayout());
		add(painelCental, BorderLayout.CENTER);
		
		JButton btnAdicionar = new JButton(new ImageIcon("ImagensSecundarias"+File.separator+"Adicionar.jpg"));
		btnAdicionar.setBackground(Color.white);
		btnAdicionar.setBorder(BorderFactory.createLineBorder(Color.blue));
		btnAdicionar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				painelCental.removeAll();
				painelCental.add(adicionadorExterno);
				revalidate();
				repaint();
			}
		});
		painelDeControle.add(btnAdicionar);
		
		JButton btnDados = new JButton(new ImageIcon("ImagensSecundarias"+File.separator+"Pesquisar.jpg"));
		btnDados.setBackground(Color.white);
		btnDados.setBorder(BorderFactory.createLineBorder(Color.blue));
		btnDados.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				painelCental.removeAll();
				painelCental.add(painelExibidor);
				revalidate();
				repaint();
			}
		});
		painelDeControle.add(btnDados);
		
		
	
		
	}
	
}
