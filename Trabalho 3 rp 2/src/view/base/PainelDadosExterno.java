package view.base;

import interfaces.ComparaPor;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

import midias.Midia;

/**
 * Responsavel por conter um painel que lista dados e liberar edi��o, salvar edi��o e deletar {@link Midia}
 * @author Leonardo
 *
 */
@SuppressWarnings("serial")
public class PainelDadosExterno extends JPanel implements ComparaPor{
//	private PainelList painelList; 
	private AdicionadorListador painelDeDados;
	private AdicionadorListador getPainelDeDados(){
		return painelDeDados;
	}
	
	
	/**
	 * Cria painel de dados externo
	 * @param painelDeDados O painel {@link AdicionadorListador} com fun��o de listar
	 * @param painelList O {@link PainelList} em que esta contido
	 */
	public PainelDadosExterno(AdicionadorListador painelDeDados,PainelList painelList) {
//		this.painelList = painelList;
		this.painelDeDados=painelDeDados;
		setLayout(new BorderLayout(0, 0));
		setBorder(BorderFactory.createLineBorder(Color.black));
		

		JPanel painel=new JPanel();
		add(painel, BorderLayout.CENTER);
		painel.setLayout(new BorderLayout(0, 0));
		
		JLabel lblMensagemDeErro = new JLabel();
		painel.add(lblMensagemDeErro, BorderLayout.NORTH);
		
		painel.add(painelDeDados, BorderLayout.CENTER);
		
		
		JPanel painelLook = new JPanel();
		add(painelLook, BorderLayout.EAST);
		painelLook.setLayout(new GridLayout(3,1));
		JButton btnEdita = new JButton(new ImageIcon("ImagensSecundarias"+File.separator+"Editar.jpg"));
		JButton btnSalva = new JButton(new ImageIcon("ImagensSecundarias"+File.separator+"Salvar.jpg"));
		
		btnSalva.setEnabled(false);
		
		JButton btnExcluir = new JButton(new ImageIcon("ImagensSecundarias"+File.separator+"Excluir.jpg"));
		
		btnSalva.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String resposta=painelDeDados.salva();
//				System.out.println(resposta);
				if (resposta==null) {
					painelList.destrava();
					btnEdita.setEnabled(true);
					btnSalva.setEnabled(false);
					btnExcluir.setEnabled(true);
					lblMensagemDeErro.setText("");
				} else {
					lblMensagemDeErro.setText(resposta);
				}
			}
		});		
		
		btnEdita.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (painelList.trava()) {
					painelDeDados.liberaEdicao();
					btnEdita.setEnabled(false);
					btnExcluir.setEnabled(false);
					btnSalva.setEnabled(true);
				}
			}
		});		
		
		btnExcluir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (painelList.remove(PainelDadosExterno.this)) {
					painelDeDados.removeMidia();
				}
				
				
			}
		});
		
		
		btnEdita.setBackground(Color.white);
		btnExcluir.setBackground(Color.white);
		btnSalva.setBackground(Color.white);
		
		btnEdita.setBorder(BorderFactory.createLineBorder(Color.blue));
		btnExcluir.setBorder(BorderFactory.createLineBorder(Color.blue));
		btnSalva.setBorder(BorderFactory.createLineBorder(Color.blue));
		
		
		painelLook.add(btnExcluir);
		painelLook.add(btnEdita);		
		painelLook.add(btnSalva);
		
	}

	/**
	 * Chama metodo que comapra dados da midia
	 */
	@Override
	public int compareToBy(ComparaPor obj, int tipo) {
		return painelDeDados.compareToBy(((PainelDadosExterno)obj).getPainelDeDados(), tipo);
	}


	/**
	 * Chama metodo que comapra dados da midia
	 */
	@Override
	public boolean equalsIn(String str, int tipo) {
		return painelDeDados.equalsIn(str, tipo);
	}
	
	

}
