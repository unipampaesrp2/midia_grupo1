package view.base;

import midias.Audiolivro;
import midias.Ebook;
import midias.Filme;
import midias.Foto;
import midias.Jogos;
import midias.Midia;
import midias.Musica;
import midias.Partitura;
import midias.Podcast;
import view.auxi.PrototipoAudioLivroGrid;
import view.auxi.PrototipoEBookGrid;
import view.auxi.PrototipoFilmeGrid;
import view.auxi.PrototipoFotoGrid;
import view.auxi.PrototipoJogosGrid;
import view.auxi.PrototipoMusicaGrid;
import view.auxi.PrototipoPartituraGrid;
import view.auxi.PrototipoPodcastGrid;

public class FabricaDePaineisMidias {
	public static AdicionadorListador getPainel(Midia m) {
		
		if (m instanceof  Audiolivro) {
			return new PrototipoAudioLivroGrid((Audiolivro)m);			
		}else if (m instanceof  Ebook) {
			return new PrototipoEBookGrid((Ebook)m);
		}else if (m instanceof  Filme) {
			return new PrototipoFilmeGrid((Filme)m);
		}else if (m instanceof  Foto) {
			return new PrototipoFotoGrid((Foto)m);
		}else if (m instanceof  Jogos) {
			return new PrototipoJogosGrid((Jogos)m);
		}else if (m instanceof Musica ) {
			return new PrototipoMusicaGrid((Musica)m);
		}else if (m instanceof  Partitura) {
			return new PrototipoPartituraGrid((Partitura)m);
		}else if (m instanceof  Podcast) {
			return new PrototipoPodcastGrid((Podcast)m);
		}else {
			throw new IllegalArgumentException("Painel ainda nao desenvolvido");
		}
	}
}
